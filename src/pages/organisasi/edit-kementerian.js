import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  setCookies,
  getCookies,
} from "../../lib/actions";
import InputAlamatKementerian from "../../components/tables/inputAlamatKementerian";

const EditKementerian = ({ nav, isAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/alamat-kemenparekrafs/` +
          getCookies("alamat_kementerian"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/organisasi/alamat-kementerian");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <WidgetTitle title={"Edit Alamat Kementerian"} />
        {nav
          .filter((el) => el.id == getCookies("alamat_kementerian"))
          .map((el, id) => (
            <InputAlamatKementerian
              id={id}
              register={register}
              handleSubmit={handleSubmit}
              onSubmit={onSubmit}
              el={el}
            />
          ))}
      </div>
    </Layout>
  );
};

EditKementerian.getInitialProps = async (ctx) => {
  let nav;
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/alamat-kemenparekrafs`,
  );

  nav = await resdata.json();
  return { nav, isAuthenticated };
};
export default withRedux(EditKementerian);
