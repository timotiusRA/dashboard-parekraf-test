import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../../lib/actions";
import InputOrganisasiPublik from "../../components/tables/inputOrganisasiPublik";

const EditKementerian = ({
  dataLink,
  isAuthenticated,
  all,
  statusKategori,
}) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (dataKu) => {
    const emailPayload = [{ email: dataKu["email"] }];
    dataKu["email"] = emailPayload;

    console.log("ini data ", dataKu);

    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses/` +
          getCookies("organisasi_publik"),
        dataKu,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/organisasi/organisasi-publik");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <WidgetTitle title={"Edit Organisasi Publik"} />
        <InputOrganisasiPublik
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          register={register}
          el={dataLink}
          kategori={all}
          statusKategori={statusKategori}
        />
      </div>
    </Layout>
  );
};

EditKementerian.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses/` +
      getCookies("organisasi_publik", ctx.req),
  );

  const dataLink = await resdata.json();
  let statusKategori;
  if (dataLink.organisasi_publik && dataLink.organisasi_publik["id"] === 1)
    statusKategori = "sertifikasi wisata";
  else if (dataLink.organisasi_publik && dataLink.organisasi_publik["id"] === 2)
    statusKategori = "vito";
  else if (dataLink.organisasi_publik && dataLink.organisasi_publik["id"] === 3)
    statusKategori = "lembaga pariwisata";
  else if (dataLink.organisasi_publik && dataLink.organisasi_publik["id"] === 4)
    statusKategori = "dinas pariwisata";
  else if (dataLink.organisasi_publik && dataLink.organisasi_publik["id"] === 5)
    statusKategori = "kementerian dan lembaga";

  const ress = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/organisasi-publiks`,
  );

  const all = await ress.json();
  return { dataLink, isAuthenticated, all, statusKategori };
};
export default withRedux(EditKementerian);
