import { useEffect } from "react";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import InputAlamatUpt from "../../components/tables/inputAlamatUpt";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

const AddUpt = ({ isAuthenticated, isAuthor, isContributor }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    await axios
      .post(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis`,
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/organisasi/alamat-upt");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <WidgetTitle title={"Add Alamat UPT"} />

        <InputAlamatUpt
          el={null}
          register={register}
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
        />
      </div>
    </Layout>
  );
};

AddUpt.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  return {
    isAuthenticated,
    isAuthor,
    isContributor,
  };
};
export default withRedux(AddUpt);
