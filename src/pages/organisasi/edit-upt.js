import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../../lib/actions";
import InputAlamatUpt from "../../components/tables/inputAlamatUpt";

const EditUpt = ({ nav, isAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis/` +
          getCookies("alamat_upt_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/organisasi/alamat-upt");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <WidgetTitle title={"Edit Alamat UPT"} />

        {nav
          .filter((el) => el.id == getCookies("alamat_upt_id"))
          .map((el, id) => (
            <InputAlamatUpt
              el={el}
              key={id}
              register={register}
              handleSubmit={handleSubmit}
              onSubmit={onSubmit}
            />
          ))}
      </div>
    </Layout>
  );
};

EditUpt.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  let nav;
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis`,
  );

  nav = await resdata.json();
  return { nav, isAuthenticated };
};
export default withRedux(EditUpt);
