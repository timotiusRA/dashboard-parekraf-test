import { useState, useEffect } from "react";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Pagination from "../../components/tables/pagination";
import TableOrganisasiPublik from "../../components/tables/tableOrganisasiPublik";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const OrganisasiPublik = ({
  dataLink,
  isAuthenticated,
  isContributor,
  isAuthor,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  const handleOnClick = (ortuId) => {
    setCookies("organisasi_publik", ortuId);
    router.push("/organisasi/edit-organisasi");
  };

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/organisasi/organisasi-publik");
      }, 1000);

      didDeleted(false);
    }
  }, [deleted, role, start, page, limit]);

  const deleteItem = async (ortu) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses/` + ortu, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses/count?title_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));

    return await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses?title_id_contains=${input}&_limit=${limit}&_start=${start}`,
    )
      .then((response) => response.json())
      .then((data) => {
        setSearchData(data);
      });
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle
            title={"Alamat Organisasi Publik".toUpperCase()}
            subtitle=""
          />
          <Link href="/organisasi/add-organisasi">
            <button className="mb-5 py-3 px-2 bg-primaryNavy text-white rounded-lg ">
              Tambah Data
            </button>
          </Link>
        </div>

        <Widget title="">
          <div className="flex justify-between mb-5">
            <span className="text-lg font-semibold -mb-5">Daftar Alamat</span>
            <div className="flex">
              <TotalEntries
                url={"/organisasi/organisasi-publik"}
                limit={limit}
              />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableOrganisasiPublik
            dataLink={input === "" ? dataLink : searchData}
            handleOnClick={handleOnClick}
            deleteItem={deleteItem}
          />

          <Pagination
            url={"/organisasi/organisasi-publik"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

OrganisasiPublik.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses?_limit=${limit}&_start=${start}`,
  );

  const dataLink = await resdata.json();

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses/count`,
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    dataLink,
    isAuthenticated,
    isAuthor,
    isContributor,
    page: +page,
    limit,
    start,
    numberOfArticles,
  };
};

export default withRedux(OrganisasiPublik);
