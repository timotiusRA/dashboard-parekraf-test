import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import InputOrganisasiPublik from "../../components/tables/inputOrganisasiPublik";

const AddOrganisasi = ({ nav, isAuthenticated, isAuthor, isContributor }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);

  const onSubmit = async (dataKu) => {
    const emailPayload = [{ email: dataKu["email"] }];
    dataKu["email"] = emailPayload;

    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/all-addresses`, dataKu, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/organisasi/organisasi-publik");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <WidgetTitle title={"Add Organisasi Publik"} />

        <InputOrganisasiPublik
          kategori={nav}
          handleSubmit={handleSubmit}
          el={null}
          onSubmit={onSubmit}
          register={register}
          statusKategori={null}
        />
      </div>
    </Layout>
  );
};

AddOrganisasi.getInitialProps = async (ctx) => {
  let nav;
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/organisasi-publiks`,
  );

  nav = await resdata.json();

  return { nav, isAuthenticated, isAuthor, isContributor };
};
export default withRedux(AddOrganisasi);
