import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Pagination from "../../components/tables/pagination";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import TableAlamatUpt from "../../components/tables/tableAlamatUpt";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const AlamatKementerian = ({
  dataLink,
  isAuthor,
  isContributor,
  isAuthenticated,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const handleOnClick = (id) => {
    setCookies("alamat_upt_id", id);
    router.push("/organisasi/edit-upt");
  };

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/organisasi/alamat-upt");
      }, 1000);

      didDeleted(false);
    }
  }, [role, deleted, start, page, limit]);

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(
          `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis/` +
            id,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis?instance_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis/count?instance_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle title={"Alamat UPT".toUpperCase()} subtitle="" />
          <Link href="/organisasi/add-upt">
            <button className="mb-5 py-3 px-2 bg-primaryNavy text-white rounded-lg ">
              Tambah Data
            </button>
          </Link>
        </div>
        <Widget title="">
          <div className="flex items-center justify-between">
            <span className="text-lg font-semibold -mb-5">Daftar Alamat</span>
            <div className="flex">
              <TotalEntries url={"/organisasi/alamat-upt"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableAlamatUpt
            dataLink={input === "" ? dataLink : searchData}
            handleOnClick={handleOnClick}
            deleteItem={deleteItem}
          />
          <Pagination
            url={"/organisasi/alamat-upt"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

AlamatKementerian.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;

  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis?_limit=${limit}&_start=${start}`,
  );

  const dataLink = await resdata.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/daftar-unit-pelaksana-teknis/count`,
  );
  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    dataLink,
    isAuthenticated,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(AlamatKementerian);
