import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import { NotificationManager } from "react-notifications";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, setCookies } from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import TableMenu from "../../components/tables/tableMenu";
import Link from "next/link";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const Menu = ({
  data,
  isAuthenticated,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/menu/all-menu");
      }, 1000);

      didDeleted(false);
    }
  }, [deleted, role, start, page, limit]);

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus/` + id, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => didDeleted(true))
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  const editItem = async (id) => {
    setCookies("menu_id", id);
    router.push("/menu/edit-menu");
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus/count?menu_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
    const allRes = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus`,
    );

    const dataAll = await allRes.json();

    return await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus?menu_id_contains=${input}&_limit=${limit}&_start=${start}`,
    )
      .then((response) => response.json())
      .then((data) => {
        for (let i = 0; i < dataAll.length; i++) {
          for (let j = 0; j < data.length; j++) {
            if (data[j].parent_id == dataAll[i].id) {
              data[j].parent_name_id = dataAll[i].menu_id;
              continue;
            }
          }
        }
        setSearchData(data);
      });
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="flex justify-between">
        <SectionTitle title="NAVIGATION MENU" subtitle="" />
        <Link href="/menu/add-menu">
          <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
            Tambah Data
          </button>
        </Link>
      </div>
      <Widget title="">
        <div className="flex justify-between mb-5">
          <span className="text-lg font-semibold -mb-5">Daftar Menu</span>
          <div className="flex">
            <TotalEntries url={"/menu/all-menu"} limit={limit} />
            <SearchBar updateInput={updateInput} input={input} />
          </div>
        </div>
        <TableMenu
          data={input === "" ? data : searchData}
          editItem={editItem}
          deleteItem={deleteItem}
        />
      </Widget>
      <Pagination
        url={"/menu/all-menu"}
        lastPage={input === "" ? lastPage : pageSearch}
        page={page}
        limit={limit}
      />
    </Layout>
  );
};

Menu.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus?_limit=${limit}&_start=${start}`,
  );

  const data = await resdata.json();

  const allRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus`,
  );

  const dataAll = await allRes.json();

  for (let i = 0; i < dataAll.length; i++) {
    for (let j = 0; j < data.length; j++) {
      if (data[j].parent_id == dataAll[i].id) {
        data[j].parent_name_id = dataAll[i].menu_id;
        continue;
      }
    }
  }

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus/count`,
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    isAuthenticated,
    data,
    page: +page,
    limit: +limit,
    start,
    numberOfArticles,
  };
};

export default withRedux(Menu);
