import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import InputMenu from "../../components/tables/inputMenu";

const EditMenu = ({ nav, isAuthenticated, el }) => {
  const [parent, setParent] = useState(nav && nav.parent_id);
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["parent_id"] = parent;
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus/` +
          getCookies("menu_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/menu/all-menu");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title="NAVIGATION MENU" subtitle="" />
        <WidgetTitle title={"Edit Menu"} />

        <InputMenu
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          register={register}
          nav={nav}
          el={el}
          parent={parent}
          setParent={setParent}
        />
      </div>
    </Layout>
  );
};

EditMenu.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  let nav;
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus`,
  );

  nav = await resdata.json();

  const ress = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus/` +
      getCookies("menu_id", ctx.req),
  );

  const el = await ress.json();
  return { nav, isAuthenticated, el };
};
export default withRedux(EditMenu);
