import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import InputMenu from "../../components/tables/inputMenu";

const AddMenu = ({ nav, isAuthenticated }) => {
  const [parent, setParent] = useState(0);
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["parent_id"] = parent;
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/menu/all-menu");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error"));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title="NAVIGATION MENU" subtitle="" />
        <WidgetTitle title={"Add New Menu"} />
        <InputMenu
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          register={register}
          nav={nav}
          el={null}
          parent={parent}
          setParent={setParent}
        />
      </div>
    </Layout>
  );
};

AddMenu.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/header-menus`,
  );

  const nav = await resdata.json();

  return {
    nav,
    isAuthenticated,
  };
};

export default withRedux(AddMenu);
