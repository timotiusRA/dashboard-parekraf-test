import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import * as Icon from "react-feather";

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

const ImageIndonesiWonderful = ({ isAuthenticated, wonderful }) => {
  const { register, handleSubmit } = useForm();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );

  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmitWonderful = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar tidak berhasil diupload", 5000),
        );
      delete data["files"];
    }

    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/wonderful-image`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diedit", 3000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <>
        <div className="w-full lg:px-2 ">
          <div className="flex justify-between">
            <SectionTitle title="EDIT WONDERFUL IMAGE" subtitle="" />
            <a target="_blank" href="https://kemenparekraf.go.id">
              <button className="flex">
                <Icon.Eye size={30} className="text-gold1" />
                <span className="mt-1 ml-3"> See Page</span>
              </button>
            </a>
          </div>

          <div className=" flex-wrap w-full mb-4">
            <form
              onSubmit={handleSubmit(onSubmitWonderful)}
              className="grid grid-cols-1"
            >
              <div className="grid grid-cols-6 ">
                <div className="col-span-6">
                  <label className="m-2">Name (ID)</label>
                  <textarea
                    className={`${style}`}
                    ref={register}
                    type="text"
                    name="name_id"
                    defaultValue={wonderful && wonderful.name_id}
                  />
                </div>
                <div className="col-span-6">
                  <label className="m-2 ">Name (EN)</label>
                  <textarea
                    className={`${style} `}
                    ref={register}
                    type="text"
                    name="name_en"
                    defaultValue={wonderful && wonderful.name_en}
                  />
                </div>
              </div>
              <div className="grid grid-cols-6">
                <div className="col-span-6">
                  <label className="m-2 ">Image </label>
                  <input
                    className={`${style}`}
                    ref={register}
                    type="file"
                    name="files"
                  />
                </div>
              </div>
              <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
                Submit
              </button>
            </form>
          </div>
        </div>
      </>
    </Layout>
  );
};

ImageIndonesiWonderful.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const res = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/wonderful-image`,
  );

  const wonderful = await res.json();
  return {
    isAuthenticated,
    wonderful,
  };
};

export default withRedux(ImageIndonesiWonderful);
