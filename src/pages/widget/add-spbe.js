import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import InputSpbe from "../../components/tables/inputSpbe";

const EditSpbe = ({  isAuthenticated }) => {
  const [htmlEN, setHtmlEN] = useState(article && article.article_en);
  const [htmlID, setHtmlID] = useState(article && article.article_id);
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (htmlEN) {
      data["article_en"] = htmlEN;
    }

    if (htmlID) {
      data["article_id"] = htmlID;
    }

    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["banner"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files"];
    }

    await axios
      .post(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/layanan-spbes` +
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/widget/spbe");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="ADD SPBE" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputSpbe
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
          />
        </div>
      </div>
    </Layout>
  );
};

EditSpbe.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {
    isAuthenticated,
  };
};

export default withRedux(EditSpbe);
