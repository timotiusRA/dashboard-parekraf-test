import React, { useState, useEffect, Fragment } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import * as Icon from "react-feather";
import { useRouter } from "next/router";
import DatePicker from "react-datepicker";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  getCookies,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";

const thPelaku = [
  "Industri ID",
  "Industri EN",
  "Persentase",
  "Rate",
  "Jumlah",
  "Action",
];
const thWidget = ["Rate", "Jumlah", "Waktu", "Action"];

const AllWidget = ({
  pelakuKreatif,
  PelakuPariwisata,
  WidgetKreatif,
  WisatawanNusantara,
  WisatawanMancanegara,
  WidgetPariwisata,
  isAuthenticated,
  isContributor,
  isAuthor,
}) => {
  const [startDate, setStartDate] = useState(new Date());
  const [editpelakuKreatif, isEditPelakuKreatif] = useState(false);
  const [editPelakuPariwisata, isEditPelakuPariwisata] = useState(false);
  const [editWidgetKreatif, isEditWidgetKreatif] = useState(false);
  const [editWisatawanNusantara, isEditWisatawanNusantara] = useState(false);
  const [editWisatawanMancanegara, isEditWisatawanMancanegara] = useState(
    false,
  );
  const [editWidgetPariwisata, isEditWidgetPariwisata] = useState(false);

  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    const url = getCookies("url");
    const arr = [];
    let title_en;
    let title_id;
    let payload;
    if (url.includes("pelaku")) {
      let obj = {};
      for (let i = 1; i < 5; i++) {
        obj = {
          industy_type_en: data[`tipeEN-${i}`],
          industry_type_id: data[`tipeID-${i}`],
          count: data[`countArr-${i}`],
        };
        arr.push(obj);
      }
      if (url.includes("pariwisatas")) {
        title_en = "Tourism Actors";
        title_id = "Pelaku Pariwisata";
      } else {
        title_en = "Creative Economy Actors";
        title_id = "Pelaku Ekonomi Kreatif";
      }
      payload = {
        title_en: title_en,
        title_id: title_id,
        count: data.count,
        rate: data.rate,
        industry_type: arr,
      };
    } else if (url.includes("widget")) {
      if (url.includes("ekonomi")) {
        title_en = "Creative Economy Industry";
        title_id = "Industri Ekonomi Kreatif";
      } else if (url.includes("pariwisatas")) {
        title_en = "Tourism industry";
        title_id = "Industri Pariwisata";
      } else if (url.includes("nusantaras")) {
        title_en = "Indonesian Tourists";
        title_id = "Wisatawan Nusantara";
      } else if (url.includes("mancanegaras")) {
        title_en = "International Tourists";
        title_id = "Wisatawan Mancanegara";
      }
      payload = {
        title_en: title_en,
        title_id: title_id,
        count: data.count,
        rate: data.rate,
        date: startDate,
      };
    }

    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/${url}/1`, payload, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil diubah",null, 500);
        setTimeout(function () {
          router.push("/widget/all-widget");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Gagal diubah", 5000));
  };

  return (
    <>
      <Layout>
        <SectionTitle title="WIDGETS" subtitle="" />
        <Widget title="">
          {/* PELAKU PARIWISATA */}
          <p className="font-semibold text-lg mt-5">Widget Pelaku Pariwisata</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thPelaku.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {PelakuPariwisata &&
                PelakuPariwisata.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editPelakuPariwisata ? (
                      <>
                        {el.industry_type.map((subel, i) => (
                          <tr key={i} className="border-b-2">
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.industry_type_id}</p>
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.industry_type_en}</p>
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.count} %</p>
                              </div>
                            </td>
                            {i === 0 ? (
                              <>
                                <td rowSpan={thPelaku.length}>{el.rate}</td>
                                <td rowSpan={thPelaku.length}>{el.count}</td>
                                <td rowSpan={thPelaku.length}>
                                  <div className="flex justify-center">
                                    <button
                                      onClick={() => {
                                        isEditPelakuPariwisata(
                                          !editPelakuPariwisata,
                                        );
                                        setCookies("url", "pelaku-pariwisatas");
                                      }}
                                      className="-ml-5"
                                    >
                                      <Icon.Edit
                                        className="text-green-500"
                                        size={16}
                                      />
                                    </button>
                                  </div>
                                </td>
                              </>
                            ) : null}
                          </tr>
                        ))}
                      </>
                    ) : (
                      <>
                        {el.industry_type.map((subel, i) => (
                          <tr key={i} className="border-b-2">
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`tipeID-${i + 1}`}
                                  defaultValue={subel.industry_type_id}
                                />
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`tipeEN-${i + 1}`}
                                  defaultValue={subel.industry_type_en}
                                />
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 w-1/2 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`countArr-${i + 1}`}
                                  defaultValue={subel.count}
                                />
                              </div>
                            </td>

                            {i === 0 ? (
                              <>
                                <td rowSpan={thPelaku.length}>
                                  <input
                                    className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                    ref={register}
                                    type="text"
                                    name={`rate`}
                                    defaultValue={el.rate}
                                  />
                                </td>
                                <td rowSpan={thPelaku.length}>
                                  <form
                                    onSubmit={handleSubmit(onSubmit)}
                                    className="flex"
                                  >
                                    <input
                                      className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                      ref={register}
                                      type="text"
                                      name={`count`}
                                      defaultValue={el.count}
                                    />
                                    <button
                                      type="submit"
                                      className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                                    >
                                      Submit
                                    </button>
                                  </form>
                                </td>
                                <td rowSpan={thPelaku.length}>
                                  <div className="flex justify-center">
                                    <button
                                      onClick={() =>
                                        isEditPelakuPariwisata(
                                          !editPelakuPariwisata,
                                        )
                                      }
                                      className="-ml-5"
                                    >
                                      <Icon.Edit
                                        className="text-green-500"
                                        size={16}
                                      />
                                    </button>
                                  </div>
                                </td>
                              </>
                            ) : null}
                          </tr>
                        ))}
                      </>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* END OF PELAKU PARIWISATA */}
        {/* PELAKU EKONOMI KREATIF */}
        <Widget title="">
          <p className="font-semibold text-lg">Widget Pelaku Ekonomi Kreatif</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thPelaku.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {pelakuKreatif &&
                pelakuKreatif.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editpelakuKreatif ? (
                      <>
                        {el.industry_type.map((subel, i) => (
                          <tr key={i} className="border-b-2">
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.industry_type_id}</p>
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.industry_type_en}</p>
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <p key={i}>{subel.count} %</p>
                              </div>
                            </td>
                            {i === 0 ? (
                              <>
                                <td rowSpan={thPelaku.length}>{el.rate}</td>
                                <td rowSpan={thPelaku.length}>{el.count}</td>
                                <td rowSpan={thPelaku.length}>
                                  <div className="flex justify-center">
                                    <button
                                      onClick={() => {
                                        isEditPelakuKreatif(!editpelakuKreatif);
                                        setCookies(
                                          "url",
                                          "pelaku-ekonomi-kreatifs",
                                        );
                                      }}
                                      className="-ml-5"
                                    >
                                      <Icon.Edit
                                        className="text-green-500"
                                        size={16}
                                      />
                                    </button>
                                  </div>
                                </td>
                              </>
                            ) : null}
                          </tr>
                        ))}
                      </>
                    ) : (
                      <>
                        {el.industry_type.map((subel, i) => (
                          <tr key={i} className="border-b-2">
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`tipeID-${i + 1}`}
                                  defaultValue={subel.industry_type_id}
                                />
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`tipeEN-${i + 1}`}
                                  defaultValue={subel.industry_type_en}
                                />
                              </div>
                            </td>
                            <td className="border">
                              <div className="flex items-center">
                                <input
                                  className="px-3 w-1/2 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                  ref={register}
                                  type="text"
                                  name={`countArr-${i + 1}`}
                                  defaultValue={subel.count}
                                />
                              </div>
                            </td>

                            {i === 0 ? (
                              <>
                                <td rowSpan={thPelaku.length}>
                                  <input
                                    className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                    ref={register}
                                    type="text"
                                    name={`rate`}
                                    defaultValue={el.rate}
                                  />
                                </td>
                                <td rowSpan={thPelaku.length}>
                                  <form
                                    onSubmit={handleSubmit(onSubmit)}
                                    className="flex"
                                  >
                                    <input
                                      className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                      ref={register}
                                      type="text"
                                      name={`count`}
                                      defaultValue={el.count}
                                    />
                                    <button
                                      type="submit"
                                      className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                                    >
                                      Submit
                                    </button>
                                  </form>
                                </td>
                                <td rowSpan={thPelaku.length}>
                                  <div className="flex justify-center">
                                    <button
                                      onClick={() =>
                                        isEditPelakuKreatif(!editpelakuKreatif)
                                      }
                                      className="-ml-5"
                                    >
                                      <Icon.Edit
                                        className="text-green-500"
                                        size={16}
                                      />
                                    </button>
                                  </div>
                                </td>
                              </>
                            ) : null}
                          </tr>
                        ))}
                      </>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* end of pelaku ekonomi kreatif */}
        {/* WIDGET EKONOMI KREATIF */}
        <Widget title="">
          <p className="font-semibold text-lg">Widget Ekonomi Kreatif</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thWidget.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {WidgetKreatif &&
                WidgetKreatif.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editWidgetKreatif ? (
                      <>
                        <tr className="border-b-2">
                          <td className="border">{el.rate}</td>
                          <td className="border">{el.count}</td>
                          <td className="border">{el.date}</td>
                          <td className="border">
                            <div className="">
                              <button
                                onClick={() => {
                                  isEditWidgetKreatif(!editWidgetKreatif);
                                  setCookies("url", "widget-ekonomi-kreatifs");
                                }}
                              >
                                <Icon.Edit
                                  className="text-green-500"
                                  size={16}
                                />
                              </button>
                            </div>
                          </td>
                        </tr>
                      </>
                    ) : (
                      <tr className="border-b-2">
                        <td className="border">
                          <input
                            className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`rate`}
                            defaultValue={el.rate}
                          />
                        </td>
                        <td className="border">
                          <input
                            className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`count`}
                            defaultValue={el.count}
                          />
                        </td>
                        <td className="border">
                          <form
                            onSubmit={handleSubmit(onSubmit)}
                            className="flex"
                          >
                            <div className="">
                              <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                              />
                            </div>
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </form>
                        </td>
                        <td className="border">
                          <div className="flex justify-center">
                            <button
                              onClick={() =>
                                isEditWidgetKreatif(!editWidgetKreatif)
                              }
                              className="-ml-5"
                            >
                              <Icon.Edit className="text-green-500" size={16} />
                            </button>
                          </div>
                        </td>
                      </tr>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* END OF WIDGET EKONOMI KREATIF */}
        {/* WIDGET Pariwisata */}
        <Widget title="">
          <p className="font-semibold text-lg">Widget Pariwisata</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thWidget.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {WidgetPariwisata &&
                WidgetPariwisata.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editWidgetPariwisata ? (
                      <>
                        <tr className="border-b-2">
                          <td className="border">{el.rate}</td>
                          <td className="border">{el.count}</td>
                          <td className="border">{el.date}</td>
                          <td className="border">
                            <div className="">
                              <button
                                onClick={() => {
                                  isEditWidgetPariwisata(!editWidgetPariwisata);
                                  setCookies("url", "widget-pariwisatas");
                                }}
                              >
                                <Icon.Edit
                                  className="text-green-500"
                                  size={16}
                                />
                              </button>
                            </div>
                          </td>
                        </tr>
                      </>
                    ) : (
                      <tr className="border-b-2">
                        <td className="border">
                          <input
                            className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`rate`}
                            defaultValue={el.rate}
                          />
                        </td>
                        <td className="border">
                          <input
                            className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`count`}
                            defaultValue={el.count}
                          />
                        </td>
                        <td className="border">
                          <form
                            onSubmit={handleSubmit(onSubmit)}
                            className="flex"
                          >
                            <div className="">
                              <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                              />
                            </div>
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </form>
                        </td>
                        <td className="border">
                          <div className="flex justify-center">
                            <button
                              onClick={() =>
                                isEditWidgetPariwisata(!editWidgetPariwisata)
                              }
                              className="-ml-5"
                            >
                              <Icon.Edit className="text-green-500" size={16} />
                            </button>
                          </div>
                        </td>
                      </tr>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* END OF WIDGET Pariwisata */}
        {/* WIDGET Wisatawan Mancanegara */}
        <Widget title="">
          <p className="font-semibold text-lg">Widget Wisatawan Mancanegara</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thWidget.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {WisatawanMancanegara &&
                WisatawanMancanegara.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editWisatawanMancanegara ? (
                      <>
                        <tr className="border-b-2">
                          <td className="border">{el.rate}</td>
                          <td className="border">{el.count}</td>
                          <td className="border">{el.date}</td>
                          <td className="border">
                            <div className="">
                              <button
                                onClick={() => {
                                  isEditWisatawanMancanegara(
                                    !editWisatawanMancanegara,
                                  );
                                  setCookies(
                                    "url",
                                    "widget-wisatawan-mancanegaras",
                                  );
                                }}
                              >
                                <Icon.Edit
                                  className="text-green-500"
                                  size={16}
                                />
                              </button>
                            </div>
                          </td>
                        </tr>
                      </>
                    ) : (
                      <tr className="border-b-2">
                        <td className="border">
                          <input
                            className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`rate`}
                            defaultValue={el.rate}
                          />
                        </td>
                        <td className="border">
                          <input
                            className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`count`}
                            defaultValue={el.count}
                          />
                        </td>
                        <td className="border">
                          <form
                            onSubmit={handleSubmit(onSubmit)}
                            className="flex"
                          >
                            <div className="">
                              <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                              />
                            </div>
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </form>
                        </td>
                        <td className="border">
                          <div className="flex justify-center">
                            <button
                              onClick={() =>
                                isEditWisatawanMancanegara(
                                  !editWisatawanMancanegara,
                                )
                              }
                              className="-ml-5"
                            >
                              <Icon.Edit className="text-green-500" size={16} />
                            </button>
                          </div>
                        </td>
                      </tr>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* END OF WIDGET Wisatawan Mancanegara */}
        {/* WIDGET Wisatawan Nusantara */}
        <Widget title="">
          <p className="font-semibold text-lg">Widget Wisatawan Nusantara</p>
          <table className="table-lg table mt-5">
            <thead>
              <tr>
                {thWidget.map((el, i) => (
                  <th key={i} className="border">
                    <h2 className="text-sm font-bold">{el}</h2>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {WisatawanNusantara &&
                WisatawanNusantara.map((el, ind) => (
                  <Fragment key={ind}>
                    {!editWisatawanNusantara ? (
                      <>
                        <tr className="border-b-2">
                          <td className="border">{el.rate}</td>
                          <td className="border">{el.count}</td>
                          <td className="border">{el.date}</td>
                          <td className="border">
                            <div className="">
                              <button
                                onClick={() => {
                                  isEditWisatawanNusantara(
                                    !editWisatawanNusantara,
                                  );
                                  setCookies(
                                    "url",
                                    "widget-wisatawan-nusantaras",
                                  );
                                }}
                              >
                                <Icon.Edit
                                  className="text-green-500"
                                  size={16}
                                />
                              </button>
                            </div>
                          </td>
                        </tr>
                      </>
                    ) : (
                      <tr className="border-b-2">
                        <td className="border">
                          <input
                            className="px-3 w-4/5 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`rate`}
                            defaultValue={el.rate}
                          />
                        </td>
                        <td className="border">
                          <input
                            className="px-3 py-1 w-4/5 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name={`count`}
                            defaultValue={el.count}
                          />
                        </td>
                        <td className="border">
                          <form
                            onSubmit={handleSubmit(onSubmit)}
                            className="flex"
                          >
                            <div className="">
                              <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                              />
                            </div>
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </form>
                        </td>
                        <td className="border">
                          <div className="flex justify-center">
                            <button
                              onClick={() =>
                                isEditWisatawanNusantara(
                                  !editWisatawanNusantara,
                                )
                              }
                              className="-ml-5"
                            >
                              <Icon.Edit className="text-green-500" size={16} />
                            </button>
                          </div>
                        </td>
                      </tr>
                    )}
                  </Fragment>
                ))}
            </tbody>
          </table>
        </Widget>
        {/* END OF WIDGET Wisatawan Mancanegara */}
      </Layout>
    </>
  );
};

AllWidget.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resPelakuKreatif = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/pelaku-ekonomi-kreatifs`,
  );
  const resPelakuPariwisata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/pelaku-pariwisatas`,
  );
  const resWidgetKreatif = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/widget-ekonomi-kreatifs`,
  );
  const resWisatawanNusantara = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/wisatawan-nusantaras`,
  );
  const resWisatawanMancanegara = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/wisatawan-mancanegaras`,
  );
  const resWidgetPariwisata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/widget-pariwisatas`,
  );

  const pelakuKreatif = await resPelakuKreatif.json();
  const PelakuPariwisata = await resPelakuPariwisata.json();
  const WidgetKreatif = await resWidgetKreatif.json();
  const WisatawanNusantara = await resWisatawanNusantara.json();
  const WisatawanMancanegara = await resWisatawanMancanegara.json();
  const WidgetPariwisata = await resWidgetPariwisata.json();

  return {
    pelakuKreatif,
    PelakuPariwisata,
    WidgetKreatif,
    WisatawanNusantara,
    WisatawanMancanegara,
    WidgetPariwisata,
    isAuthenticated,
    isAuthor,
    isContributor,
  };
};

export default withRedux(AllWidget);
