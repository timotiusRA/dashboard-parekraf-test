import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import Link from "next/link";
import TableMedia from "../../components/tables/media";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import Widget from "../../components/widget";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";

const Ppt = ({
  mediaImg,
  start,
  limit,
  isAuthenticated,
  page,
  numberOfArticles,
}) => {
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {
    fetchData(input);
  }, [role, start, page, limit]);

  const lastPage = Math.ceil(numberOfArticles / limit);

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files?ext=.pptx&ext=.ppt&name_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.pptx&ext=.ppt&name_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <div className="flex justify-between">
          <SectionTitle title="MEDIA" subtitle="" />
          <Link href="/media/upload-media">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Data
            </button>
          </Link>
        </div>
        <Widget title="">
          <div className="flex justify-between mb-5">
            <span className="text-lg font-semibold -mb-5">Daftar PPT</span>
            <div className="flex">
              <TotalEntries url={"/media/images"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <div className=" flex-wrap w-full mb-4">
            <TableMedia
              data={input === "" ? mediaImg : searchData}
              token={token}
              type={"presentasi"}
            />
          </div>
          <Pagination
            url={"/media/ppt"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </div>
    </Layout>
  );
};

Ppt.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);

  const mediaImage = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files?ext=.pptx&ext=.ppt&_limit=${limit}&_start=${start}`,
  );

  const mediaImg = await mediaImage.json();

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.pptx&ext=.ppt`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    isAuthenticated,
    mediaImg,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(Ppt);
