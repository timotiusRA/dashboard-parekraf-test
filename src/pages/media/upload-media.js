import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, checkContributor } from "../../lib/actions";
import { UseContributor } from "../../lib/customHooks/useRole";
import Widget from "../../components/widget";

const UploadMedia = ({ isAuthenticated, isContributor }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    const formData = new FormData();
    formData.append("files", data.files[0]);
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/media/all-media");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="MEDIA" subtitle="" />
        <Widget title="Upload Media">
          <form onSubmit={handleSubmit(onSubmit)}>
            <input ref={register} type="file" name="files" />
            <button className="hover:bg-white border border-primaryNavy hover:text-primaryNavy my-3 bg-primaryNavy text-white rounded-lg p-2 text-1rem">
              Submit
            </button>
          </form>
          <p>* Maximum size 300MB</p>
          <p>
            * File type (JPG / PNG / PDF / DOC / DOCX / XLS / XLSX / PPT / PPTX
            / RAR / ZIP / MP3 / MP4)
          </p>
        </Widget>
      </div>
    </Layout>
  );
};
UploadMedia.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isContributor } = checkContributor(ctx);

  return {
    isAuthenticated,
    isContributor,
  };
};

export default withRedux(UploadMedia);
