import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import TableMedia from "../../components/tables/media";
import { NotificationManager } from "react-notifications";
import Link from "next/link";
import * as Icon from "react-feather";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, checkContributor } from "../../lib/actions";
import { UseContributor } from "../../lib/customHooks/useRole";

const AllMedia = ({
  isAuthenticated,
  ppt,
  rar,
  data,
  mediaImg,
  pdf,
  doc,
  audio,
  isContributor,
}) => {
  const categories = [
    {
      info: `Image ${mediaImg} data`,
      href: "/media/images",
      icon: <Icon.Film size={50} />,
    },
    {
      info: `PDF ${pdf} data`,
      href: "/media/pdf",
      icon: <Icon.Book size={50} />,
    },
    {
      info: `Doc ${doc} data`,
      href: "/media/doc",
      icon: <Icon.FileText size={50} />,
    },
    {
      info: `Audio ${audio} data`,
      href: "/media/audio",
      icon: <Icon.Mic size={50} />,
    },
    {
      info: `PPT ${ppt} data`,
      href: "/media/ppt",
      icon: <Icon.Radio size={50} />,
    },
    {
      info: `RAR ${rar} data`,
      href: "/media/rar",
      icon: <Icon.Paperclip size={50} />,
    },
  ];
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);
  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="MEDIA" subtitle="" />
        <div className=" flex-wrap w-full mb-4">
          <div className="grid grid-cols-6 gap-10">
            {categories.map((el, i) => (
              <Link key={i} href={el.href}>
                <button className="col-span-2 bg-primaryNavy border border-white text-xl rounded-lg p-20 text-white">
                  <div className="mb-5 flex justify-center">{el.icon}</div>
                  {el.info}
                </button>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
};

AllMedia.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files`,
  );

  const mediaImage = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.svg&ext=.png&ext=.jpg&ext=.jpeg`,
  );
  const mediaPdf = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.pdf`,
  );
  const mediaDoc = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.doc&ext=.docs&ext=.docx`,
  );
  const mediaAudio = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.mp3&ext=.mp4`,
  );
  const mediaPpt = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.ppt&ext=.pptx`,
  );
  const mediaRar = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/count?ext=.rar&ext=.zip`,
  );

  const rar = await mediaRar.json();
  const ppt = await mediaPpt.json();
  const mediaImg = await mediaImage.json();
  const audio = await mediaAudio.json();
  const pdf = await mediaPdf.json();
  const doc = await mediaDoc.json();
  const data = await resdata.json();

  if (rar === undefined) rar = 0;
  if (ppt === undefined) ppt = 0;
  if (mediaImg === undefined) mediaImg = 0;
  if (audio === undefined) audio = 0;
  if (pdf === undefined) pdf = 0;
  if (doc === undefined) doc = 0;

  return {
    audio,
    rar,
    ppt,
    data,
    pdf,
    doc,
    mediaImg,
    isAuthenticated,
    isContributor,
  };
};

export default withRedux(AllMedia);
