import React, { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import { NotificationManager } from "react-notifications";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Pagination from "../../components/tables/pagination";
import TableEvent from "../../components/tables/tableEvent";
import Link from "next/link";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const Events = ({
  data,
  isAuthenticated,
  isContributor,
  isAuthor,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);

      setTimeout(function () {
        router.push("/event/all-event");
      }, 1000);
      didDeleted(false);
    }
  }, [role, deleted, start, page, limit]);

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/events/` + id, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  const editItem = async (id) => {
    setCookies("event_id", id);
    router.push("/event/edit-event");
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/events?name_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/events/count?name_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="flex justify-between">
        <SectionTitle title="EVENTS" subtitle="" />
        <Link href="/event/add-event">
          <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
            Tambah Data
          </button>
        </Link>
      </div>
      <Widget title="">
        <div className="flex justify-between mb-5">
          <span className="text-lg font-semibold">Daftar Events</span>
          <div className="flex">
            <TotalEntries url={"/event/all-event"} limit={limit} />
            <SearchBar updateInput={updateInput} input={input} />
          </div>
        </div>
        <TableEvent
          data={input === "" ? data : searchData}
          editItem={editItem}
          deleteItem={deleteItem}
        />
        <Pagination
          url={"/event/all-event"}
          lastPage={input === "" ? lastPage : pageSearch}
          page={page}
          limit={limit}
        />
      </Widget>
    </Layout>
  );
};

Events.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/events?_limit=${limit}&_start=${start}`,
  );

  const data = await resdata.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/events/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    data,
    isAuthenticated,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(Events);
