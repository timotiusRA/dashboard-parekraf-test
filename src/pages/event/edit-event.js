import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import WidgetTitle from "../../components/widget-title";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../../lib/actions";
import InputEvent from "../../components/tables/inputEvent";
const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const monthsID = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "Nopember",
  "Desember",
];

const EditEvent = ({ event, isAuthenticated, cities }) => {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [city, setCity] = useState(event && event.city && event.city.id);

  const { register, handleSubmit } = useForm();

  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["city"] = city;

    if (startDate) {
      data["date"] = startDate;
      data["start_date"] = startDate;
    }
    if (endDate) {
      data["end_date"] = endDate;
    }

    const awalEN = months[startDate.getMonth()];
    const awalID = monthsID[startDate.getMonth()];
    const akhirEN = months[endDate.getMonth()];
    const akhirID = monthsID[endDate.getMonth()];

    data[
      "duration_id"
    ] = `${startDate.getDate()} ${awalID} ${startDate.getFullYear()} - ${endDate.getDate()} ${akhirID} ${endDate.getFullYear()}`;
    data[
      "duration_en"
    ] = `${awalEN} ${startDate.getDate()}, ${startDate.getFullYear()} - ${akhirEN} ${endDate.getDate()}, ${endDate.getFullYear()}`;

    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files"];
    }
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/events/` + getCookies("event_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/event/all-event");
        }, 1000);
      })
      .catch((err) => {
        NotificationManager.error("Error", 5000);
      });
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title="Edit Event" subtitle="" />
        <WidgetTitle title={"Edit Event"} />
        <InputEvent
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          register={register}
          event={event}
          startDate={startDate}
          setStartDate={setStartDate}
          endDate={endDate}
          setEndDate={setEndDate}
          cities={cities}
          setCity={setCity}
          city={city}
        />
      </div>
    </Layout>
  );
};

EditEvent.getInitialProps = async (ctx) => {
  let event;
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/events/` +
      getCookies("event_id", ctx.req),
  );
  event = await resdata.json();

  const rescities = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/cities?_start=0&_limit=514`,
  );

  const cities = await rescities.json();

  return {
    event,
    isAuthenticated,
    cities,
  };
};

export default withRedux(EditEvent);
