import { useEffect } from "react";
import { withRedux } from "../lib/redux";
import Layout from "../layouts";
import SectionTitle from "../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import useAuth from "../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../lib/actions";
import { useRouter } from "next/router";

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

const AlertBanner = ({ article, isAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );

  useAuth(isAuthenticated);
  useEffect(() => {}, [role, article]);

  const onSubmit = async (data) => {
    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/alert-banners/2`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/alert-banner");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  const deleteFunc = async () => {
    const data = {
      content_id: null,
      content_en: null,
      link: null,
    };
    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/alert-banners/2`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Dihapus", 3000);
        router.push("/");
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <div className="flex justify-between">
          <SectionTitle title="EDIT ALERT BANNER" subtitle="" />
          <button
            onClick={() => deleteFunc()}
            className="mb-5 bg-primaryNavy text-white rounded-lg p-3"
          >
            Hapus Alert Banner
          </button>
        </div>

        <div className=" flex-wrap w-full mb-4">
          <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
            <div className="grid grid-cols-6 mr-8">
              <div className="col-span-6">
                <label className="m-2">Content (ID)</label>
                <input
                  className={`${style}`}
                  ref={register}
                  type="text"
                  name="content_id"
                  defaultValue={article && article.content_id}
                />
              </div>
              <div className="col-span-6">
                <label className="m-2 ">Content (EN)</label>
                <input
                  className={`${style} `}
                  ref={register}
                  type="text"
                  name="content_en"
                  defaultValue={article && article.content_en}
                />
              </div>
            </div>
            <div className="grid grid-cols-6 mr-8">
              <div className="col-span-6">
                <label className="m-2">URL (Link)</label>
                <input
                  className={`${style}`}
                  ref={register}
                  type="text"
                  name="link"
                  defaultValue={article && article.link}
                />
              </div>
            </div>

            <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
              Submit
            </button>
          </form>
        </div>
      </div>
    </Layout>
  );
};

AlertBanner.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  let article;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/alert-banners/2`,
  );
  article = await resdata.json();

  return {
    article,
    isAuthenticated,
  };
};

export default withRedux(AlertBanner);
