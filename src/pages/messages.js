import { useEffect } from "react";
import moment from "moment";
import id from "moment/locale/id";
import Layout from "../layouts";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../lib/actions";
import { withRedux } from "../lib/redux";
import useAuth from "../lib/customHooks/useAuth";
import Pagination from "../components/tables/pagination";
import SectionTitle from "../components/section-title";
import Widget from "../components/widget";
import TotalEntries from "../components/tables/totalEntries";
import { CSVLink } from "react-csv";

const Messages = ({
  data,
  isAuthenticated,
  page,
  numberOfArticles,
  limit,
  start,
  csvData,
}) => {
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role, start, page, limit]);

  const lastPage = Math.ceil(numberOfArticles / limit);

  return (
    <>
      <Layout>
        <SectionTitle title="MESSAGES" subtitle="" />
        <div className="flex justify-between mb-5">
          <button className="bg-primaryNavy rounded-md text-white px-5 mb-3">
            <CSVLink data={csvData}>Download CSV</CSVLink>
          </button>
          <TotalEntries url={"/messages"} limit={limit} />
        </div>
        <Widget title="Daftar Pesan">
          <table className="table-fixed mt-10">
            <thead>
              <tr>
                <th className="border w-1/12">
                  <h2 className="text-sm font-bold">No</h2>
                </th>
                <th className="border w-1/12">
                  <h2 className="text-sm font-bold">Tanggal</h2>
                </th>
                <th className="border w-2/12">
                  <h2 className="text-sm font-bold">Nama Lengkap</h2>
                </th>
                <th className="border w-2/12">
                  <h2 className="text-sm font-bold">Email</h2>
                </th>
                <th className="border w-2/12">
                  <h2 className="text-sm font-bold">Subjek</h2>
                </th>
                <th className="border w-4/12">
                  <h2 className="text-sm font-bold">Pesan</h2>
                </th>
              </tr>
            </thead>
            <tbody>
              {data.map((el, i) => (
                <tr key={i}>
                  <td className="border py-5">
                    <div className="flex items-center">
                      {i + 1 + (page - 1) * 10}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(el.created_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.full_name && el.full_name}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.email && el.email}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.subject && el.subject}
                    </div>
                  </td>
                  <td className="border break-words">
                    <div className="flex items-center break-words">
                      {el.message && el.message}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Pagination
            url={"/messages"}
            lastPage={lastPage}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

Messages.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/messages?_limit=${limit}&_start=${start}`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );

  const data = await resdata.json();
  const resdataAll = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/messages`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );

  const dataAll = await resdataAll.json();
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/messages/count`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  const csvData = [["firstname", "email", "subject", "message", "date"]];
  dataAll.forEach((el) => {
    let arr = [];
    arr.push(el.full_name);
    arr.push(el.email);
    arr.push(el.subject);
    arr.push(el.message);
    arr.push(el.created_at);

    csvData.push(arr);
  });

  return {
    data,
    isAuthenticated,
    page: +page,
    numberOfArticles,
    csvData,
    limit: +limit,
    start,
  };
};

export default withRedux(Messages);
