import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import Widget from "../../components/widget";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import { useSelector, shallowEqual } from "react-redux";
import { getCookies, setCookies } from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import TableKategoriJabatan from "../../components/tables/tableKategoriJabatan";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const AllCategories = ({
  categoryData,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [isEdit, showIsEdit] = useState(categoryData);
  const [deleted, didDeleted] = useState(false);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("category_jabatan_id", el.id);
    showIsEdit([...isEdit]);
  };

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/profil/kategori-jabatan");
      }, 1000);

      didDeleted(false);
    }
  }, [deleted, role, start, page, limit]);

  const onSubmit = async (data) => {
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions/` +
          getCookies("category_jabatan_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil diubah", 3000);
        router.push("/profil/kategori-jabatan");
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };
  const onSubmitPost = async (data) => {
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions/`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil ditambahkan", 3000);
        router.push("/profil/kategori-jabatan");
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };
  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(
          `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions/` + id,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions?divisi_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    let resData = await res.json();
    setSearchData(resData);
    resData.forEach((el) => (el.isEdit = false));

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions/count?divisi_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="KATEGORI JABATAN" subtitle="" />
        <div className="mb-10">
          <p className="text-lg my-5">Add new category</p>
          <form onSubmit={handleSubmit(onSubmitPost)} className="flex">
            <div>
              <p>Kategori Jabatan (ID)</p>
              <input
                className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                id="nama_id"
                type="text"
                aria-label="nama_en"
                name="divisi_id"
                ref={register({ required: true })}
              />
            </div>
            <div className="mx-5 ">
              <p>Kategori Jabatan (EN)</p>
              <input
                className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                id="category_en"
                type="text"
                aria-label="nama_en"
                name="divisi_en"
                ref={register({ required: true })}
              />
            </div>
            <div className="mx-5 ">
              <p>Urutan</p>
              <input
                className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                id="category_en"
                type="text"
                aria-label="nama_en"
                name="level"
                ref={register({ required: true })}
              />
            </div>
            <button
              type="submit"
              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
            >
              Submit
            </button>
          </form>
        </div>
        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">
              Kategori Jabatan
            </span>
            <div className="flex">
              <TotalEntries url={"/categories/all-categories"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <div className=" flex-wrap w-full mb-4 mt-5">
            <TableKategoriJabatan
              categoryData={input === "" ? categoryData : searchData}
              handleOnClick={handleOnClick}
              handleSubmit={handleSubmit}
              deleteItem={deleteItem}
              register={register}
              onSubmit={onSubmit}
            />
          </div>
          <Pagination
            url={"/categories/all-categories"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </div>
    </Layout>
  );
};

AllCategories.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const category = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions?_limit=${limit}&_start=${start}`,
  );

  const categoryData = await category.json();
  categoryData.forEach((el) => (el.isEdit = false));

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    categoryData,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(AllCategories);
