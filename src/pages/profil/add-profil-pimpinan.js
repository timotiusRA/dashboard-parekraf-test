import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import InputProfilPimpinan from "../../components/tables/inputProfilPimpinan";

const AddProfilPimpinan = ({ divisi, isAuthenticated }) => {
  const [htmlEN, setHtmlEN] = useState("");
  const [htmlID, setHtmlID] = useState("");
  const [eksDivisi, setEksDivisi] = useState("");
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["linkUrl"] = res.data[0].url;
        })
        .catch((err) => NotificationManager.error("File gagal diupload", 5000));
      delete data["files"];
    }

    if (data["files-img"] && data["files-img"].length > 0) {
      const formData = new FormData();
      formData.append("files", data["files-img"][0]);

      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["photo"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files-img"];
    }

    if (eksDivisi) data["executive_division"] = eksDivisi;
    if (htmlID) data["description_id"] = htmlID;
    if (htmlEN) data["description_en"] = htmlEN;

    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/executives`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/profil/profil-pimpinan");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="ADD PROFIL PIMPINAN" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputProfilPimpinan
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
            divisi={divisi}
            setEksDivisi={setEksDivisi}
          />
        </div>
      </div>
    </Layout>
  );
};

AddProfilPimpinan.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/executive-divisions`,
  );
  const divisi = await resdata.json();

  return {
    divisi,
    isAuthenticated,
  };
};

export default withRedux(AddProfilPimpinan);
