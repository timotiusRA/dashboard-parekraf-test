import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import dynamic from "next/dynamic";
import * as Icon from "react-feather";

const Editor = dynamic(() => import("../../components/tables/editor"), {
  ssr: false,
});

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

const ProfilLembaga = ({ article, isAuthenticated }) => {
  const [htmlEN, setHtmlEN] = useState(article && article.content_en);
  const [htmlID, setHtmlID] = useState(article && article.content_id);
  const { register, handleSubmit } = useForm();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );

  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (htmlEN || htmlID) {
      data["content_en"] = htmlEN;
      data["content_id"] = htmlID;
    }

    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/profil-lembagas/1`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", 3000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <div className="flex justify-between">
          <SectionTitle title="EDIT PROFIL LEMBAGA" subtitle="" />
          <a
            target="_blank"
            href="https://kemenparekraf.go.id/profil/profil-lembaga"
          >
            <button className="flex">
              <Icon.Eye size={30} className="text-gold1" />
              <span className="mt-1 ml-3"> See Page</span>
            </button>
          </a>
        </div>

        <div className=" flex-wrap w-full mb-4">
          <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
            <div className="grid grid-cols-6 mr-3">
              <div className="col-span-6">
                <label className="m-2">Title (ID)</label>
                <input
                  className={`${style}`}
                  ref={register}
                  type="text"
                  name="title_id"
                  defaultValue={article && article.title_id}
                />
              </div>
              <div className="col-span-6">
                <label className="m-2 ">Title (EN)</label>
                <input
                  className={`${style} `}
                  ref={register}
                  type="text"
                  name="title_en"
                  defaultValue={article && article.title_en}
                />
              </div>
            </div>

            <label className="m-2">Content (ID)</label>
            <Editor onChange={setHtmlID} value={htmlID} />
            <label className="m-2">Content (EN)</label>
            <Editor onChange={setHtmlEN} value={htmlEN} />
            <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
              Submit
            </button>
          </form>
        </div>
      </div>
    </Layout>
  );
};

ProfilLembaga.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  let article;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/profil-lembagas/1`,
  );
  article = await resdata.json();
  return {
    article,
    isAuthenticated,
  };
};

export default withRedux(ProfilLembaga);
