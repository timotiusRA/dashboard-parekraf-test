import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Pagination from "../../components/tables/pagination";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import TableProfilPimpinan from "../../components/tables/tableProfilPimpinan";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import Link from "next/link";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import { confirm } from "../../components/tables/confirm";

const ProfilPimpinan = ({
  dataLink,
  page,
  numberOfArticles,
  isAuthenticated,
  isContributor,
  isAuthor,
  start,
  limit,
}) => {
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);
  useEffect(() => {
    fetchData(input);
  }, [role, start, page, limit]);

  const handleOnClick = (id) => {
    setCookies("profil_pimpinan_id", id);
    router.push("/profil/edit-profil-pimpinan");
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/executives?name_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/executives/count?name_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/executives/` + id, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          NotificationManager.info("Berhasil didelete", null, 500);
          setTimeout(function () {
            router.push("/profil/profil-pimpinan");
          }, 1000);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
    }
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle title={"Profil Pimpinan".toUpperCase()} subtitle="" />
          <Link href="/profil/add-profil-pimpinan">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Data
            </button>
          </Link>
        </div>

        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">
              Daftar Profil Pimpinan
            </span>
            <div className="flex">
              <TotalEntries url={"/profil/profil-pimpinan"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableProfilPimpinan
            dataLink={input === "" ? dataLink : searchData}
            page={page}
            handleOnClick={handleOnClick}
            deleteItem={deleteItem}
          />

          <Pagination
            url={"/profil/profil-pimpinan"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

ProfilPimpinan.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/executives?_limit=${limit}&_start=${start}`,
  );

  const dataLink = await resdata.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/executives/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    isAuthenticated,
    dataLink,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(ProfilPimpinan);
