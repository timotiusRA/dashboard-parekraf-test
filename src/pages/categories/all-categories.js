import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import Widget from "../../components/widget";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  getCookies,
  setCookies,
} from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import TableKategoriArtikel from "../../components/tables/tableKategoriArtikel";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";

const AllCategories = ({
  categoryData,
  isAuthenticated,
  isAuthor,
  isContributor,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [isEdit, showIsEdit] = useState(categoryData);
  const [deleted, didDeleted] = useState(false);
  const [kategoriID, setKategoriID] = useState("");
  const [kategoriEN, setKategoriEN] = useState("");

  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("category_id", el.id);
    showIsEdit([...isEdit]);
  };

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/categories/all-categories");
      }, 1000);

      didDeleted(false);
    }
  }, [deleted, role, start, page, limit]);

  const onSubmit = async (data) => {
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories/` +
          getCookies("category_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil diubah", null, 500);
        setTimeout(function () {
          router.push("/categories/all-categories");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };
  const onSubmitPost = async (data) => {
    data["category_id"] = kategoriID;
    data["category_en"] = kategoriEN;

    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil ditambahkan", null, 500);
        setKategoriID("");
        setKategoriEN("");
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };
  const deleteItem = async (id) => {
    await axios
      .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories/` + id, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        didDeleted(true);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
    didDeleted(false);
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories?category_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    let resData = await res.json();
    setSearchData(resData);
    resData.forEach((el) => (el.isEdit = false));

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories/count?category_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="CATEGORIES" subtitle="" />
        <div className="mb-10">
          <p className="text-lg my-5">Add new category</p>
          <form onSubmit={handleSubmit(onSubmitPost)} className="flex">
            <div>
              <p>Kategori ID</p>
              <input
                className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                id="nama_id"
                type="text"
                aria-label="nama_en"
                name="category_id"
                value={kategoriID}
                onChange={(e) => setKategoriID(e.target.value)}
              />
            </div>
            <div className="mx-5 ">
              <p>Kategori EN</p>
              <input
                className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                id="category_en"
                type="text"
                aria-label="nama_en"
                name="category_en"
                value={kategoriEN}
                onChange={(e) => setKategoriEN(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
            >
              Submit
            </button>
          </form>
        </div>
        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">
              Kategori Artikel
            </span>
            <div className="flex">
              <TotalEntries url={"/categories/all-categories"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <div className=" flex-wrap w-full mb-4 mt-5">
            <TableKategoriArtikel
              categoryData={input === "" ? categoryData : searchData}
              handleOnClick={handleOnClick}
              handleSubmit={handleSubmit}
              deleteItem={deleteItem}
              register={register}
              onSubmit={onSubmit}
            />
          </div>
          <Pagination
            url={"/categories/all-categories"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </div>
    </Layout>
  );
};

AllCategories.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const category = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories?_limit=${limit}&_start=${start}`,
  );

  const categoryData = await category.json();
  categoryData.forEach((el) => (el.isEdit = false));

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/article-categories/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    isAuthenticated,
    categoryData,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(AllCategories);
