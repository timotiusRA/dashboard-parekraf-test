import { useEffect } from "react";
import moment from "moment";
import id from "moment/locale/id";
import { withRedux } from "../lib/redux";
import Layout from "../layouts";
import { useSelector, shallowEqual } from "react-redux";
import useAuth from "../lib/customHooks/useAuth";
import { checkServerSideCookie, getCookies } from "../lib/actions";
import Pagination from "../components/tables/pagination";
import SectionTitle from "../components/section-title";
import Widget from "../components/widget";
import { CSVLink } from "react-csv";
import TotalEntries from "../components/tables/totalEntries";

const Subscribers = ({
  data,
  isAuthenticated,
  page,
  limit,
  start,
  csvData,
  numberOfArticles,
}) => {
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role, start, page, limit]);

  const lastPage = Math.ceil(numberOfArticles / limit);

  return (
    <>
      <Layout>
        <SectionTitle title="SUBSCRIBERS" subtitle="" />
        <div className="flex justify-between mb-5">
          <button className="bg-primaryNavy rounded-md text-white px-5 mb-3">
            <CSVLink data={csvData}>Download CSV</CSVLink>
          </button>
          <TotalEntries url={"/subscribers"} limit={limit} />
        </div>
        <Widget title="Daftar Email Subscriber">
          <table className="table table-lg mt-10">
            <thead>
              <tr>
                <th className="border">
                  <h2 className="text-sm font-bold">No</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold">Email</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold">Tanggal</h2>
                </th>
              </tr>
            </thead>
            <tbody>
              {data.map((el, i) => (
                <tr key={i}>
                  <td className="border">
                    <div className="flex items-center">
                      {i + 1 + (page - 1) * 10}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.email && el.email}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(el.created_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Pagination
            url={"/subscribers"}
            limit={limit}
            lastPage={lastPage}
            page={page}
          />
        </Widget>
      </Layout>
    </>
  );
};

Subscribers.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;

  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/subscribers?_limit=${limit}&_start=${start}`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/subscribers/count`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  const data = await resdata.json();
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdataAll = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/subscribers`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );
  const dataAll = await resdataAll.json();
  const csvData = [["email", "date"]];
  dataAll.forEach((el) => {
    let arr = [];
    arr.push(el.email);
    arr.push(el.created_at);

    csvData.push(arr);
  });

  return {
    data,
    isAuthenticated,
    page: +page,
    numberOfArticles,
    csvData,
    limit: +limit,
    start,
  };
};

export default withRedux(Subscribers);
