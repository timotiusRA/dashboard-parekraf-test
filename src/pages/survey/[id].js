import { useState, useEffect } from "react";
import Layout from "../../layouts";
import { checkServerSideCookie } from "../../lib/actions";
import { withRedux } from "../../lib/redux";
import { useSelector, shallowEqual } from "react-redux";
import { getSurveyByID } from "../../lib/fetchGraphql";
import { updateData, updateSurveyQuestionCategory } from "../../lib/updateData";
import { postQuestion } from "../../lib/postData";
import CheckboxQuestion from "../../components/survey/CheckboxQuestion";
import RadioQuestion from "../../components/survey/RadioQuestion";
import TextAreaQuestion from "../../components/survey/TextareaQuestion";

const UpdateSurvey = ({ data }) => {
  const [dataSurvey, setDataSurvey] = useState(data);
  const [dataChanged, setDataChanged] = useState({});
  const [newQuestion, setNewQuestion] = useState({
    question: "",
    is_required: false,
    survey_question_type: "",
  });

  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const changeText = (event) => {
    const targetValue = event.target.value
      ? event.target.value
      : event.target.placeholder;
    setDataChanged({ ...dataChanged, [event.target.name]: targetValue });
  };

  const updateDataSurvey = async () => {
    for (const [key, value] of Object.entries(dataChanged)) {
      const [url, id, field] = key.split(":");
      const setData = { [field]: value };
      console.log(setData);
      await updateData(setData, url, token, id);
    }
    window.location.reload();
  };

  const addQuestion = async (categoryId, listQuestion) => {
    const newQuestionId = listQuestion.map((item) => parseInt(item.id));
    const newId = await postQuestion(newQuestion, token);
    newQuestionId.push(newId);
    await updateSurveyQuestionCategory(
      { question_id: { data: newQuestionId } },
      token,
      categoryId,
    );
    window.location.reload();
  };

  // console.log(dataSurvey);
  return (
    <Layout>
      <div className={`bg-white flex-row justify-center pt-10 pb-10 md:pt-24`}>
        <div
          className={`m-auto sm:mx-20 rounded-md shadow-lg pb-8 pt-8 bg-primaryNavy h-30`}
        >
          <div
            className={`mt-6 text-center text-lg sm:text-xl md:text-3xl font-semibold text-white h-20`}
          >
            <input
              name={`surveys:${dataSurvey.id}:name`}
              placeholder={dataSurvey.name}
              className="bg-primaryNavy p-1 text-center w-full"
              onChange={changeText}
            ></input>
          </div>
        </div>
        <div
          className={`m-auto sm:mx-20 text-sm font-semibold pb-8 pt-8 pl-2 pr-2`}
        >
          <textarea
            name={`surveys:${dataSurvey.id}:desc`}
            placeholder={dataSurvey.desc}
            onChange={changeText}
            className="p-1 w-full h-20"
          ></textarea>
          <div className="mt-6 text-red-600 mb-6">*Harus Diisi</div>
        </div>
        {dataSurvey.categories
          ? dataSurvey.categories.map((item, index) => {
              return (
                <div key={index}>
                  <div
                    className={`m-auto sm:mx-20 rounded-md shadow-lg pb-3 pt-3 bg-primaryNavy`}
                  >
                    <div
                      className={`text-center text-sm md:text-lg font-semibold text-white`}
                    >
                      <input
                        placeholder={item.name}
                        name={`survey-question-categories:${item.id}:name`}
                        className="bg-primaryNavy p-1 text-center w-full"
                        onChange={changeText}
                      ></input>
                    </div>
                  </div>
                  {item.question.map((q, i) => {
                    return q.survey_question_type &&
                      q.survey_question_type.name === "radio" ? (
                      <RadioQuestion
                        getData={changeText}
                        key={i}
                        data={q}
                        num={i + 1}
                        token={token}
                      />
                    ) : q.survey_question_type &&
                      q.survey_question_type.name === "checkbox" ? (
                      <CheckboxQuestion
                        key={i}
                        data={q}
                        num={i + 1}
                        getData={changeText}
                        token={token}
                      />
                    ) : q.survey_question_type &&
                      q.survey_question_type.name === "text_area" ? (
                      <TextAreaQuestion
                        key={i}
                        data={q}
                        num={i + 1}
                        getData={changeText}
                        token={token}
                      />
                    ) : (
                      ""
                    );
                  })}
                  <div
                    className={`m-auto sm:mx-20 border-blue-200 border-2 rounded-md shadow-lg p-8 mt-4 mb-5`}
                  >
                    <div className="mb-4">
                      <label>
                        <input
                          name="question"
                          type="checkbox"
                          checked={newQuestion.is_required}
                          onChange={(event) => {
                            setNewQuestion({
                              ...newQuestion,
                              is_required: event.target.checked,
                            });
                          }}
                        />
                        <span className="text-xs ml-2 text-red-500">
                          Required
                        </span>
                      </label>
                    </div>
                    <div className="mb-4">
                      <div className="text-xs">Type</div>
                      <select
                        className="border-2 border-gray-600 rounded-md w-full sm:w-1/4 mt-1"
                        value={newQuestion.survey_question_type}
                        onChange={(event) => {
                          setNewQuestion({
                            ...newQuestion,
                            survey_question_type: event.target.value,
                          });
                        }}
                      >
                        <option value="">-</option>
                        <option value="1">Radio</option>
                        <option value="2">Checkbox</option>
                        <option value="3">Text Area</option>
                      </select>
                    </div>
                    <div className="grid grid-cols-3 gap-4">
                      <div className="col-span-2">
                        <input
                          onChange={(event) => {
                            setNewQuestion({
                              ...newQuestion,
                              question: event.target.value,
                            });
                          }}
                          placeholder="Add new Question here..."
                          className={`text-sm font-semibold p-1 w-full border-2 border-light-blue-500 border-opacity-50 rounded-md`}
                        ></input>
                      </div>
                      <button
                        onClick={() => {
                          addQuestion(item.id, item.question);
                        }}
                        className={`bg-primaryNavy text-white p-1 text-sm rounded-md`}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </div>
              );
            })
          : ""}
        <button
          onClick={updateDataSurvey}
          className={`bg-red-600 text-white p-5 text-sm rounded-md shadow-md fixed top-0 right-0 mt-20 mr-3`}
        >
          Save
        </button>
      </div>
    </Layout>
  );
};
UpdateSurvey.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { id } = ctx.query;
  const data = await getSurveyByID(id);

  return { data };
};
export default withRedux(UpdateSurvey);
