import { useState, useEffect } from "react";
import Layout from "../../layouts";
import * as Icon from "react-feather";
import { checkServerSideCookie } from "../../lib/actions";
import { withRedux } from "../../lib/redux";
import { getAllSurvey } from "../../lib/fetchGraphql";
import { useSelector, shallowEqual } from "react-redux";
import { updateSurvey } from "../../lib/updateData";
import { convertToCSV } from "../../lib/convertToCSV";
import { getSurveyUserData } from "../../lib/fetchGraphql";
import { useRouter } from "next/router";

const AllSurvey = () => {
  const [dataSurvey, setDataSurvey] = useState([]);
  const [limit, setLimit] = useState(10);
  const router = useRouter();

  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const getData = () => {
    getAllSurvey(limit)
      .then((res) => {
        setDataSurvey(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deleteSurvey = async (id) => {
    const setData = {
      is_trash: true,
    };
    await updateSurvey(setData, token, id);
    getData();
  };

  const editSurvey = (id) => {
    router.push(`/survey/${id}`);
  };

  const allSurveyCSV = () => {
    const setData = [
      ["NO.", "TITLE", "AUTHOR"],
      ...dataSurvey.map((item, index) => {
        return [index + 1, item.name, item.created_by.username];
      }),
    ];
    convertToCSV(setData);
  };

  const sepcificSurveyCSV = async (questionId) => {
    const userData = await getSurveyUserData(questionId);
    const setData = [
      ["NO.", "IP", "DATE", "QUESTION", "ANSWER"],
      ...userData.map((item, index) => {
        return [
          index + 1,
          item.ip,
          item.created_at,
          item.question,
          item.answer.data.join("/"),
        ];
      }),
    ];
    convertToCSV(setData);
  };

  useEffect(() => {
    getData();
  }, [limit]);

  // console.log(dataSurvey);
  return (
    <Layout>
      <div className="font-bold text-xl">ALL SURVEY</div>
      <div className="flex justify-between mt-8 mb-8">
        <button
          className="bg-primaryNavy text-white pt-1 pb-1 pl-2 pr-2 rounded-md"
          onClick={allSurveyCSV}
        >
          Download CSV
        </button>
        <div className="w-auto md: w-1/4">
          <div className="font-semibold text-xs">TOTAL ENTIRES</div>
          <select
            className="border-2 border-gray-600 rounded-md w-full"
            onChange={(event) => {
              setLimit(event.target.value);
            }}
          >
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
      <table className="table-fixed border-collapse border border-gray-400 w-full">
        <thead>
          <tr>
            <th className="w-1/12 border border-gray-400 p-2">NO.</th>
            <th className="w-5/12 border border-gray-400">TITLE</th>
            <th className="w-4/12 border border-gray-400">AUTHOR</th>
            <th className="w-2/12 border border-gray-400">ACTION</th>
          </tr>
        </thead>
        <tbody>
          {dataSurvey.map((item, index) => {
            return (
              <tr className="mt-4" key={index}>
                <td className="text-center border border-gray-400 font-semibold">
                  {index + 1}
                </td>
                <td className="border border-gray-400 flex justify-between">
                  <p className="break-words p-1">{item.name}</p>
                  <button
                    className={`${
                      item.status ? "bg-green-500" : "bg-red-500"
                    } text-white p-1 rounded-md`}
                    style={{ fontSize: "10px" }}
                  >
                    {item.status ? "Published" : "Draft"}
                  </button>
                </td>
                <td className="text-center border border-gray-400">
                  {item.created_by.username
                    ? item.created_by.username
                    : "undefined"}
                </td>
                <td className="text-center border border-gray-400">
                  <div className="flex justify-around">
                    <Icon.Edit
                      className="text-green-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        editSurvey(item.id);
                      }}
                    />
                    <Icon.Trash
                      className="text-red-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        deleteSurvey(item.id);
                      }}
                    />
                    <Icon.Download
                      className="text-blue-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        sepcificSurveyCSV(
                          item.question_category_id
                            ? item.question_category_id.data
                            : false,
                        );
                      }}
                    />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </Layout>
  );
};
AllSurvey.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {};
};
export default withRedux(AllSurvey);
