import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import * as Icon from "react-feather";
import Layout from "../../layouts";
import Cookies from "js-cookie";
import { checkServerSideCookie } from "../../lib/actions";
import AddQuestion from "../../components/survey-question";
import SaveSurvey from "../../components/save-survey";

const AddSurvey = () => {
  const [sectionData, setSectionData] = useState([]);
  const [userId, setUserId] = useState(null);
  const [category, setCategory] = useState([]);
  const [tmpCategory, setTmpCategory] = useState("");
  const [editSection, setEditSection] = useState(false);

  const [toggle, setToggle] = useState(false);
  const [surveyData, setSurveyData] = useState({});

  const removeCategory = (item) => {
    setCategory(category.filter((e) => e !== item));
  };

  const getSectionQuestion = (data, section) => {
    if (category.length > 0) {
      setSectionData([...sectionData, data]);
      const newCategory = category;
      newCategory.splice(0, 1);
      setCategory(newCategory);
      setToggle(!toggle);
    }
  };

  const changeText = (event) => {
    setSurveyData({ ...surveyData, [event.target.name]: event.target.value });
  };

  useEffect(() => {
    setUserId(Cookies.get("kpkfadmid"));
    setSurveyData({
      name: "",
      desc: "",
      start_date: null,
      end_date: null,
      is_trash: false,
      status: true,
      created_by: Cookies.get("kpkfadmid"),
    });
  }, []);

  // console.log(surveyData);
  // console.log(sectionData, "section");

  return (
    <Layout>
      {!editSection ? (
        <div>
          <div className="font-bold text-xl mt-3 mb-5">ADD Survey</div>
          <div className="m-2 rounded-md shadow-lg border-blue-200 border-2 p-4">
            <div className="w-full mb-4 mt-5">
              <label className="block">
                <span className="text-default">Title</span>
                <input
                  onChange={(event) => {
                    changeText(event);
                  }}
                  name="name"
                  type="text"
                  className="form-input mt-1 text-xs block w-full bg-white"
                  placeholder="Enter survey title"
                />
              </label>
            </div>
            <div className="w-full mb-4">
              <label className="block">
                <span className="text-default">Description</span>
                <input
                  onChange={(event) => {
                    changeText(event);
                  }}
                  name="desc"
                  type="text"
                  className="form-input mt-1 text-xs block w-full bg-white"
                  placeholder="Enter survey description"
                />
              </label>
            </div>
            <div className="w-full mb-4">
              <label className="block">
                <span className="text-default">Start date</span>
                <input
                  onChange={(event) => {
                    changeText(event);
                  }}
                  name="start_date"
                  type="date"
                  className="form-input mt-1 text-xs block w-full bg-white"
                  placeholder="Enter survey description"
                />
              </label>
            </div>
            <div className="w-full mb-4">
              <label className="block">
                <span className="text-default">End date</span>
                <input
                  onChange={(event) => {
                    changeText(event);
                  }}
                  name="end_date"
                  type="date"
                  className="form-input mt-1 text-xs block w-full bg-white"
                  placeholder="Enter survey description"
                />
              </label>
            </div>
            <div className="w-full mb-4">
              <label className="block">
                <div className="text-default">Section</div>
                <input
                  name="category"
                  type="text"
                  className="form-input mt-1 w-10/12 text-xs bg-white"
                  onChange={(event) => {
                    setTmpCategory(event.target.value);
                  }}
                  placeholder="Enter survey section"
                />
                <button
                  className="bg-blue-800 text-white p-2 rounded-md ml-2"
                  onClick={() => {
                    setCategory([...category, tmpCategory]);
                  }}
                >
                  Add
                </button>
              </label>
            </div>
            {category.map((item, index) => {
              return (
                <button
                  className="bg-blue-600 text-white p-2 rounded-md ml-3 flex mb-2"
                  key={index}
                >
                  {item}
                  <Icon.Trash
                    className="text-white-500 cursor-pointer ml-2"
                    onClick={() => {
                      removeCategory(item);
                    }}
                    size={16}
                  />
                </button>
              );
            })}
            {category.length > 0 ? (
              <button
                className="bg-green-800 text-white p-2 rounded-md mt-3 w-full"
                onClick={() => {
                  setEditSection(true);
                }}
              >
                Continue
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
      ) : editSection && category.length > 0 ? (
        <AddQuestion
          userId={userId}
          section={category[0]}
          key={`${toggle}`}
          getSectionQuestion={getSectionQuestion}
        />
      ) : (
        <SaveSurvey
          surveyData={surveyData}
          sectionData={sectionData}
          userId={userId}
        />
      )}
    </Layout>
  );
};
AddSurvey.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {};
};
export default withRedux(AddSurvey);
