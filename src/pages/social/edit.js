import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import * as Icon from "react-feather";
import { useForm } from "react-hook-form";
import moment from "moment";
import id from "moment/locale/id";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import Widget from "../../components/widget";
import { route } from "next/dist/next-server/server/router";

const Youtube = ({ sosmedData, isAuthenticated }) => {
  const [editYoutube, showEditYoutube] = useState(false);
  const [editFacebook, showEditFacebook] = useState(false);
  const [editTwitter, showEditTwitter] = useState(false);
  const [editInstagram, showEditInstagram] = useState(false);
  const [editInstagramPhoto, showEditInstagramPhoto] = useState(false);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data = { Instagram: res.data[0] };
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
    }
    await axios
      .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/social-medias/1`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        showEditFacebook(false);
        showEditYoutube(false);
        showEditInstagramPhoto(false);
        showEditInstagram(false);
        showEditTwitter(false);
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/social/edit");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="SOCIAL MEDIA" subtitle="" />
        <div className=" flex-wrap w-full mb-4">
          <Widget title="Daftar Social Media">
            <table className="table-lg table-fixed">
              <thead>
                <tr>
                  <th className="border">
                    <h2 className="text-sm font-bold w-1/5 ">Tipe</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-2/5">Link</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-1/5">Date</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-1/5">Action</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                {/* Youtube */}
                <tr>
                  <td className="border">Youtube</td>
                  <td className="px-5 border">
                    <div className="items-center">
                      {!editYoutube ? (
                        <a
                          href={sosmedData[0].Youtube_link}
                          className="hover:text-blue-600 hover:underline"
                        >
                          {sosmedData[0].Youtube_link}
                        </a>
                      ) : (
                        <form onSubmit={handleSubmit(onSubmit)}>
                          <div className="">
                            <input
                              className="px-3 py-1 appearance-none border border-black rounded w-1/2  leading-tight focus:outline-none focus:shadow-outline"
                              id="nama"
                              type="text"
                              aria-label="nama"
                              name="Youtube_link"
                              placeholder="https://youtube.com/watch?v=X23feWgjc"
                              ref={register}
                              defaultValue={sosmedData[0].Youtube_link}
                            />
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </div>
                        </form>
                      )}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(sosmedData[0].updated_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex justify-center">
                      <button onClick={() => showEditYoutube(!editYoutube)}>
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </div>
                  </td>
                </tr>
                {/* End of Youtube */}
                {/* Twitter */}
                <tr>
                  <td className="border">Twitter</td>
                  <td className="break-words px-5 border">
                    {!editTwitter ? (
                      <p className="">{sosmedData[0].Twitter_2}</p>
                    ) : (
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="">
                          <textarea
                            className="px-3 resize py-1 appearance-none border border-black rounded w-1/2 leading-tight focus:outline-none focus:shadow-outline"
                            id="nama"
                            type="text"
                            aria-label="nama"
                            name="Twitter_2"
                            ref={register}
                            rows={6}
                            defaultValue={sosmedData[0].Twitter_2}
                          />
                          <button
                            type="submit"
                            className="ml-3 resize border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    )}
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(sosmedData[0].updated_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex justify-center">
                      <button onClick={() => showEditTwitter(!editTwitter)}>
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </div>
                  </td>
                </tr>
                {/* End of Twitter */}
                {/* Facebook */}
                <tr>
                  <td className="border">Facebook</td>
                  <td className="p-5 border ">
                    {!editFacebook ? (
                      <div className="">
                        <p className="break-words">{sosmedData[0].Facebook}</p>
                      </div>
                    ) : (
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="">
                          <textarea
                            className="px-3 resize py-1 appearance-none border border-black rounded w-1/2 leading-tight focus:outline-none focus:shadow-outline"
                            id="nama"
                            type="text"
                            aria-label="nama"
                            name="Facebook"
                            ref={register}
                            rows={6}
                            defaultValue={sosmedData[0].Facebook}
                          />
                          <button
                            type="submit"
                            className="ml-3 resize border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    )}
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(sosmedData[0].updated_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex justify-center">
                      <button onClick={() => showEditFacebook(!editFacebook)}>
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </div>
                  </td>
                </tr>
                {/* End of Facebook */}
                {/* Instagram Caption*/}
                <tr>
                  <td className="border">Instagram Caption</td>
                  <td className="break-words px-5 border ">
                    {!editInstagram ? (
                      <div className="">
                        <p className="">{sosmedData[0].Twitter_1}</p>
                      </div>
                    ) : (
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="">
                          <textarea
                            className="px-3 resize py-1 appearance-none border border-black rounded w-1/2 leading-tight focus:outline-none focus:shadow-outline"
                            id="nama"
                            type="text"
                            aria-label="nama"
                            name="Twitter_1"
                            ref={register}
                            rows={6}
                            defaultValue={sosmedData[0].Twitter_1}
                          />
                          <button
                            type="submit"
                            className="ml-3 resize border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    )}
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(sosmedData[0].updated_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex justify-center">
                      <button onClick={() => showEditInstagram(!editInstagram)}>
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </div>
                  </td>
                </tr>
                {/* End of Instagram Caption*/}
                {/* Instagram Photo */}
                <tr>
                  <td className="border">Instagram Photo</td>
                  <td className="p-5 border">
                    {!editInstagramPhoto ? (
                      <div className="">
                        <img
                          src={sosmedData[0].Instagram.url}
                          alt={"instagram photo"}
                        />
                      </div>
                    ) : (
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <input ref={register} type="file" name="files" />
                        <button
                          type="submit"
                          className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                        >
                          Submit
                        </button>
                      </form>
                    )}
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {moment(sosmedData[0].updated_at)
                        .locale("id", id)
                        .format("DD MMM YYYY")}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex justify-center">
                      <button
                        onClick={() =>
                          showEditInstagramPhoto(!editInstagramPhoto)
                        }
                      >
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </div>
                  </td>
                </tr>
                {/* End of Instagram Photo */}
              </tbody>
            </table>
          </Widget>
        </div>
      </div>
    </Layout>
  );
};

Youtube.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const sosmed = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/social-medias`,
  );

  const sosmedData = await sosmed.json();

  return {
    sosmedData,
    isAuthenticated,
  };
};

export default withRedux(Youtube);
