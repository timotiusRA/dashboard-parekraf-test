import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import * as Icon from "react-feather";
import { useRouter } from "next/router";
import Widget from "../../components/widget";
import SectionTitle from "../../components/section-title";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, setCookies } from "../../lib/actions";

const Seos = ({ data, isAuthenticated }) => {
  const router = useRouter();
  const edit = (id) => {
    setCookies("seo_id", id);
    router.push("/seo/seo-edit");
  };
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);
  return (
    <>
      <Layout>
        <SectionTitle title="SEOs" subtitle="" />

        <Widget title="Daftar SEO">
          <div className="overflow-x-scroll w-auto">
            <table className="table table-lg">
              <thead>
                <tr>
                  <th className="border">
                    <h2 className="text-sm font-bold">No</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Image</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Page</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Title ID</h2>
                  </th>

                  <th className="border">
                    <h2 className="text-sm font-bold">Keywords ID</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Action</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((el, i) => (
                  <tr key={i}>
                    <td className="border">
                      <div className="flex items-center">{i + 1}</div>
                    </td>
                    <td className="border">
                      <img src={el.image && el.image.url} alt="image-seo" />
                    </td>
                    <td className="border">
                      <div className="flex items-center">
                        {el.page && el.page}
                      </div>
                    </td>
                    <td className="border">
                      <div className="flex items-center">
                        {el.title_id && el.title_id}
                      </div>
                    </td>
                    <td className="border">
                      <div className="flex items-center">
                        {el.keywords_id && el.keywords_id}
                      </div>
                    </td>
                    <td className="border">
                      <button onClick={() => edit(el.id)}>
                        <Icon.Edit className="text-green-500" size={16} />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </Widget>
      </Layout>
    </>
  );
};

Seos.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(`${process.env.NEXT_PUBLIC_FETCH_URL}/seos`);

  const data = await resdata.json();

  return {
    data,
    isAuthenticated,
  };
};

export default withRedux(Seos);
