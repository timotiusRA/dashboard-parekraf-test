import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import "react-markdown-editor-lite/lib/index.css";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../../lib/actions";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

const EditSEO = ({ event, isAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files"];
    }
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/seos/` + getCookies("seo_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/seo/seo");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title={`Edit SEO ${event.page} Page`} subtitle="" />

        <form
          onSubmit={handleSubmit(onSubmit)}
          className="w-full grid grid-cols-1"
        >
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2 ">Title ID</label>
              <input
                ref={register}
                type="text"
                name="title_id"
                placeholder="Judul"
                defaultValue={event.title_id}
                className={`${style}`}
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">Title EN</label>
              <input
                ref={register}
                type="text"
                name="title_en"
                placeholder="Title"
                defaultValue={event.title_en}
                className={`${style}`}
              />
            </div>
          </div>
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2">Keywords ID</label>

              <input
                className={`${style}`}
                ref={register}
                type="text"
                name="keywords_id"
                placeholder="kemenparekraf, baparekraf"
                defaultValue={event.keywords_id}
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">Keywords EN</label>

              <input
                className={`${style}`}
                ref={register}
                type="text"
                name="keywords_en"
                placeholder="kemenparekraf, baparekraf"
                defaultValue={event.keywords_en}
              />
            </div>
          </div>
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2">Description ID</label>

              <textarea
                className={`${style} resize`}
                ref={register}
                type="text"
                name="description_id"
                placeholder=".........."
                defaultValue={event.description_id}
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">Description EN</label>

              <textarea
                className={`${style} resize`}
                ref={register}
                type="text"
                name="description_en"
                placeholder=".........."
                defaultValue={event.description_en}
              />
            </div>
          </div>

          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2">Image</label>
              <input
                className={`${style}`}
                ref={register}
                type="file"
                name="files"
              />
            </div>
            <div className="col-span-3 ml-8">
              <label className="m-2">Author</label>
              <input
                className={`${style}`}
                ref={register}
                type="text"
                name="author"
                placeholder="Ratnasari"
                defaultValue={event.author}
              />
            </div>
          </div>
          <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
            Submit
          </button>
        </form>
      </div>
    </Layout>
  );
};

EditSEO.getInitialProps = async (ctx) => {
  let event;
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/seos/` +
      getCookies("seo_id", ctx.req),
  );
  event = await resdata.json();
  return {
    event,
    isAuthenticated,
  };
};

export default withRedux(EditSEO);
