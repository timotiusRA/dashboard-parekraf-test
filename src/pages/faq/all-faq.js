import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Pagination from "../../components/tables/pagination";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import TableFaq from "../../components/tables/tableFaq";
import Link from "next/link";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const AllFaq = ({
  dataLink,
  kategori,
  isContributor,
  isAuthor,
  isAuthenticated,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const handleOnClick = (el) => {
    setCookies("faq_id", el.id);
    router.push("/faq/edit-faq");
  };

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);

      setTimeout(function () {
        router.push("/faq/all-faq");
      }, 1000);

      didDeleted(false);
    }
  }, [role, deleted, start, page, limit]);

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/faqs/` + id, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  const lastPage = Math.ceil(numberOfArticles / limit);

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs?question_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs/count?question_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };
  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle title="FAQ" subtitle="" />
          <Link href="/faq/add-faq">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Data
            </button>
          </Link>
        </div>

        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">Daftar FAQ</span>
            <div className="flex">
              <TotalEntries url={"/faq/all-faq"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableFaq
            dataLink={input === "" ? dataLink : searchData}
            page={page}
            handleOnClick={handleOnClick}
            deleteItem={deleteItem}
          />
          <Pagination
            url={"/faq/all-faq"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

AllFaq.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const limit = query.limit || 10;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs?_limit=${limit}&_start=${start}`,
  );
  const reskategori = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories`,
  );

  const dataLink = await resdata.json();
  const kategori = await reskategori.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    dataLink,
    isAuthenticated,
    kategori,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(AllFaq);
