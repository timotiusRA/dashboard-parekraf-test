import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../../lib/actions";
import InputFaq from "../../components/tables/inputFaq";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const monthsID = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "Nopember",
  "Desember",
];

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

const EditFaq = ({ article, kategori, isAuthenticated }) => {
  const [htmlEN, setHtmlEN] = useState(article && article.answer_en);
  const [htmlID, setHtmlID] = useState(article && article.answer_id);
  const [kategoriFaq, setKategoriFaq] = useState(
    article && article.faq_category["id"],
  );
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["faq_category"] = kategoriFaq;

    if (htmlEN) {
      data["answer_en"] = htmlEN;
    }
    if (htmlID) {
      data["answer_id"] = htmlID;
    }
    const d = new Date();
    const monthName = months[d.getMonth()];
    const monthNameID = monthsID[d.getMonth()];

    let dateEN = d.getDate();
    if (dateEN === 1) dateEN = "1st";
    else if (dateEN === 2) dateEN = "2nd";
    else if (dateEN === 3) dateEN = "3rd";
    else dateEN = `${dateEN}th`;
    data["title_en"] = "Frequently Asked Question";
    data["title_id"] = "Tanya Jawab";
    data["label_en"] = `Last Update ${monthName} ${dateEN}, ${d.getFullYear()}`;
    data[
      "label_id"
    ] = `Terakhir diperbarui pada ${d.getDate()} ${monthNameID} ${d.getFullYear()}`;

    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs/` + getCookies("faq_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/faq/all-faq");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="EDIT FAQ" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputFaq
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            article={article}
            kategori={kategori}
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
            setKategoriFaq={setKategoriFaq}
            kategoriFaq={kategoriFaq}
          />
        </div>
      </div>
    </Layout>
  );
};

EditFaq.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  let article;
  let kategori;

  const resKategori = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories`,
  );
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faqs/` +
      getCookies("faq_id", ctx.req),
  );
  kategori = await resKategori.json();
  article = await resdata.json();
  return {
    article,
    kategori,
    isAuthenticated,
  };
};

export default withRedux(EditFaq);
