import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import InputFaq from "../../components/tables/inputFaq";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const monthsID = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "Nopember",
  "Desember",
];

const AddFaq = ({
  article,
  kategori,
  isContributor,
  isAuthor,
  isAuthenticated,
}) => {
  const [htmlEN, setHtmlEN] = useState(" ");
  const [htmlID, setHtmlID] = useState(" ");
  const [kategoriFaq, setKategoriFaq] = useState(0);
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["faq_category"] = kategoriFaq;
    if (htmlEN && htmlID) {
      data["answer_en"] = htmlEN;
      data["answer_id"] = htmlID;
    }
    const d = new Date();
    const monthName = months[d.getMonth()];
    const monthNameID = monthsID[d.getMonth()];

    let dateEN = d.getDate();
    if (dateEN === 1) dateEN = "1st";
    else if (dateEN === 2) dateEN = "2nd";
    else if (dateEN === 3) dateEN = "3rd";
    else dateEN = `${dateEN}th`;
    data["title_en"] = "Frequently Asked Question";
    data["title_id"] = "Tanya Jawab";
    data["label_en"] = `Last Update ${monthName} ${dateEN}, ${d.getFullYear()}`;
    data[
      "label_id"
    ] = `Terakhir diperbarui pada ${d.getDate()} ${monthNameID} ${d.getFullYear()}`;

    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/faqs`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/faq/all-faq");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="ADD FAQ" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputFaq
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            article={null}
            kategori={kategori}
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
            setKategoriFaq={setKategoriFaq}
            kategoriFaq={kategoriFaq}
          />
        </div>
      </div>
    </Layout>
  );
};

AddFaq.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  let article;
  let kategori;

  const resKategori = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories`,
  );
  const resdata = await fetch(`${process.env.NEXT_PUBLIC_FETCH_URL}/faqs`);
  kategori = await resKategori.json();
  article = await resdata.json();
  return {
    article,
    kategori,
    isAuthenticated,
    isAuthor,
    isContributor,
  };
};

export default withRedux(AddFaq);
