import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
  getCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import Pagination from "../../components/tables/pagination";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import TableFaqCategories from "../../components/tables/tableFaqCategories";
import { confirm } from "../../components/tables/confirm";

const FaqCategories = ({
  dataLink,
  isAuthenticated,
  isContributor,
  isAuthor,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [isEdit, showIsEdit] = useState(dataLink);
  const [deleted, didDeleted] = useState(false);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("faq_category_id", el.id);
    showIsEdit([...isEdit]);
  };

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/faq/all-categories");
      }, 1000);

      didDeleted(false);
    }
  }, [role, deleted, start, page, limit]);

  const onSubmit = async (data) => {
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories/` +
          getCookies("faq_category_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil diubah", 3000);
        router.push("/faq/all-categories");
      })
      .catch((err) => NotificationManager.error("Gagal diubah", 5000));
  };

  const onSubmitPost = async (data) => {
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil ditambahkan", 3000);
        router.push("/faq/all-categories");
      })
      .catch((err) => NotificationManager.error("Gagal ditambahkan", 5000));
  };

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories/` + id)
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories?category_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    const resData = await res.json();
    resData.forEach((el) => (el.isEdit = false));
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories/count?category_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <>
      <Layout>
        <SectionTitle title="FAQ" subtitle="" />

        <div className="mb-10">
          <p className="text-lg my-5">Add new category</p>
          <div className="flex">
            <form onSubmit={handleSubmit(onSubmitPost)} className="flex">
              <div>
                <p>Kategori ID</p>
                <input
                  className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                  ref={register({ required: true })}
                  type="text"
                  name="category_id"
                />
              </div>
              <div className="ml-5">
                <p>Kategori EN</p>

                <input
                  className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                  ref={register({ required: true })}
                  type="text"
                  name="category_en"
                />
              </div>
              <button
                type="submit"
                className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
              >
                Submit
              </button>
            </form>
          </div>
        </div>
        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">Daftar Kategori</span>
            <div className="flex">
              <TotalEntries url={"/faq/all-categories"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableFaqCategories
            dataLink={input === "" ? dataLink : searchData}
            page={page}
            handleOnClick={handleOnClick}
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            deleteItem={deleteItem}
            register={register}
            limit={limit}
          />

          <Pagination
            url={"/faq/all-categories"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

FaqCategories.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories?_limit=${limit}&_start=${start}`,
  );

  let dataLink = await resdata.json();
  dataLink.forEach((el) => (el.isEdit = false));

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/faq-categories/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    isAuthenticated,
    dataLink,
    isAuthor,
    isContributor,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(FaqCategories);
