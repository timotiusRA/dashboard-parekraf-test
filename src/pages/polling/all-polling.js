import { useState, useEffect } from "react";
import Layout from "../../layouts";
import * as Icon from "react-feather";
import { checkServerSideCookie } from "../../lib/actions";
import { withRedux } from "../../lib/redux";
import { getAllPolling, getPollingUserData } from "../../lib/fetchGraphql";
import { useSelector, shallowEqual } from "react-redux";
import { updatePolling } from "../../lib/updateData";
import { convertToCSV } from "../../lib/convertToCSV";
import { useRouter } from "next/router";

const AllPolling = () => {
  const [dataPolling, setDataPolling] = useState([]);
  const [limit, setLimit] = useState(10);
  const router = useRouter();

  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const getData = () => {
    getAllPolling(limit)
      .then((res) => {
        setDataPolling(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deletePolling = async (id) => {
    const setData = {
      is_trash: true,
    };
    await updatePolling(setData, token, id);
    getData();
  };

  const editPolling = (id) => {
    router.push(`/polling/${id}`);
  };

  const allPollingCSV = () => {
    const setData = [
      ["NO.", "TITLE", "AUTHOR"],
      ...dataPolling.map((item, index) => {
        return [index + 1, item.name, item.created_by.username];
      }),
    ];
    convertToCSV(setData);
  };

  const sepcificSurveyCSV = async (pollingId, question) => {
    const rawData = await getPollingUserData(pollingId);
    console.log(rawData, question);
    const setData = [
      ["NO.", "IP", "QUESTION", "ANSWER", "DATE"],
      ...rawData.map((item, index) => {
        return [
          index + 1,
          item.user_ip,
          question,
          item.answer,
          item.choice_date,
        ];
      }),
    ];
    convertToCSV(setData);
  };

  useEffect(() => {
    getData();
  }, [limit]);

  // console.log(dataPolling);
  return (
    <Layout>
      <div className="font-bold text-xl">ALL POLLING</div>
      <div className="flex justify-between mt-8 mb-8">
        <button
          className="bg-primaryNavy text-white pt-1 pb-1 pl-2 pr-2 rounded-md"
          onClick={allPollingCSV}
        >
          Download CSV
        </button>
        <div className="w-auto md: w-1/4">
          <div className="font-semibold text-xs">TOTAL ENTIRES</div>
          <select
            className="border-2 border-gray-600 rounded-md w-full"
            onChange={(event) => {
              setLimit(event.target.value);
            }}
          >
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
      <table className="table-fixed border-collapse border border-gray-400 w-full">
        <thead>
          <tr>
            <th className="w-1/12 border border-gray-400 p-2">NO.</th>
            <th className="w-5/12 border border-gray-400">TITLE</th>
            <th className="w-4/12 border border-gray-400">AUTHOR</th>
            <th className="w-2/12 border border-gray-400">ACTION</th>
          </tr>
        </thead>
        <tbody>
          {dataPolling.map((item, index) => {
            return (
              <tr className="mt-4" key={index}>
                <td className="text-center border border-gray-400 font-semibold">
                  {index + 1}
                </td>
                <td className="border border-gray-400 flex justify-between">
                  <p className="break-words p-1">{item.name}</p>
                  <button
                    className={`${
                      item.status ? "bg-green-500" : "bg-red-500"
                    } text-white p-1 rounded-md`}
                    style={{ fontSize: "10px" }}
                  >
                    {item.status ? "Published" : "Draft"}
                  </button>
                </td>
                <td className="text-center border border-gray-400">
                  {item.created_by.username
                    ? item.created_by.username
                    : "undefined"}
                </td>
                <td className="text-center border border-gray-400">
                  <div className="flex justify-around">
                    <Icon.Edit
                      className="text-green-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        editPolling(item.id);
                      }}
                    />
                    <Icon.Trash
                      className="text-red-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        deletePolling(item.id);
                      }}
                    />
                    <Icon.Download
                      className="text-blue-500 cursor-pointer"
                      size={16}
                      onClick={() => {
                        sepcificSurveyCSV(item.id, item.question);
                      }}
                    />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </Layout>
  );
};
AllPolling.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {};
};
export default withRedux(AllPolling);
