import { useState } from "react";
import Layout from "../../layouts";
import { checkServerSideCookie } from "../../lib/actions";
import { withRedux } from "../../lib/redux";
import { useSelector, shallowEqual } from "react-redux";
import { getPollingByID } from "../../lib/fetchGraphql";
import { updatePolling, updateData } from "../../lib/updateData";
import { deletePollingAnswer } from "../../lib/deleteData";
import { postPollingAnswer } from "../../lib/postData";
import * as Icon from "react-feather";

const UpdatePolling = ({ data, id }) => {
  const [dataPolling, setDataPolling] = useState(data);
  const [dataChanged, setDataChanged] = useState({});
  const [newAnswer, setNewAnswer] = useState("");

  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const changeText = (event) => {
    const targetValue = event.target.value
      ? event.target.value
      : event.target.placeholder;
    setDataChanged({ ...dataChanged, [event.target.name]: targetValue });
  };

  const updateDataPolling = async () => {
    for (const [key, value] of Object.entries(dataChanged)) {
      const [url, id, field] = key.split(":");
      const setData = { [field]: value };
      console.log(setData);
      await updateData(setData, url, token, id);
    }
    window.location.reload();
  };

  const publishPolling = async () => {
    setDataChanged({
      ...dataChanged,
      [`pollings:${dataPolling.id}:status`]: true,
    });
    await updateDataPolling();
  };

  const deleteOption = async (listOption, optionId) => {
    const newOptionId = listOption
      .map((item) => parseInt(item.id))
      .filter((item) => item !== parseInt(optionId));

    await updatePolling({ answer_id: { data: newOptionId } }, token, id);
    await deletePollingAnswer(optionId, token);
    window.location.reload();
  };

  const addOption = async (listOption) => {
    const newOptionId = listOption.map((item) => parseInt(item.id));
    const newAnswerId = await postPollingAnswer(
      { answer_choice: newAnswer },
      token,
    );
    newOptionId.push(newAnswerId);
    await updatePolling({ answer_id: { data: newOptionId } }, token, id);
    window.location.reload();
  };

  console.log(dataChanged);
  return (
    <Layout>
      <div className={`bg-white flex-row justify-center pt-10 pb-10 md:pt-24`}>
        <div
          className={`m-auto sm:mx-20 rounded-md shadow-lg pb-8 pt-8 bg-primaryNavy h-30`}
        >
          <div
            className={`mt-6 text-center text-lg sm:text-xl md:text-3xl font-semibold text-white h-20`}
          >
            <input
              name={`pollings:${dataPolling.id}:name`}
              placeholder={dataPolling.name}
              className="bg-primaryNavy p-1 text-center w-full"
              onChange={changeText}
            ></input>
          </div>
        </div>
        <div
          className={`m-auto sm:mx-20 text-sm font-semibold pb-8 pt-8 pl-2 pr-2`}
        >
          <textarea
            name={`pollings:${dataPolling.id}:desc`}
            placeholder={dataPolling.desc}
            onChange={changeText}
            className="p-1 w-full h-20"
          ></textarea>
        </div>
        <div className={`m-auto sm:mx-20 rounded-xl shadow-lg`}>
          <div
            className={`text-sm sm:text-sm md:text-lg font-bold p-2 md:p-10`}
          >
            <input
              name={`pollings:${dataPolling.id}:question`}
              placeholder={dataPolling.question}
              className="p-1 w-full"
              onChange={changeText}
            ></input>
            <div className="mt-5 mb-5">
              {dataPolling.option
                ? dataPolling.option.map((item, index) => {
                    return (
                      <div
                        key={index}
                        className={`mb-2 font-medium rounded-md border-2 p-3 flex justify-between`}
                      >
                        <input
                          placeholder={item.answer_choice}
                          name={`polling-answers:${item.id}:answer_choice`}
                          className={`text-sm sm:text-sm md:text-lg font-bold p-1 w-3/5`}
                          onChange={changeText}
                        ></input>
                        <Icon.Trash2
                          className="text-red-600 cursor-pointer my-auto"
                          size={20}
                          onClick={() => {
                            deleteOption(dataPolling.option, item.id);
                          }}
                        />
                      </div>
                    );
                  })
                : ""}
              <div className="grid grid-cols-3 gap-4">
                <div className="col-span-2">
                  <input
                    onChange={(event) => {
                      setNewAnswer(event.target.value);
                    }}
                    placeholder="Add new option here..."
                    className={`text-sm font-semibold p-1 w-full border-2 border-light-blue-500 border-opacity-50 rounded-md`}
                  ></input>
                </div>
                <button
                  onClick={() => {
                    addOption(dataPolling.option);
                  }}
                  className={`bg-red-600 text-white p-1 text-sm rounded-md`}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fixed top-0 right-0 mt-20 mr-3 z-50 flex flex-col">
        <button
          onClick={updateDataPolling}
          className={`bg-red-600 text-white p-3 text-sm rounded-md shadow-md mb-2`}
        >
          Save
        </button>
        {!dataPolling.status ? (
          <button
            onClick={publishPolling}
            className={`bg-green-600 text-white p-3 text-sm rounded-md shadow-md`}
          >
            Publish
          </button>
        ) : (
          ""
        )}
      </div>
    </Layout>
  );
};

UpdatePolling.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { id } = ctx.query;
  const data = await getPollingByID(id);

  return { data, id };
};
export default withRedux(UpdatePolling);
