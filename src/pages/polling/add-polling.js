import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import * as Icon from "react-feather";
import Layout from "../../layouts";
import Cookies from "js-cookie";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";
import { postPolling, postPollingAnswer } from "../../lib/postData";
import { useRouter } from "next/router";

const AddSurvey = () => {
  const router = useRouter();
  const [option, setOption] = useState([]);
  const [tmpOption, setTmpOption] = useState("");
  const [dataPolling, setDataPolling] = useState({
    name: "",
    desc: "",
    question: "",
    start_date: null,
    end_date: null,
    is_trash: false,
    status: true,
    created_by: "",
  });

  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const removeOption = (item) => {
    setOption(option.filter((e) => e !== item));
  };

  const changeText = (event) => {
    setDataPolling({ ...dataPolling, [event.target.name]: event.target.value });
  };

  const createPolling = async (status) => {
    const setData = { ...dataPolling, status };
    // console.log(setData);
    const answerId = [];
    for (const item of option) {
      const setDataAnswer = {
        answer_choice: item,
      };
      const resAnswer = await postPollingAnswer(setDataAnswer, token);
      answerId.push(resAnswer);
    }

    setData.answer_id = answerId.length > 0 ? { data: answerId } : null;
    await postPolling(setData, token);
    alert("Succes add Polling");
    router.push("/polling/all-polling");
  };

  useEffect(() => {
    setDataPolling({ ...dataPolling, created_by: Cookies.get("kpkfadmid") });
  }, []);

  return (
    <Layout>
      <div className="font-bold text-xl mt-3 mb-5">ADD Polling</div>
      <div className="m-2 rounded-md shadow-lg border-blue-200 border-2 p-4">
        <div className="w-full mb-4 mt-5">
          <label className="block">
            <span className="text-default">Title</span>
            <input
              onChange={(event) => {
                changeText(event);
              }}
              name="name"
              type="text"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter polling title"
            />
          </label>
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">Description</span>
            <input
              onChange={(event) => {
                changeText(event);
              }}
              name="desc"
              type="text"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter polling description"
            />
          </label>
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">Start date</span>
            <input
              onChange={(event) => {
                changeText(event);
              }}
              name="start_date"
              type="date"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter survey description"
            />
          </label>
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">End date</span>
            <input
              onChange={(event) => {
                changeText(event);
              }}
              name="end_date"
              type="date"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter survey description"
            />
          </label>
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">Question</span>
            <input
              onChange={(event) => {
                changeText(event);
              }}
              name="question"
              type="text"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter polling Question"
            />
          </label>
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <div className="text-default">Option</div>
            <input
              name="option"
              type="text"
              className="form-input mt-1 w-10/12 text-xs bg-white"
              onChange={(event) => {
                setTmpOption(event.target.value);
              }}
              placeholder="Enter polling option"
            />
            <button
              className="bg-blue-800 text-white p-2 rounded-md ml-2"
              onClick={() => {
                setOption([...option, tmpOption]);
              }}
            >
              Add
            </button>
          </label>
        </div>
        <div>
          {option.map((item, index) => {
            return (
              <button
                className="bg-blue-600 text-white p-2 rounded-md ml-3 flex mb-2"
                key={index}
              >
                {item}
                <Icon.Trash
                  className="text-white-500 cursor-pointer ml-2"
                  onClick={() => {
                    removeOption(item);
                  }}
                  size={16}
                />
              </button>
            );
          })}
        </div>
        {option.length > 0 ? (
          <div className="flex justify-end">
            <button
              className="bg-yellow-800 text-white p-2 rounded-md ml-2"
              onClick={() => {
                createPolling(false);
              }}
            >
              Save As Draft
            </button>
            <button
              className="bg-green-800 text-white p-2 rounded-md ml-2"
              onClick={() => {
                createPolling(true);
              }}
            >
              Publish
            </button>
          </div>
        ) : (
          ""
        )}
      </div>
    </Layout>
  );
};
AddSurvey.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {};
};
export default withRedux(AddSurvey);
