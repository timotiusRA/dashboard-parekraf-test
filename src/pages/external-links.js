import { useState, useEffect } from "react";
import SectionTitle from "../components/section-title";
import Widget from "../components/widget";
import Layout from "../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import * as Icon from "react-feather";
import { useRouter } from "next/router";
import { useSelector, shallowEqual } from "react-redux";
import useAuth from "../lib/customHooks/useAuth";
import { withRedux } from "../lib/redux";
import { checkServerSideCookie, getCookies, setCookies } from "../lib/actions";

const ExternalLinks = ({ dataLink, isAuthenticated }) => {
  const [isEdit, showIsEdit] = useState(dataLink);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("ext_id", el.id);
    showIsEdit([...isEdit]);
  };

  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files"];
    }
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/external-links/` +
          getCookies("ext_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/external-links");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Gagal diubah", 5000));
  };

  return (
    <>
      <Layout>
        <SectionTitle title="EXTERNAL LINKS" subtitle="" />
        <Widget title="Daftar Link Eksternal (Beranda)">
          <div className="overflow-x-scroll w-auto">
            <table className="table-fixed mt-10">
              <thead>
                <tr>
                  <th className="border">
                    <h2 className="text-sm font-bold">No</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Image</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Link</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Title ID</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Title EN</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Desc ID</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Desc EN</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Action</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                {dataLink &&
                  dataLink.map((el, i) => (
                    <tr key={i} className="border-b-2">
                      <td className="border">
                        <div className="flex items-center">{i + 1}</div>
                      </td>
                      {!el.isEdit ? (
                        <>
                          <td className="border">
                            <img src={el.image.url} alt="gambar-i" />
                          </td>
                          <td className="mx-2">
                            <div className="flex items-center ml-3">
                              {el.link}
                            </div>
                          </td>
                          <td className="border">
                            <div className="flex items-center ml-3">
                              {el.title_id}
                            </div>
                          </td>
                          <td className="border">
                            <div className="flex items-center ml-3">
                              {el.title_en}
                            </div>
                          </td>
                          <td className="border">
                            <div className="flex items-center ml-3">
                              {el.desc_id}
                            </div>
                          </td>
                          <td className="border">
                            <div className="flex items-center ml-3">
                              {el.desc_en}
                            </div>
                          </td>
                        </>
                      ) : (
                        <>
                          <td className="border">
                            <input ref={register} type="file" name="files" />
                          </td>
                          <td className="border">
                            <input
                              className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="link"
                              defaultValue={el.link}
                            />
                          </td>
                          <td className="border">
                            <input
                              className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="title_id"
                              defaultValue={el.title_id}
                            />
                          </td>
                          <td className="border">
                            <input
                              className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="title_en"
                              defaultValue={el.title_en}
                            />
                          </td>
                          <td className="border">
                            <textarea
                              className="resize px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="desc_en"
                              defaultValue={el.desc_en}
                            />
                          </td>
                          <td className="border">
                            <form
                              onSubmit={handleSubmit(onSubmit)}
                              className="flex"
                            >
                              <textarea
                                className="px-3 resize py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                ref={register}
                                type="text"
                                name="desc_id"
                                defaultValue={el.desc_id}
                              />
                              <button
                                type="submit"
                                className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                              >
                                Submit
                              </button>
                            </form>
                          </td>
                        </>
                      )}
                      <td className="border">
                        <div className="flex justify-center">
                          <button
                            onClick={() => handleOnClick(el)}
                            className="ml-5"
                          >
                            <Icon.Edit className="text-green-500" size={16} />
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </Widget>
      </Layout>
    </>
  );
};

ExternalLinks.getInitialProps = async (ctx) => {
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/external-links`,
  );
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const dataLink = await resdata.json();
  dataLink.forEach((el) => (el.isEdit = false));

  return {
    dataLink,
    isAuthenticated,
  };
};

export default withRedux(ExternalLinks);
