import { useEffect, useState } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import InputArticle from "../../components/tables/inputArticle";
import { getArticleCategories } from "../../lib/fetchGraphql";
import moment from "moment";

// moment(props.input.value, 'dd-mm-yyyy')
const EditArticle = ({ article, kategori, isAuthenticated, defaultStatus }) => {
  const [startDate, setStartDate] = useState(
    article.date && moment(article.date).toDate(),
  );

  const [publishDate, setPublishDate] = useState(
    article.published_at && moment(article.published_at).toDate(),
  );
  const [expireDate, setExpireDate] = useState(
    article.exp_date && moment(article.exp_date).toDate(),
  );

  const [kategoriArtikel, setKategoriArtikel] = useState(
    article &&
      article.article_categories &&
      article.article_categories[0] &&
      article.article_categories[0].id,
  );
  const [kategoriCardArtikel, setKategoriCardArtikel] = useState(
    article.tag_category,
  );

  const [htmlEN, setHtmlEN] = useState(article.content_en);
  const [htmlID, setHtmlID] = useState(article.content_id);
  const { register, handleSubmit } = useForm();
  const [tagsID, setTagsID] = useState([]);
  const [suggestionsID, setSuggestionsID] = useState([
    { id: "Siaran Pers", text: "Siaran Pers" },
    { id: "Berita", text: "Berita" },
    { id: "Pengumuman", text: "Pengumuman" },
    { id: "Artikel", text: "Artikel" },
    { id: "Terbaru", text: "Terbaru" },
  ]);
  const [tagsEN, setTagsEN] = useState([]);
  const [suggestionsEN, setSuggestionsEN] = useState([
    { id: "Press Conference", text: "Press Conference" },
    { id: "News", text: "News" },
    { id: "Announcement", text: "Announcement" },
    { id: "Article", text: "Article" },
    { id: "Newest", text: "Newest" },
  ]);
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  // chip input
  const handleDeleteID = (i) => {
    setTagsID(tagsID.filter((tag, index) => index !== i));
  };

  const handleAdditionID = (tag) => {
    setTagsID([...tagsID, tag]);
  };

  const handleDragID = (tag, currPos, newPos) => {
    const newTags = tagsID.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setTagsID(newTags);
  };

  const handleDeleteEN = (i) => {
    setTagsEN(tagsEN.filter((tag, index) => index !== i));
  };

  const handleAdditionEN = (tag) => {
    setTagsEN([...tagsEN, tag]);
  };

  const handleDragEN = (tag, currPos, newPos) => {
    const newTags = tagsEN.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setTagsEN(newTags);
  };

  const KeyCodes = {
    comma: 188,
    enter: 13,
  };

  const delimiters = [KeyCodes.comma, KeyCodes.enter];

  // end of chip input
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  const onSubmit = async (data) => {
    data["article_categories"] = kategoriArtikel;
    data["user"] = getCookies("kpkfadmid");
    data["published_at"] = publishDate;
    data["exp_date"] = expireDate;

    if (tagsID) {
      let tag_id = "";
      tagsID.map((tag) => (tag_id = tag_id + tag.text + ", "));
      data["tag_id"] = tag_id;
    }
    if (tagsEN) {
      let tag_en = "";
      tagsEN.map((tag) => (tag_en = tag_en + tag.text + ", "));
      data["tag_en"] = tag_en;
    }

    if (getCookies("kebijakan")) {
      data["tag_category"] = "kebijakan";
      setCookies("kebijakan", false);
    } else if (getCookies("panduan_perjalanan_wisata")) {
      data["tag_category"] = "panduan perjalanan wisata";
      setCookies("panduan_perjalanan_wisata", false);
    } else if (getCookies("pelatihan_parekraf")) {
      data["tag_category"] = "pelatihan parekraf";
      setCookies("pelatihan_parekraf", false);
    } else if (getCookies("industri_parekraf")) {
      data["tag_category"] = "industri parekraf";
      setCookies("industri_parekraf", false);
    }

    if (kategoriCardArtikel) data["tag_category"] = kategoriCardArtikel;

    if (startDate) {
      data["date"] = formatDate(startDate);
    }

    if (htmlEN) {
      data["content_en"] = htmlEN;
    }
    if (htmlID) {
      data["content_id"] = htmlID;
    }
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files"];
    }

    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/` +
          getCookies("article_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/articles/allArticles");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="EDIT ARTIKEL" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputArticle
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            article={article}
            kategori={kategori}
            //ckeditor
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
            //datepicker
            startDate={startDate}
            setStartDate={setStartDate}
            publishDate={publishDate}
            setPublishDate={setPublishDate}
            expireDate={expireDate}
            setExpireDate={setExpireDate}
            // react tags
            tagsID={tagsID}
            tagsEN={tagsEN}
            suggestionsID={suggestionsID}
            suggestionsEN={suggestionsEN}
            handleDeleteID={handleDeleteID}
            handleAdditionID={handleAdditionID}
            handleDragID={handleDragID}
            handleDeleteEN={handleDeleteEN}
            handleAdditionEN={handleAdditionEN}
            handleDragEN={handleDragEN}
            delimiters={delimiters}
            setKategoriArtikel={setKategoriArtikel}
            kategoriArtikel={kategoriArtikel}
            kategoriCardArtikel={kategoriCardArtikel}
            setKategoriCardArtikel={setKategoriCardArtikel}
            defaultStatus={defaultStatus}
          />
        </div>
      </div>
    </Layout>
  );
};

EditArticle.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const fetchData = await getArticleCategories();

  const kategori = fetchData.articleCategories;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/` +
      getCookies("article_id", ctx.req),
  );

  const article = await resdata.json();

  return {
    article,
    isAuthenticated,
    kategori,
    defaultStatus: article?.status,
  };
};

export default withRedux(EditArticle);
