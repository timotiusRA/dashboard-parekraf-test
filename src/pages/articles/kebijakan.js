import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import Table2 from "../../components/tables/table1";
import { useRouter } from "next/router";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import Pagination from "../../components/tables/pagination";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import Widget from "../../components/widget";
import TableCards from "../../components/tables/tableCards";
import { confirm } from "../../components/tables/confirm";

const thKu = ["Image", "Title ID", "Title EN", "Link", "Action"];

const AllKebijakan = ({
  isAuthenticated,
  data,
  mainFeature,
  page,
  numberOfArticles,
}) => {
  const [deleted, didDeleted] = useState(false);
  const [isEdit, showIsEdit] = useState(mainFeature);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / 10);
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);

  useEffect(() => {
    if (deleted) {
      NotificationManager.info(
        "Berhasil didelete dari daftar cards",
        null,
        500,
      );
      setTimeout(function () {
        router.push("/articles/kebijakan");
      }, 1000);

      didDeleted(false);
    }
  }, [deleted, role]);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("kebijakan_id", el.id);
    showIsEdit([...isEdit]);
  };

  const toEditPage = async (id) => {
    setCookies("article_id", id);
    setCookies("kebijakan", true);
    router.push("/articles/edit-article");
  };

  const toPostPage = async () => {
    setCookies("kebijakan", true);
    router.push("/articles/add-article");
  };
  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      const payload = {
        tag_category: "",
      };
      await axios
        .put(`${process.env.NEXT_PUBLIC_FETCH_URL}/articles/` + id, payload, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) => console.log(err));
      delete data["files"];
    }
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/card-lists/` +
          getCookies("kebijakan_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/articles/kebijakan");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Gagal diubah", 5000));
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="CARD KEBIJAKAN" subtitle="" />
        <Widget title="Link Terkait">
          <TableCards
            thKu={thKu}
            mainFeature={mainFeature}
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            handleOnClick={handleOnClick}
          />
        </Widget>

        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">Daftar Artikel</span>
            <button
              onClick={toPostPage}
              className="mb-5 bg-primaryNavy text-white rounded-lg p-3"
            >
              Tambah Artikel
            </button>
          </div>
          <div className=" flex-wrap w-full mb-4">
            <Table2
              data={data}
              title=""
              toEditPage={toEditPage}
              deleteItem={deleteItem}
              subtitle=""
            />
          </div>
          <Pagination
            url={"/articles/kebijakan"}
            lastPage={lastPage}
            page={page}
          />
        </Widget>
      </div>
    </Layout>
  );
};

AllKebijakan.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * 10;
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?tag_category=kebijakan&_limit=10&_start=${start}`,
  );
  const data = await resdata.json();

  const resMain = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/card-lists?card.title_id=kebijakan`,
  );
  const mainFeature = await resMain.json();
  mainFeature.forEach((el) => (el.isEdit = false));

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?tag_category=kebijakan`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    data,
    mainFeature,
    page: +page,
    isAuthenticated,
    numberOfArticles,
  };
};

export default withRedux(AllKebijakan);
