import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import Table2 from "../../components/tables/table1";
import { useRouter } from "next/router";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, setCookies } from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import Widget from "../../components/widget";
import Link from "next/link";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const AllAnnouncements = ({
  data,
  isAuthenticated,
  page,
  numberOfArticles,
  limit,
  start,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/articles/all-announcements");
      }, 1000);
      didDeleted(false);
    }
  }, [deleted, role, start, page, limit]);

  const toEditPage = async (id) => {
    setCookies("article_id", id);
    router.push("/articles/edit-article");
  };

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/articles/` + id, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?article_categories.category_id=Pengumuman&title_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));

    return await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?article_categories.category_id=Pengumuman&title_id_contains=${input}&_limit=${limit}&_start=${start}`,
    )
      .then((response) => response.json())
      .then((data) => {
        setSearchData(data);
      });
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <div className="flex justify-between">
          <SectionTitle title="ANNOUNCEMENTS" subtitle="" />
          <Link href="/articles/add-article">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Artikel
            </button>
          </Link>
        </div>
        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">
              Daftar Pengumuman
            </span>
            <div className="flex">
              <TotalEntries url={"/articles/all-announcements"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <div className=" flex-wrap w-full mb-4">
            <Table2
              data={input === "" ? data : searchData}
              title=""
              toEditPage={toEditPage}
              deleteItem={deleteItem}
              subtitle=""
            />
          </div>
          <Pagination
            url={"/articles/all-announcements"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </div>
    </Layout>
  );
};

AllAnnouncements.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;
  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?article_categories.category_id=Pengumuman&_limit=${limit}&_start=${start}`,
  );
  const data = await resdata.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?article_categories.category_id=Pengumuman`,
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    isAuthenticated,
    data,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(AllAnnouncements);
