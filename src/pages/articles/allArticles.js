import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import Widget from "../../components/widget";
import SectionTitle from "../../components/section-title";
import Table2 from "../../components/tables/table1";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import useAuth from "../../lib/customHooks/useAuth";
import { useRouter } from "next/router";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import Pagination from "../../components/tables/pagination";
import Link from "next/link";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import { confirm } from "../../components/tables/confirm";

const AllArticles = ({
  dataPagi,
  isAuthenticated,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [deleted, didDeleted] = useState(false);
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );

  useAuth(isAuthenticated);

  const toEditPage = async (id) => {
    setCookies("article_id", id);
    router.push("/articles/edit-article");
  };

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/articles/` + id, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    } 
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?title_id_contains=${input}&_limit=${limit}&_start=${start}&_sort=date:DESC`,
    );
    const resData = await res.json();
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?title_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  // filter
  const [filterData, setFilterData] = useState();
  const [pageFilter, setPageFilter] = useState();
  const [filterBy, setFilterBy] = useState("");

  const filter = async (str) => {
    let res;
    let resData;
    let numberOfFilteredResultResponse;
    let numberOfFilteredResult;
    if (str === "mine") {
      res = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?user.id=${getCookies(
          "kpkfadmid",
        )}&_limit=${limit}&_start=${start}&_sort=date:DESC`,
      );
      resData = await res.json();

      numberOfFilteredResultResponse = await fetch(
        `${
          process.env.NEXT_PUBLIC_FETCH_URL
        }/articles/count?user.id=${getCookies("kpkfadmid")}`,
      );

      numberOfFilteredResult = await numberOfFilteredResultResponse.json();
      setFilterBy(str);
    } else if (str === "published") {
      res = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?status=published&_limit=${limit}&_start=${start}&_sort=date:DESC`,
      );
      resData = await res.json();

      numberOfFilteredResultResponse = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?status=published`,
      );

      numberOfFilteredResult = await numberOfFilteredResultResponse.json();
      setFilterBy(str);
    } else if (str === "draft") {
      res = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?status=draft&_limit=${limit}&_start=${start}&_sort=date:DESC`,
      );
      resData = await res.json();

      numberOfFilteredResultResponse = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?status=draft`,
      );

      numberOfFilteredResult = await numberOfFilteredResultResponse.json();
      setFilterBy(str);
    } else if (str === "all") {
      res = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?_limit=${limit}&_start=${start}&_sort=date:DESC`,
      );
      resData = await res.json();

      numberOfFilteredResultResponse = await fetch(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count`,
      );

      numberOfFilteredResult = await numberOfFilteredResultResponse.json();
      setFilterBy(str);
    }

    setFilterData(resData);
    setPageFilter(Math.ceil(numberOfFilteredResult / limit));
  };

  useEffect(() => {
    fetchData(input);
    filter(filterBy);
    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/articles/allArticles");
      }, 1000);

      didDeleted(false);
    }
  }, [role, deleted, start, page, limit, filterBy]);

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <div className="flex justify-between">
          <SectionTitle title="ARTICLES" subtitle="" />
          <Link href="/articles/add-article">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Artikel
            </button>
          </Link>
        </div>
        <Widget title="">
          <div className="flex justify-between">
            <div>
              <span className="text-lg font-semibold -mb-5">
                Daftar Artikel
              </span>
              <button
                onClick={() => filter("all")}
                className={`ml-10  ${
                  filterBy === "all" || filterBy === ""
                    ? "bg-gold1 text-black"
                    : "bg-primaryNavy text-white"
                } px-3 py-2 rounded-md`}
              >
                All
              </button>
              <button
                onClick={() => filter("mine")}
                className={`ml-3  ${
                  filterBy === "mine"
                    ? "bg-gold1 text-black"
                    : "bg-primaryNavy text-white"
                } px-3 py-2 rounded-md`}
              >
                Mine
              </button>
              <button
                onClick={() => filter("published")}
                className={`ml-3  ${
                  filterBy === "published"
                    ? "bg-gold1 text-black"
                    : "bg-primaryNavy text-white"
                } px-3 py-2 rounded-md`}
              >
                Published
              </button>
              <button
                onClick={() => filter("draft")}
                className={`ml-3 ${
                  filterBy === "draft"
                    ? "bg-gold1 text-black"
                    : "bg-primaryNavy text-white"
                }  px-3 py-2 rounded-md`}
              >
                Draft
              </button>
            </div>
            <div className="flex">
              <TotalEntries
                url={"/articles/allArticles"}
                category={filterBy}
                limit={limit}
              />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>

          <div className=" flex-wrap w-full mb-4">
            <Table2
              data={
                input !== "" ? searchData : filterBy ? filterData : dataPagi
              }
              title=""
              toEditPage={toEditPage}
              deleteItem={deleteItem}
              subtitle=""
            />
          </div>
          <Pagination
            url={"/articles/allArticles"}
            lastPage={
              input !== "" ? pageSearch : filterBy ? pageFilter : lastPage
            }
            page={page}
            limit={limit}
            category={filterBy}
          />
        </Widget>
      </div>
    </Layout>
  );
};

AllArticles.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const query = ctx.query;
  const limit = query.limit || 10;
  const filter = query.category || "";
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;
  let resdata;
  let numberOfArticlesResponse;

  resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles?_limit=${limit}&_start=${start}&_sort=date:DESC`,
  );

  numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count`,
  );

  const dataPagi = await resdata.json();
  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    dataPagi,
    page: +page,
    limit: +limit,
    start,
    numberOfArticles,
    isAuthenticated,
  };
};

export default withRedux(AllArticles);
