import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import InputArticle from "../../components/tables/inputArticle";
import { getArticleCategories } from "../../lib/fetchGraphql";

const AddArticle = ({ kategori, isAuthenticated }) => {
  const [startDate, setStartDate] = useState(new Date());
  const [publishDate, setPublishDate] = useState();
  const [expireDate, setExpireDate] = useState();

  const [htmlEN, setHtmlEN] = useState(" ");
  const [kategoriArtikel, setKategoriArtikel] = useState(" ");
  const [htmlID, setHtmlID] = useState(" ");
  const { register, handleSubmit } = useForm();
  const [tagsID, setTagsID] = useState([]);
  const [suggestionsID, setSuggestionsID] = useState([
    { id: "Siaran Pers", text: "Siaran Pers" },
    { id: "Berita", text: "Berita" },
    { id: "Pengumuman", text: "Pengumuman" },
    { id: "Artikel", text: "Artikel" },
    { id: "Terbaru", text: "Terbaru" },
  ]);
  const [tagsEN, setTagsEN] = useState([]);
  const [suggestionsEN, setSuggestionsEN] = useState([
    { id: "Press Conference", text: "Press Conference" },
    { id: "News", text: "News" },
    { id: "Announcement", text: "Announcement" },
    { id: "Article", text: "Article" },
    { id: "Newest", text: "Newest" },
  ]);

  const [kategoriCardArtikel, setKategoriCardArtikel] = useState("");

  const router = useRouter();

  // chip input
  const handleDeleteID = (i) => {
    setTagsID(tagsID.filter((tag, index) => index !== i));
  };

  const handleAdditionID = (tag) => {
    setTagsID([...tagsID, tag]);
  };

  const handleDragID = (tag, currPos, newPos) => {
    const newTags = tagsID.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setTagsID(newTags);
  };

  const handleDeleteEN = (i) => {
    setTagsEN(tagsEN.filter((tag, index) => index !== i));
  };

  const handleAdditionEN = (tag) => {
    setTagsEN([...tagsEN, tag]);
  };

  const handleDragEN = (tag, currPos, newPos) => {
    const newTags = tagsEN.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setTagsEN(newTags);
  };

  const KeyCodes = {
    comma: 188,
    enter: 13,
  };

  const delimiters = [KeyCodes.comma, KeyCodes.enter];

  // end of chip input

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);

  useEffect(() => {}, [role]);

  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  const onSubmit = async (data) => {
    data["article_categories"] = kategoriArtikel;
    data["user"] = getCookies("kpkfadmid");
    data["published_at"] = publishDate;
    data["exp_date"] = expireDate;

    if (kategoriCardArtikel) data["tag_category"] = kategoriCardArtikel;

    if (tagsID) {
      let tag_id = "";
      tagsID.map((tag) => (tag_id = tag_id + tag.text + ", "));
      data["tag_id"] = tag_id;
    }
    if (tagsEN) {
      let tag_en = "";
      tagsEN.map((tag) => (tag_en = tag_en + tag.text + ", "));
      data["tag_en"] = tag_en;
    }

    if (getCookies("kebijakan")) {
      data["tag_category"] = "kebijakan";
      setCookies("kebijakan", false);
    } else if (getCookies("panduan_perjalanan_wisata")) {
      data["tag_category"] = "panduan perjalanan wisata";
      setCookies("panduan_perjalanan_wisata", false);
    } else if (getCookies("pelatihan_parekraf")) {
      data["tag_category"] = "pelatihan parekraf";
      setCookies("pelatihan_parekraf", false);
    } else if (getCookies("industri_parekraf")) {
      data["tag_category"] = "industri parekraf";
      setCookies("industri_parekraf", false);
    }

    if (startDate) data["date"] = formatDate(startDate);
    data["content_en"] = htmlEN;
    data["content_id"] = htmlID;
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) => console.log(err));
      delete data["files"];
    }
    // data["author_instance_en"] = data["author_instance_id"];
    data["view_count"] = 0;
    data["like_count"] = 0;
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/articles`, data, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/articles/allArticles");
        }, 1000);
      })
      .catch((err) => {
        NotificationManager.error("error", 5000);
      });
  };

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="ADD ARTIKEL" subtitle="" />

        <div className=" flex-wrap w-full mb-4">
          <InputArticle
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            register={register}
            article={null}
            kategori={kategori}
            //ckeditor
            htmlEN={htmlEN}
            htmlID={htmlID}
            setHtmlEN={setHtmlEN}
            setHtmlID={setHtmlID}
            //datepicker
            startDate={startDate}
            setStartDate={setStartDate}
            publishDate={publishDate}
            setPublishDate={setPublishDate}
            expireDate={expireDate}
            setExpireDate={setExpireDate}
            // react tags
            tagsID={tagsID}
            tagsEN={tagsEN}
            suggestionsID={suggestionsID}
            suggestionsEN={suggestionsEN}
            handleDeleteID={handleDeleteID}
            handleAdditionID={handleAdditionID}
            handleDragID={handleDragID}
            handleDeleteEN={handleDeleteEN}
            handleAdditionEN={handleAdditionEN}
            setKategoriArtikel={setKategoriArtikel}
            kategoriArtikel={kategoriArtikel}
            handleDragEN={handleDragEN}
            delimiters={delimiters}
            kategoriCardArtikel={kategoriCardArtikel}
            setKategoriCardArtikel={setKategoriCardArtikel}
          />
        </div>
      </div>
    </Layout>
  );
};

AddArticle.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const fetchData = await getArticleCategories();

  const kategori = fetchData.articleCategories;

  return {
    isAuthenticated,
    kategori,
  };
};

export default withRedux(AddArticle);
