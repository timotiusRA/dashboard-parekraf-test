import { useState } from "react";
import { withRedux } from "../../lib/redux";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import Layout from "../../layouts";
import axios from "axios";
import * as Icon from "react-feather";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";

const AllCards = ({ dataLink, isAuthenticated }) => {
  const [isEdit, showIsEdit] = useState(dataLink);
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("card_id", el.id);
    showIsEdit([...isEdit]);
  };

  const onSubmit = async (data) => {
    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) => console.log(err));
      delete data["files"];
    }
    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/cards/` + getCookies("card_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/articles/all-cards");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Gagal diubah", 5000));
  };

  return (
    <>
      <Layout>
        <SectionTitle title="CARDS" subtitle="" />

        <Widget title="Daftar Cards">
          <div className="w-auto">
            <table className="table-fixed mt-10">
              <thead>
                <tr>
                  <th className="border">
                    <h2 className="text-sm font-bold w-1/12">Card</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-1/12">Image</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-5/12">Content ID</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold w-5/12">Content EN</h2>
                  </th>
                  <th className="border">
                    <h2 className="text-sm font-bold">Action</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                {dataLink &&
                  dataLink.map((el, i) => (
                    <tr key={i} className="border-b-2">
                      {!el.isEdit ? (
                        <>
                          <td className="border w-1/12">
                            <div className="flex items-center ml-3">
                              {el.title_id}
                            </div>
                            <hr className="w-3/4 mx-auto my-3" />
                            <div className="flex items-center ml-3">
                              {el.title_en}
                            </div>
                          </td>
                          <td className="border w-1/12 ">
                            <img
                              src={el.image.url}
                              alt="gambar-i"
                              className=""
                            />
                          </td>
                          <td className="border w-5/12">
                            <div className="flex items-center ml-3">
                              {el.content_id}
                            </div>
                          </td>
                          <td className="border w-5/12">
                            <div className="flex items-center ml-3">
                              {el.content_en}
                            </div>
                          </td>
                        </>
                      ) : (
                        <>
                          <td className="border w-1/12">
                            <p>Title ID</p>
                            <p className="mt-3">{el.title_id}</p>
                            <p className="mt-4">Title EN</p>
                            <input
                              className="resize px-1 py-1 w-full h-85 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="title_en"
                              defaultValue={el.title_en && el.title_en}
                            />
                          </td>
                          <td className="border w-1/12">
                            <input
                              ref={register}
                              type="file"
                              name="files"
                              className="ml-5"
                            />
                          </td>
                          <td className="border w-5/12">
                            <textarea
                              className="resize px-3 py-1 w-full h-140 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="content_id"
                              defaultValue={el.content_id && el.content_id}
                            />
                          </td>

                          <td className="border w-5/12">
                            <form
                              onSubmit={handleSubmit(onSubmit)}
                              className="flex"
                            >
                              <textarea
                                className="px-3 resize py-1 w-full h-140  appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                                ref={register}
                                type="text"
                                name="content_en"
                                defaultValue={el.content_en && el.content_en}
                              />
                              <button
                                type="submit"
                                className="ml-3 border my-10 border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                              >
                                Submit
                              </button>
                            </form>
                          </td>
                        </>
                      )}
                      <td className="border">
                        <div className="flex justify-center">
                          <button
                            onClick={() => handleOnClick(el)}
                            className="mr-2 ml-5"
                          >
                            <Icon.Edit className="text-green-500" size={16} />
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </Widget>
      </Layout>
    </>
  );
};

AllCards.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const resdata = await fetch(`${process.env.NEXT_PUBLIC_FETCH_URL}/cards`);

  const dataLink = await resdata.json();
  dataLink.forEach((el) => (el.isEdit = false));

  return {
    dataLink,
    isAuthenticated,
  };
};

export default withRedux(AllCards);
