import React, { useState, useEffect } from "react";
import useAuth from "../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie, getCookies } from "../lib/actions";
import { withRedux } from "../lib/redux";
import Layout from "../layouts";
import SectionTitle from "../components/section-title";
import TableMedia from "../components/tables/media";
import { NotificationManager } from "react-notifications";
import Link from "next/link";
import * as Icon from "react-feather";

const Dashboard = ({
  msg,
  subscribers,
  data,
  announcementData,
  news,
  kebijakan,
  event,
  isAuthenticated,
}) => {
  const { role, token, user } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
      user: state.user,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);
  const categories = [
    {
      info: `Berita ${news} data`,
      href: "/",
      icon: <Icon.Book size={50} />,
    },
    {
      info: `Pengumuman ${announcementData} data`,
      href: "/",
      icon: (
        <svg
          className="w-16 h-16"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z"
          ></path>
        </svg>
      ),
    },
    {
      info: `Kebijakan ${kebijakan} data`,
      href: "/",
      icon: (
        <svg
          className="w-16 h-16"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z"
          ></path>
        </svg>
      ),
    },
    {
      info: `Events ${event} data`,
      href: "/event/all-event",
      icon: <Icon.Bell size={50} />,
    },
    {
      info: `Messages ${msg} data`,
      href: "/messages",
      icon: <Icon.MessageCircle size={50} />,
    },
    {
      info: `Subscribers ${subscribers} data`,
      href: "/subscribers",
      icon: <Icon.Rss size={50} />,
    },
  ];

  return (
    <Layout>
      <div className="w-full lg:px-2">
        <SectionTitle title="All Data" subtitle="" />
        <div className=" flex-wrap w-full mb-4">
          <div className="grid grid-cols-6 gap-10">
            {categories.map((el, i) => (
              <button
                key={i}
                className="col-span-2 cursor-default bg-primaryNavy text-xl rounded-lg p-20 border border-white text-white"
              >
                <div className="mb-5 flex justify-center">{el.icon}</div>
                {el.info}
              </button>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
};

Dashboard.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files`,
  );

  const announcement = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?article_categories.id=2`,
  );
  const newsRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?article_categories.id=1`,
  );
  const kebijakanRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/articles/count?article_categories.id=3`,
  );
  const eventRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/events/count`,
  );
  const msgRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/messages/count`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );
  const subsRes = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/subscribers/count`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );

  const announcementData = await announcement.json();
  const news = await newsRes.json();

  const subscribers = await subsRes.json();
  const msg = await msgRes.json();
  const event = await eventRes.json();
  const kebijakan = await kebijakanRes.json();
  const data = await resdata.json();

  if (subscribers === undefined) subscribers = 0;
  if (msg === undefined) msg = 0;
  if (announcementData === undefined) announcementData = 0;
  if (event === undefined) event = 0;
  if (news === undefined) news = 0;
  if (kebijakan === undefined) kebijakan = 0;

  return {
    event,
    subscribers,
    msg,
    data,
    news,
    kebijakan,
    announcementData,
    isAuthenticated,
  };
};

export default withRedux(Dashboard);
