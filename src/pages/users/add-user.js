import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import "react-markdown-editor-lite/lib/index.css";
import { useRouter } from "next/router";
import cookieCutter from "cookie-cutter";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import { checkServerSideCookie } from "../../lib/actions";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";
const kategori = [
  {
    id: 4,
    role: "Contributor",
  },
  {
    id: 5,
    role: "Editor",
  },
  {
    id: 6,
    role: "Author",
  },
];
const AddUser = ({ isAuthenticated }) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    data["provider"] = "local";
    data["confirmed"] = true;
    data["blocked"] = "false";
    data["role"] = {
      id: data["akses"],
    };
    delete data["akses"];

    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/users`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/users/all-users");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title={`Add User`} subtitle="" />
        <p className="mb-5 -mt-5 text-1rem font-semibold">
          Role: Editor &bull; Contributor &bull; Author
        </p>

        <form
          onSubmit={handleSubmit(onSubmit)}
          className="w-full grid grid-cols-1"
        >
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2 ">Username</label>
              <input
                ref={register}
                type="text"
                name="username"
                className={`${style}`}
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">Email</label>
              <input
                ref={register}
                type="text"
                name="email"
                className={`${style}`}
              />
            </div>
          </div>
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2">Password</label>

              <input
                className={`${style}`}
                ref={register}
                type="text"
                name="password"
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">Role</label>

              <select
                className="w-full ml-2 p-2"
                id="grid-state"
                ref={register}
                name="akses"
              >
                {kategori.map((el, i) => (
                  <option value={el.id} key={i}>
                    {el.role}
                  </option>
                ))}
              </select>
            </div>
          </div>

          <button className="hover:bg-white border border-primaryNavy text-primaryNavy my-3 bg-blue-300 rounded-lg p-2 text-lg">
            Submit
          </button>
        </form>
      </div>
    </Layout>
  );
};
AddUser.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);

  return {
    isAuthenticated,
  };
};
export default withRedux(AddUser);
