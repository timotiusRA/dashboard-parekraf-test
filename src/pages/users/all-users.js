import { useEffect, useState } from "react";
import Widget from "../../components/widget";
import SectionTitle from "../../components/section-title";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  getCookies,
  setCookies,
} from "../../lib/actions";
import Pagination from "../../components/tables/pagination";
import TableUser from "../../components/tables/tableUser";
import Link from "next/link";
import TotalEntries from "../../components/tables/totalEntries";
import SearchBar from "../../components/tables/searchBar";
import { confirm } from "../../components/tables/confirm";

const kategori = [
  {
    id: 4,
    role: "Contributor",
  },
  {
    id: 5,
    role: "Editor",
  },
  {
    id: 6,
    role: "Author",
  },
];
const Users = ({
  data,
  isAuthenticated,
  page,
  numberOfArticles,
  start,
  limit,
}) => {
  const [isEdit, showIsEdit] = useState(data);
  const [thEdit, showThEdit] = useState(false);

  const { register, handleSubmit } = useForm();

  const router = useRouter();
  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  useEffect(() => {
    fetchData(input);
  }, [role, start, page, limit]);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("user_id", el.id);
    showIsEdit([...isEdit]);
    showThEdit(!thEdit);
  };

  const deleted = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/users/` + id, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          NotificationManager.success("Berhasil dihapus", null, 500);
          setTimeout(function () {
            router.push("/users/all-users");
          }, 1000);
        })
        .catch((err) => NotificationManager.error("Gagal dihapus", 5000));
    }
  };

  const lastPage = Math.ceil(numberOfArticles / limit);

  const onSubmit = async (data) => {
    data["provider"] = "local";
    data["confirmed"] = true;
    data["blocked"] = "false";
    data["role"] = {
      id: data["akses"],
    };
    delete data["akses"];

    await axios
      .put(
        `${process.env.NEXT_PUBLIC_FETCH_URL}/users/` + getCookies("user_id"),
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((res) => {
        NotificationManager.success("Berhasil Diganti", null, 500);
        setTimeout(function () {
          router.push("/users/all-users");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
    showThEdit(false);
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/users?username_contains=${input}&_limit=${limit}&_start=${start}`,
      {
        headers: {
          Authorization: `Bearer ` + token,
        },
      },
    );
    let resData = await res.json();
    resData.forEach((element) => {
      if (element.role["name"] === "Authenticated") {
        element.role["name"] = "Administrator";
      }
    });
    resData.forEach((el) => (el.isEdit = false));
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/users/count?username_contains=${input}`,
      {
        headers: {
          Authorization: `Bearer ` + token,
        },
      },
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };
  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle title="USERS" subtitle=" " />
          <Link href="/users/add-user">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Data
            </button>
          </Link>
        </div>
        <p className="mb-5 -mt-5 text-1rem font-semibold">
          Role: Editor &bull; Contributor &bull; Author
        </p>

        <Widget title="">
          <div className="flex justify-between mb-5">
            <span className="text-lg font-semibold -mb-5">Daftar User</span>
            <div className="flex">
              <TotalEntries url={"/users/all-users"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <TableUser
            data={input === "" ? data : searchData}
            page={page}
            thEdit={thEdit}
            register={register}
            kategori={kategori}
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
            handleOnClick={handleOnClick}
            deleted={deleted}
          />
          <Pagination
            url={"/users/all-users"}
            lastPage={input === "" ? lastPage : pageSearch}
            limit={limit}
            page={page}
          />
        </Widget>
      </Layout>
    </>
  );
};

Users.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const { isAuthenticated } = checkServerSideCookie(ctx);

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/users?_limit=${limit}&_start=${start}`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );

  const data = await resdata.json();
  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/users/count`,
    {
      headers: {
        Authorization: `Bearer ` + getCookies("kpkfadmtoken", ctx.req),
      },
    },
  );
  const numberOfArticles = await numberOfArticlesResponse.json();

  data.forEach((element) => {
    if (element.role["name"] === "Authenticated") {
      element.role["name"] = "Administrator";
    }
  });
  data.forEach((el) => (el.isEdit = false));

  return {
    data,
    isAuthenticated,
    page: +page,
    numberOfArticles,
    limit: +limit,
    start,
  };
};

export default withRedux(Users);
