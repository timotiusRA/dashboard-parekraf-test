import { useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import { numberToRoman } from "big-roman";
import { uniq } from "lodash";
import InputEmagz from "../../components/tables/inputEmagz";

const AddEmagz = ({
  isAuthenticated,
  isContributor,
  isAuthor,
  tahun,
  edisi,
  kategori,
}) => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    const category_id = `Pesona E-Magazine Edisi ${numberToRoman(
      parseInt(data["edisi"]),
    )} ${data["tahun"]}`;
    const magazine_kategori = kategori.filter(
      (el) => el.category_id === category_id,
    );
    data["magazine_category"] = magazine_kategori[0];

    if (data.files && data.files.length > 0) {
      const formData = new FormData();
      formData.append("files", data.files[0]);
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["file"] = res.data[0];
        })
        .catch((err) => NotificationManager.error("File gagal diupload", 5000));
      delete data["files"];
    }
    if (data["files-img"] && data["files-img"].length > 0) {
      const formData = new FormData();
      formData.append("files", data["files-img"][0]);

      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          data["image"] = res.data[0];
        })
        .catch((err) =>
          NotificationManager.error("Gambar gagal diupload", 5000),
        );
      delete data["files-img"];
    }
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/magazines/`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Berhasil Diupload", "", 500);
        setTimeout(function () {
          router.push("/emagz/all-emagz");
        }, 1000);
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle title={`Add Magazine`} subtitle="" />
        <InputEmagz
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          register={register}
          magz={null}
          tahun={tahun}
          edisi={edisi}
        />
      </div>
    </Layout>
  );
};

AddEmagz.getInitialProps = async (ctx) => {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories`,
  );
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  const kategori = await res.json();
  let tahun = [];
  let edisi = [];
  kategori.forEach((element) => {
    tahun.push(element.tahun);
    edisi.push(element.edisi);
  });
  tahun = uniq(tahun);
  edisi = uniq(edisi).sort();

  return {
    isAuthenticated,
    kategori,
    isAuthor,
    isContributor,
    tahun,
    edisi,
  };
};

export default withRedux(AddEmagz);
