import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import * as Icon from "react-feather";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  getCookies,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import SectionTitle from "../../components/section-title";
import Widget from "../../components/widget";
import Pagination from "../../components/tables/pagination";
import { numberToRoman } from "big-roman";
import { confirm } from "../../components/tables/confirm";

const EmagzCategories = ({
  dataLink,
  isAuthenticated,
  isContributor,
  isAuthor,
  page,
  numberOfArticles,
  kategori,
}) => {
  const [isEdit, showIsEdit] = useState(dataLink);
  const [deleted, didDeleted] = useState(false);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / 10);

  const handleOnClick = (el) => {
    el["isEdit"] = !el["isEdit"];
    setCookies("emagz_category_id", el.id);
    showIsEdit([...isEdit]);
  };

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  useEffect(() => {
    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);

      setTimeout(function () {
        router.push("/emagz/categories");
      }, 1000);
      didDeleted(false);
    }
  }, [deleted, role]);

  const onSubmit = async (data) => {
    const category_id = `Pesona E-Magazine Edisi ${numberToRoman(
      parseInt(data["edisi"]),
    )} ${data["tahun"]}`;

    const category_en = `Pesona E-Magazine Edition ${numberToRoman(
      parseInt(data["edisi"]),
    )} ${data["tahun"]}`;
    data["category_id"] = category_id;
    data["category_en"] = category_en;

    const magazine_kategori = kategori.filter(
      (el) => el.category_id === category_id,
    );
    if (magazine_kategori.length == 0) {
      await axios
        .put(
          `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories/` +
            getCookies("emagz_category_id"),
          data,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((res) => {
          NotificationManager.success("Berhasil diubah", 3000);
          router.push("/emagz/categories");
        })
        .catch((err) => NotificationManager.error("Gagal diubah", 5000));
    } else {
      NotificationManager.error("Data sudah ada. Gagal mengubah data!", 5000);
    }
  };

  const onSubmitPost = async (data) => {
    const category_id = `Pesona E-Magazine Edisi ${numberToRoman(
      parseInt(data["edisi"]),
    )} ${data["tahun"]}`;

    const category_en = `Pesona E-Magazine Edition ${numberToRoman(
      parseInt(data["edisi"]),
    )} ${data["tahun"]}`;
    data["category_id"] = category_id;
    data["category_en"] = category_en;

    const magazine_kategori = kategori.filter(
      (el) => el.category_id === category_id,
    );
    if (magazine_kategori.length == 0) {
      await axios
        .post(
          `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories`,
          data,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((res) => {
          NotificationManager.success("Berhasil ditambahkan", 3000);
          router.push("/emagz/categories");
        })
        .catch((err) => NotificationManager.error("Gagal ditambahkan", 5000));
    } else {
      NotificationManager.error(
        "Data sudah ada. Gagal menambahkan data!",
        5000,
      );
    }
  };

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(
          `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories/` + id,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  return (
    <>
      <Layout>
        <SectionTitle title="E-MAGAZINES" subtitle="" />

        <div className="mb-10">
          <p className="text-lg my-5">Add new category</p>
          <div className="flex">
            <form onSubmit={handleSubmit(onSubmitPost)} className="flex">
              <div>
                <p>Edisi</p>
                <input
                  className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                  ref={register}
                  type="text"
                  name="edisi"
                  placeholder="dalam angka (ex: 1)"
                />
              </div>
              <div className="ml-5">
                <p>Tahun</p>
                <input
                  className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                  ref={register}
                  type="text"
                  name="tahun"
                />
              </div>
              <button
                type="submit"
                className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
              >
                Submit
              </button>
            </form>
          </div>
        </div>
        <Widget title="Daftar Kategori">
          <table className="table table-lg mt-10">
            <thead>
              <tr>
                <th className="border">
                  <h2 className="text-sm font-bold">No</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold ml-3">Kategori</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold ml-3">Edisi</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold ml-3">Tahun</h2>
                </th>
                <th className="border">
                  <h2 className="text-sm font-bold">Action</h2>
                </th>
              </tr>
            </thead>
            <tbody>
              {dataLink &&
                dataLink.map((el, i) => (
                  <tr key={i} className="border-b-2">
                    <td className="border">
                      <div className="flex items-center">
                        {i + 1 + (page - 1) * 10}
                      </div>
                    </td>
                    <td className="border">
                      <div className="flex items-center ml-3">
                        {el.category_id}
                      </div>
                    </td>
                    {!el.isEdit ? (
                      <>
                        <td className="border">
                          <div className="flex items-center ml-3">
                            {el.edisi}
                          </div>
                        </td>
                        <td className="border">
                          <div className="flex items-center ml-3">
                            {el.tahun}
                          </div>
                        </td>
                      </>
                    ) : (
                      <>
                        <td className="border">
                          <input
                            className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                            ref={register}
                            type="text"
                            name="edisi"
                            defaultValue={el.edisi}
                          />
                        </td>
                        <td className="border">
                          <form
                            onSubmit={handleSubmit(onSubmit)}
                            className="flex"
                          >
                            <input
                              className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                              ref={register}
                              type="text"
                              name="tahun"
                              defaultValue={el.tahun}
                            />
                            <button
                              type="submit"
                              className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                            >
                              Submit
                            </button>
                          </form>
                        </td>
                      </>
                    )}
                    <td className="border">
                      <div className="-ml-5">
                        <button
                          onClick={() => handleOnClick(el)}
                          className="ml-5"
                        >
                          <Icon.Edit className="text-green-500" size={16} />
                        </button>
                        <button
                          onClick={() => deleteItem(el.id)}
                          className="mx-5 "
                        >
                          <Icon.Trash2 className="text-red-600" size={16} />
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
          <Pagination
            url={"/emagz/categories"}
            lastPage={lastPage}
            page={page}
          />
        </Widget>
      </Layout>
    </>
  );
};

EmagzCategories.getInitialProps = async (ctx) => {
  const query = ctx.query;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * 10;

  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  const res = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories`,
  );
  const kategori = await res.json();

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories?_limit=10&_start=${start}`,
  );

  const dataLink = await resdata.json();
  dataLink.forEach((el) => (el.isEdit = false));

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/magazine-categories/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();
  return {
    dataLink,
    isAuthenticated,
    isContributor,
    isAuthor,
    page: +page,
    numberOfArticles,
    kategori,
  };
};

export default withRedux(EmagzCategories);
