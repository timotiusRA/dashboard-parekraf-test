import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import Layout from "../../layouts";
import SectionTitle from "../../components/section-title";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

const AddBanner = ({ isAuthenticated, isAuthor, isContributor }) => {
  const [startDate, setStartDate] = useState(new Date());
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);
  useEffect(() => {}, [role]);

  const onSubmit = async (data) => {
    if (data.description_en.length > 230 || data.description_id.length > 230) {
      NotificationManager.error("Deskripsi melebihi 230 karakter", "", 500);
    } else {
      if (data.files && data.files.length > 0) {
        const formData = new FormData();
        formData.append("files", data.files[0]);
        await axios
          .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `Bearer ${token}`,
            },
          })
          .then((res) => {
            data["image"] = res.data[0];
          })
          .catch((err) =>
            NotificationManager.error("Gambar gagal diupload", 5000),
          );
        delete data["files"];
      }
      await axios
        .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites`, data, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          NotificationManager.success("Berhasil Diupload", "", 500);
          setTimeout(function () {
            router.push("/banner/all-carousels");
          }, 1000);
        })
        .catch((err) => {
          NotificationManager.error("Error Upload", 5000);
        });
    }
  };

  return (
    <Layout>
      <div className="lg:px-2">
        <SectionTitle
          title="ADD CAROUSEL HOME"
          subtitle="Harap isi semua field"
        />
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="w-full grid grid-cols-1"
        >
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-6">
              <label className="m-2 ">Title ID</label>
              <input
                ref={register({ required: true })}
                type="text"
                name="title_id"
                className={`${style} resize`}
              />
            </div>
            <div className="col-span-6">
              <label className="m-2">Title EN</label>
              <input
                ref={register({ required: true })}
                type="text"
                name="title_en"
                className={`${style} resize`}
              />
            </div>
          </div>
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-6">
              <label className="m-2 ">Link</label>
              <input
                ref={register({ required: true })}
                type="text"
                name="url"
                className={`${style} resize`}
              />
            </div>
          </div>
          <div className="flex my-3">
            <div>
              <p className="ml-3 mb-3">Image</p>
              <input
                ref={register({ required: true })}
                type="file"
                name="files"
                className="mb-5 ml-3"
              />
            </div>
            <p>
              * Image with ratio 12 : 5
              <br />
              * Best Example : 1920 x 800
              <br />
              * Maximum size 2MB
              <br />
              * Image must high resolution
              <br />* Image type (JPG/PNG/GIF)
            </p>
          </div>
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-6">
              <label className="m-2 ">Description ID (max 230 character)</label>
              <textarea
                ref={register({ required: true })}
                type="text"
                name="description_id"
                className={`${style} resize`}
                rows={4}
              />
            </div>
            <div className="col-span-6">
              <label className="m-2">Description EN (max 230 character)</label>
              <textarea
                ref={register({ required: true })}
                type="text"
                name="description_en"
                className={`${style} resize`}
                rows={4}
              />
            </div>
          </div>
          <button className="hover:bg-white border border-primaryNavy text-primaryNavy m-3 bg-blue-300 rounded-lg p-2 text-lg">
            Submit
          </button>
        </form>
      </div>
    </Layout>
  );
};
AddBanner.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);

  return {
    isAuthenticated,
    isAuthor,
    isContributor,
  };
};
export default withRedux(AddBanner);
