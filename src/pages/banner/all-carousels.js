import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import Layout from "../../layouts";
import axios from "axios";
import useAuth from "../../lib/customHooks/useAuth";
import { useSelector, shallowEqual } from "react-redux";
import {
  checkServerSideCookie,
  checkAuthor,
  checkContributor,
  setCookies,
} from "../../lib/actions";
import { UseAuthor, UseContributor } from "../../lib/customHooks/useRole";
import Widget from "../../components/widget";
import SectionTitle from "../../components/section-title";
import TableBanner from "../../components/tables/tableCarousel";
import SearchBar from "../../components/tables/searchBar";
import TotalEntries from "../../components/tables/totalEntries";
import Pagination from "../../components/tables/pagination";
import Link from "next/link";
import { confirm } from "../../components/tables/confirm";

const Carousels = ({
  page,
  limit,
  start,
  numberOfArticles,
  dataLink,
  isAuthenticated,
  isAuthor,
  isContributor,
}) => {
  const [deleted, didDeleted] = useState(false);
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const lastPage = Math.ceil(numberOfArticles / limit);

  const { role, token } = useSelector(
    (state) => ({
      role: state.role,
      token: state.token,
    }),
    shallowEqual,
  );
  useAuth(isAuthenticated);
  UseAuthor(isAuthor);
  UseContributor(isContributor);

  const handleOnClick = (el) => {
    setCookies("banner_carousel_id", el.id);
    router.push("/banner/edit-carousel");
  };

  useEffect(() => {
    fetchData(input);

    if (deleted) {
      NotificationManager.info("Berhasil didelete", null, 500);
      setTimeout(function () {
        router.push("/banner/all-carousels");
      }, 1000);

      didDeleted(false);
    }
  }, [role, deleted, start, page, limit]);

  const deleteItem = async (id) => {
    if (
      await confirm({
        confirmation: "Are you sure?",
      })
    ) {
      await axios
        .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites/` + id, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          didDeleted(true);
        })
        .catch((err) => NotificationManager.error("Error", 5000));
      didDeleted(false);
    }
  };

  // SEARCH
  const [input, setInput] = useState("");
  const [searchData, setSearchData] = useState();
  const [pageSearch, setPageSearch] = useState(0);

  const fetchData = async (input) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites?title_id_contains=${input}&_limit=${limit}&_start=${start}`,
    );
    let resData = await res.json();
    resData.forEach((el) => (el.isEdit = false));
    setSearchData(resData);

    const numberOfSearchResultResponse = await fetch(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites/count?title_id_contains=${input}`,
    );

    const numberOfSearchResult = await numberOfSearchResultResponse.json();
    setPageSearch(Math.ceil(numberOfSearchResult / limit));
  };

  const updateInput = async (input) => {
    setInput(input);
    fetchData(input);
  };

  return (
    <>
      <Layout>
        <div className="flex justify-between">
          <SectionTitle title="Carousels" subtitle="" />
          <Link href="/banner/add-carousel">
            <button className="mb-5 bg-primaryNavy text-white rounded-lg p-3">
              Tambah Carousel
            </button>
          </Link>
        </div>

        <Widget title="">
          <div className="flex justify-between">
            <span className="text-lg font-semibold -mb-5">
              Daftar Carousel Home
            </span>
            <div className="flex">
              <TotalEntries url={"/banner/all-carousels"} limit={limit} />
              <SearchBar updateInput={updateInput} input={input} />
            </div>
          </div>
          <div className="w-auto">
            <TableBanner
              dataLink={input === "" ? dataLink : searchData}
              page={page}
              handleOnClick={handleOnClick}
              handleSubmit={handleSubmit}
              deleteItem={deleteItem}
              register={register}
              limit={limit}
            />
          </div>
          <Pagination
            url={"/banner/all-carousels"}
            lastPage={input === "" ? lastPage : pageSearch}
            page={page}
            limit={limit}
          />
        </Widget>
      </Layout>
    </>
  );
};

Carousels.getInitialProps = async (ctx) => {
  const { isAuthenticated } = checkServerSideCookie(ctx);
  const { isAuthor } = checkAuthor(ctx);
  const { isContributor } = checkContributor(ctx);
  const query = ctx.query;
  const limit = query.limit || 10;
  const page = query.page || 1;
  const start = +page === 1 ? 0 : (+page - 1) * +limit;

  const resdata = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites?_limit=${limit}&_start=${start}`,
  );

  let dataLink = await resdata.json();

  const numberOfArticlesResponse = await fetch(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/external-sites/count`,
  );

  const numberOfArticles = await numberOfArticlesResponse.json();

  return {
    dataLink,
    isAuthenticated,
    isAuthor,
    isContributor,
    page: +page,
    limit: +limit,
    start,
    numberOfArticles,
  };
};

export default withRedux(Carousels);
