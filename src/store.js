import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const initialState = {
  token: "",
  role: "",
  name: "",
  username: "",
  id: "",
  description: "",
  user: "",
  url: "",
  layout: "layout-1",
  direction: "ltr",
  collapsed: false,
  toggleRightSidebar: false,
  colors: [
    "gray",
    "red",
    "orange",
    "yellow",
    "green",
    "teal",
    "blue",
    "indigo",
    "purple",
    "pink",
  ],
  palettes: {
    background: "bg-white",
    logo: "bg-gray-900-nav",
    leftSidebar: "bg-gray-900-nav",
    rightSidebar: "white",
    navbar: "bg-gray-900",
    topNavigation: "bg-gray-900",
  },
  leftSidebar: {
    showButtonText: true,
    showSectionTitle: true,
    showLogo: true,
    showCard: true,
    showAccountLinks: false,
    showProjects: true,
    showTags: true,
    card: 1,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "AUTHENTICATE":
      return { ...state, token: action.payload };
    case "DEAUTHENTICATE":
      return { ...state, token: null };
    case "SET_ROLE":
      return { ...state, role: action.payload };
    case "SET_USERNAME":
      return { ...state, username: action.payload };
    case "SET_USER":
      return { ...state, user: action.payload };
    case "SET_ID":
      return { ...state, id: action.payload };
    case "SET_DEMO":
      return {
        ...state,
        layout: action.layout,
        collapsed: action.collapsed,
        direction: action.direction,
        leftSidebar: {
          ...action.leftSidebar,
        },
        palettes: {
          ...action.palettes,
        },
      };
    case "SET_PALETTE":
      return {
        ...state,
        palettes: {
          ...state.palettes,
          [`${action.key}`]: action.value,
        },
      };
    case "SET_LEFT_SIDEBAR_CONFIG":
      return {
        ...state,
        leftSidebar: {
          ...state.leftSidebar,
          [`${action.key}`]: action.value,
        },
      };
    case "SET_LAYOUT":
      if (action.layout === "layout-3" || action.layout === "layout-4") {
        return {
          ...state,
          layout: action.layout,
          collapsed: true,
        };
      }
      return {
        ...state,
        layout: action.layout,
        collapsed: false,
      };
      return {
        ...state,
        [`${key}`]: value,
      };
    case "SET_CONFIG":
      let { key, value } = { ...action.config };
      return {
        ...state,
        [`${key}`]: value,
      };
    default:
      return state;
  }
};

export const initializeStore = (preloadedState = initialState) => {
  return createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware(thunk)),
  );
};
