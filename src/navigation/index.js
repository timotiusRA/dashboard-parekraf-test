import React from "react";
import * as Icon from "react-feather";

const navigation = [
  {
    title: "",
    items: [
      {
        url: "/",
        icon: <Icon.Box size={20} />,
        title: "Dashboards",
        items: [],
      },
      {
        url: "/",
        icon: <Icon.BookOpen size={20} />,
        title: "Posts",
        items: [
          {
            url: "/articles/allArticles",
            title: "All posts",
            items: [],
          },
          {
            url: "/articles/add-article",
            title: "Add post",
            items: [],
          },
          {
            url: "/categories/all-categories",
            title: "Category",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Cast size={20} />,
        title: "Announcements",
        items: [
          {
            url: "/articles/all-announcements",
            title: "All Announcement",
            items: [],
          },
          {
            url: "/articles/add-article",
            title: "Add Announcement",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Edit size={20} />,
        title: "Survey & Pollings",
        items: [
          {
            url: "/survey/all-survey",
            title: "All Survey",
            items: [],
          },
          {
            url: "/polling/all-polling",
            title: "All Pollings",
            items: [],
          },
          {
            url: "/survey/add-survey",
            title: "Add Survey",
            items: [],
          },
          {
            url: "/polling/add-polling",
            title: "Add Pollings",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Smartphone size={20} />,
        title: "Cards",
        items: [
          {
            url: "/articles/all-cards",
            title: "All Cards",
            items: [],
          },
          {
            url: "/articles/kebijakan",
            title: "Kebijakan",
            items: [],
          },
          {
            url: "/articles/panduan-perjalanan-wisata",
            title: "Panduan Perjalanan Wisata",
            items: [],
          },
          {
            url: "/articles/pelatihan-parekraf",
            title: "Pelatihan Parekraf",
            items: [],
          },
          {
            url: "/articles/industri-parekraf",
            title: "Industri Parekraf",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Navigation size={20} />,
        title: "Menu",
        items: [
          {
            url: "/menu/all-menu",
            title: "All Menu",
            items: [],
          },
          {
            url: "/menu/add-menu",
            title: "Add Menu",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Image size={20} />,
        title: "Media",
        items: [
          {
            url: "/media/all-media",
            title: "All Media",
            items: [],
          },
          {
            url: "/media/upload-media",
            title: "Upload Media",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Bell size={20} />,
        title: "Events",
        items: [
          {
            url: "/event/all-event",
            title: "All Events",
            items: [],
          },
          {
            url: "/event/add-event",
            title: "Add Event",
            items: [],
          },
        ],
      },

      {
        url: "/",
        icon: <Icon.Airplay size={20} />,
        title: "Banners",
        items: [
          {
            url: "/banner/all-banners",
            title: "All banners",
            items: [],
          },
          {
            url: "/banner/add-banner",
            title: "Add banner",
            items: [],
          },
          {
            url: "/banner/all-carousels",
            title: "All Carousels",
            items: [],
          },
          {
            url: "/banner/add-carousel",
            title: "Add Carousel",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.HelpCircle size={20} />,
        title: "FAQ",
        items: [
          {
            url: "/faq/all-faq",
            title: "All FAQ",
            items: [],
          },
          {
            url: "/faq/add-faq",
            title: "Add FAQ",
            items: [],
          },
          {
            url: "/faq/all-categories",
            title: "FAQ Categories",
            items: [],
          },
        ],
      },
      {
        url: "/",
        icon: <Icon.Printer size={20} />,
        title: "E-Magazines",
        items: [
          {
            url: "/emagz/all-emagz",
            title: "All E-Magazines",
            items: [],
          },
          {
            url: "/emagz/add-emagz",
            title: "Add E-Magazine",
            items: [],
          },
          {
            url: "/emagz/categories",
            title: "Categories",
            items: [],
          },
        ],
      },

      {
        url: "/",
        icon: <Icon.Star size={20} />,
        title: "Profil",
        items: [
          {
            url: "/profil/profil-lembaga",
            title: "Profil Lembaga",
            items: [],
          },
          {
            url: "/profil/struktur-organisasi",
            title: "Struktur Organisasi",
            items: [],
          },
          {
            url: "/profil/profil-pimpinan",
            title: "Profil Pimpinan",
            items: [],
          },
          {
            url: "/profil/kategori-jabatan",
            title: "Kategori Jabatan",
            items: [],
          },
        ],
      },

      {
        url: "/",
        icon: <Icon.Grid size={20} />,
        title: "Widget",
        items: [
          {
            url: "/widget/all-widget",
            title: "Widget Chart",
            items: [],
          },

          {
            url: "/widget/ekonomi-kreatif",
            title: "Subsektor Ekonomi Kreatif",
            items: [],
          },
          {
            url: "/widget/spbe",
            title: "SPBE",
            items: [],
          },
          {
            url: "/widget/image-indonesia-travel",
            title: "Indonesia Travel",
            items: [],
          },
          {
            url: "/widget/image-wonderful",
            title: "Wonderful Image",
            items: [],
          },
          {
            url: "/organisasi/alamat-kementerian",
            title: "Alamat Kementerian",
            items: [],
          },
          {
            url: "/organisasi/alamat-upt",
            title: "Alamat UPT",
            items: [],
          },
          {
            url: "/organisasi/organisasi-publik",
            title: "Organisasi Publik",
            items: [],
          },
        ],
      },
      {
        url: "/social/edit",
        icon: <Icon.Speaker size={20} />,
        title: "Social Media",
        items: [],
      },
      {
        url: "/seo/seo",
        icon: <Icon.Search size={20} />,
        title: "SEO",
        items: [],
      },
      {
        url: "/alert-banner",
        icon: <Icon.Bell size={20} />,
        title: "Alert Banner",
        items: [],
      },
      {
        url: "/messages",
        icon: <Icon.MessageCircle size={20} />,
        title: "Messages",
        items: [],
      },
      {
        url: "/subscribers",
        icon: <Icon.Rss size={20} />,
        title: "Subscribers",
        items: [],
      },
      {
        url: "/external-links",
        icon: <Icon.Globe size={20} />,
        title: "External Links",
        items: [],
      },

      {
        url: "/",
        icon: <Icon.Users size={20} />,
        title: "Users",
        items: [
          {
            url: "/users/all-users",
            title: "All Users",
            items: [],
          },
          {
            url: "/users/add-user",
            title: "Add User",
            items: [],
          },
        ],
      },
    ],
  },
];
export default navigation;
