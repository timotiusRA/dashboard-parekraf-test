import { useEffect } from "react";
import { useRouter } from "next/router";
import { NotificationManager } from "react-notifications";

export const UseAuthor = (isAuthor) => {
  const route = useRouter();
  //   useEffect(() => {
  //     if (!isAuthor) {
  //       route.push("/pages/login");
  //     }
  //   }, [isAuthor]);

  useEffect(() => {
    if (isAuthor) {
      NotificationManager.error("Anda tidak memiliki akses", null);
      route.back();
    }
  }, []);

  return {
    isAuthor,
  };
};

export const UseContributor = (isContributor) => {
  const route = useRouter();

  useEffect(() => {
    if (isContributor) {
      NotificationManager.error("Anda tidak memiliki akses", null);
      route.back();
    }
  }, []);

  return {
    isContributor,
  };
};
