import { useEffect } from "react";
import { useRouter } from "next/router";

const UseAuth = (isAuthenticated) => {
  const route = useRouter();
  useEffect(() => {
    if (!isAuthenticated) {
      route.push("/pages/login");
    }
  }, [isAuthenticated]);

  useEffect(() => {
    if (isAuthenticated && route && route.pathname === "/pages/login") {
      route.push("/");
    }
  }, []);

  return {
    isAuthenticated,
  };
};

export default UseAuth;
