export const AUTHENTICATE = "AUTHENTICATE";
export const DEAUTHENTICATE = "DEAUTHENTICATE";
export const SET_ROLE = "SET_ROLE";
export const SET_USERNAME = "SET_USERNAME";
export const SET_ID = "SET_ID";
export const SET_USER = "SET_USER";
export const SET_EMAIL = "SET_EMAIL";
