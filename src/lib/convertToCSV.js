export const convertToCSV = (content) => {
  let finalVal = "";

  for (let i = 0; i < content.length; i++) {
    let value = content[i];

    for (let j = 0; j < value.length; j++) {
      const innerValue = value[j] === null ? "" : value[j].toString();
      let result = innerValue.replace(/"/g, '""');
      if (result.search(/("|,|\n)/g) >= 0) result = '"' + result + '"';
      if (j > 0) finalVal += ",";
      finalVal += result;
    }

    finalVal += "\n";
  }

  const download = document.createElement("a");
  download.setAttribute(
    "href",
    "data:text/csv;charset=utf-8," + encodeURIComponent(finalVal),
  );
  download.setAttribute("download", "test.csv");

  document.body.appendChild(download);
  download.click();
  document.body.removeChild(download);
};
