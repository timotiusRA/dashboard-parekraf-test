import axios from "axios";

export const postData = async (data, url, token) => {
  const res = await axios.post(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/${url}`,
    data,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
  return res;
};

export const postSurvey = async (data, token) => {
  const res = await postData(data, "surveys", token);
  return res.data ? res.data.id : null;
};

export const postSection = async (data, token) => {
  const res = await postData(data, "survey-question-categories", token);
  return res.data ? res.data.id : null;
};

export const postQuestion = async (data, token) => {
  const res = await postData(data, "survey-questions", token);
  return res.data ? res.data.id : null;
};

export const postOption = async (data, token) => {
  const res = await postData(data, "survey-question-options", token);
  return res.data ? res.data.id : null;
};

export const postPolling = async (data, token) => {
  const res = await postData(data, "pollings", token);
  return res.data ? res.data.id : null;
};

export const postPollingAnswer = async (data, token) => {
  const res = await postData(data, "polling-answers", token);
  return res.data ? res.data.id : null;
};
