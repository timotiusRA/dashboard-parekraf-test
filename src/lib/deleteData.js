import axios from "axios";

export const deleteData = async (url, token, id) => {
  const res = await axios.delete(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/${url}/${id}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
  return res;
};

export const deletePollingAnswer = async (id, token) => {
  const res = await deleteData("polling-answers", token, id);
  return res.data ? res.data.id : null;
};

export const deleteSurveyAnswer = async (id, token) => {
  const res = await deleteData("survey-question-options", token, id);
  return res.data ? res.data.id : null;
};

export const deleteSurveyQuestion = async (id, token) => {
  const res = await deleteData("survey-questions", token, id);
  return res.data ? res.data.id : null;
};
