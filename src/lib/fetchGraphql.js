import { request, gql } from "graphql-request";

export const getFromGraphql = (query) => {
  return request(`${process.env.NEXT_PUBLIC_FETCH_URL}/graphql`, query)
    .then((data) => data)
    .catch((err) => console.log(err));
};

export const fetchCategory = gql`
  {
    articleCategories {
      id
      category_en
      category_id
    }
  }
`;

const fetchSurvey = (limit) => gql`
  {
    surveys(
      limit: ${limit}
      where: { is_trash: false }
      sort: "created_at:desc"
    ) {
      id
      name
      status
      created_by {
        username
      }
      question_category_id
    }
  }
`;

const fetchPolling = (limit) => gql`
  {
    pollings(
      limit: ${limit}
      where: { is_trash: false }
      sort: "created_at:desc"
    ) {
      id
      name
      status
      question
      created_by {
        username
      }
      answer_id
    }
  }
`;

const getAllFetchResults = () => {
  return getFromGraphql(fetchCategory).then((respo) => ({
    articleCategories: respo && respo.articleCategories,
  }));
};

export const getArticleCategories = async () => {
  const response = await getAllFetchResults();
  return response;
};

export const getAllSurvey = async (limit) => {
  return getFromGraphql(fetchSurvey(limit)).then((res) => res.surveys);
};

export const getAllPolling = async (limit) => {
  return getFromGraphql(fetchPolling(limit)).then((res) => res.pollings);
};

const getSurveyOption = async (optionId) => {
  if (!optionId) {
    return [];
  }
  const query = gql`
      {
          surveyQuestionOptions(where: { id: [${optionId}] }) {
              id
              option
          }
      }
  `;
  const response = await getFromGraphql(query);
  const option = response.surveyQuestionOptions;

  return option;
};

const getSurveyQuestion = async (questionId) => {
  if (!questionId) {
    return [];
  }
  const query = gql`
      {
          surveyQuestions(where: { id: [${questionId}] }) {
              id
              question
              is_required
              option_id
              survey_question_type {
                  name
              }
          }
      }
  `;
  const response = await getFromGraphql(query);
  const question = response.surveyQuestions;

  for (const item of question) {
    item.option = await getSurveyOption(
      item.option_id && item.option_id.data ? item.option_id.data : false,
    );
    delete item.option_id;
  }
  return question;
};

const getSurveyCategory = async (categoryId) => {
  if (!categoryId) {
    return [];
  }
  const query = gql`
      {
          surveyQuestionCategories(where: { id: [${categoryId}] }) {
              id
              name
              desc
              question_id
          }
      }
  `;
  const response = await getFromGraphql(query);
  const categories = response.surveyQuestionCategories;

  for (const item of categories) {
    item.question = await getSurveyQuestion(
      item.question_id && item.question_id.data ? item.question_id.data : false,
    );
    delete item.question_id;
  }
  return categories;
};

export const getSurveyByID = async (id) => {
  const query = gql`
      {
          surveys(where: { is_trash: false, id: ${id} }, sort: "created_at:desc") {
              id
              name
              desc
              is_trash
              status
              start_date
              end_date
              question_category_id
          }
      }
  `;
  const response = await getFromGraphql(query);
  if (response.surveys.length === 0) {
    return {};
  }

  const survey = response.surveys[0];
  survey.categories = await getSurveyCategory(
    survey.question_category_id &&
      survey.question_category_id.data &&
      survey.question_category_id.data.length > 0
      ? survey.question_category_id.data
      : false,
  );
  delete survey.question_category_id;
  return survey;
};

const getPollingCount = async (optionId) => {
  const query = gql`
      {
          pollingUsers(where: { answer_id: [${optionId}] }) {
              id
          }
      }
  `;
  const response = await getFromGraphql(query);
  if (response.pollingUsers.length === 0) {
    return 0;
  }
  return response.pollingUsers.length;
};

const getPollingOption = async (optionId, notRecursive) => {
  const query = gql`
      {
          pollingAnswers(where: { id: [${optionId}] }) {
              id
              answer_choice
          }
      }
  `;
  const response = await getFromGraphql(query);
  if (response.pollingAnswers.length === 0) {
    return [];
  }

  const options = response.pollingAnswers;
  if (!notRecursive) {
    for (const option of options) {
      option.count = await getPollingCount(option.id);
    }
  }

  return options;
};

export const getPollingByID = async (id) => {
  const query = gql`
    {
        pollings (where: {is_trash: false, id: ${id} }) {
          id
          name
          question
          desc
          is_trash
          status
          start_date
          end_date
          answer_id
      }
    }
`;
  const response = await getFromGraphql(query);
  if (response.pollings.length === 0) {
    return {};
  }
  const pollings = response.pollings[0];
  pollings.option = await getPollingOption(pollings.answer_id.data);
  delete pollings.answer_id;

  return pollings;
};

const getSurveyQuestionById = async (questionId) => {
  if (!questionId) {
    return [];
  }
  const query = gql`
    {
      surveyQuestions(where: { id: ${questionId} }) {
        question
      }
    }
  `;
  const response = await getFromGraphql(query);
  if (response.surveyQuestions.length === 0) {
    return "undefined";
  }

  return response.surveyQuestions[0].question;
};

export const getSurveyUserData = async (questionId) => {
  if (!questionId) {
    return [];
  }
  const query = gql`
    {
      surveyUsers(where: { question_id: [${questionId}] }) {
        ip
        question_id
        created_at
        answer
      }
    }
  `;
  const response = await getFromGraphql(query);
  if (response.surveyUsers.length === 0) {
    return [];
  }
  const userData = response.surveyUsers;
  for (const item of userData) {
    item.question = await getSurveyQuestionById(item.question_id);
    delete item.question_id;
  }

  return response ? userData : [];
};

export const getPollingUserData = async (pollingId) => {
  if (!pollingId) {
    return [];
  }
  const query = gql`
    {
      pollingUsers(where: {polling_id: ${pollingId}}) {
        user_ip
        choice_date
        answer_id
      }
    }
  `;
  const response = await getFromGraphql(query);
  if (response.pollingUsers.length === 0) {
    return [];
  }
  const userData = response.pollingUsers;
  for (const data of userData) {
    const answer = await getPollingOption(data.answer_id, true);
    data.answer = answer[0].answer_choice
      ? answer[0].answer_choice
      : "undefined";
    delete data.answer_id;
  }

  return userData;
};
