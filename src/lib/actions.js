import axios from "axios";
import Cookies from "js-cookie";
import {
  AUTHENTICATE,
  DEAUTHENTICATE,
  SET_ROLE,
  SET_USERNAME,
  SET_ID,
  SET_USER,
  SET_EMAIL,
} from "../lib/actionTypes";
import Router from "next/router";

export const postLogin = (data) => async (dispatch) => {
  const { email, password } = data;

  try {
    const res = await axios.post(
      `${process.env.NEXT_PUBLIC_FETCH_URL}/auth/local`,
      {
        identifier: email,
        password: password,
      },
    );

    if (res.status === 200) {
      const token = res.data.jwt;
      const role = res.data.user.role.name;
      const username = res.data.user.username;
      const id = res.data.user.id;
      const email = res.data.user.email;

      dispatch({ type: AUTHENTICATE, payload: token });
      setCookies("kpkfadmtoken", token);
      dispatch({ type: SET_ROLE, payload: role });
      setCookies("kpkfadmrole", role);
      dispatch({ type: SET_USERNAME, payload: username });
      setCookies("kpkfadmusername", username);
      dispatch({ type: SET_ID, payload: id });
      setCookies("kpkfadmid", id);
      dispatch({ type: SET_EMAIL, payload: email });
      setCookies("kpkfadmemail", email);
      dispatch({
        type: SET_USER,
        payload: {
          name: username,
          email: email,
          location: "Jakarta",
          company: "Kemenparekraf/Baparekraf",
          description: role,
          img: "m1.png",
          color: "green",
          country: "Indonesia",
        },
      });
      setCookies("kpkfadmuser", {
        name: username,
        email: res.data.user.email,
        location: "Jakarta",
        company: "Kemenparekraf/Baparekraf",
        description: role,
        img: "m1.png",
        color: "green",
        country: "Indonesia",
      });
      Router.push("/");
    }
  } catch (error) {
    alert("Invalid password/username");
  }
};

export const setCookies = (key, value) => {
  if (process.browser) {
    Cookies.set(key, value, {
      expires: 1, // 1 day
      path: "/",
    });
  }
};

export const reauthenticate = ({ token, role, username, id, email }) => {
  return (dispatch) => {
    dispatch({ type: AUTHENTICATE, payload: token });
    dispatch({ type: SET_ROLE, payload: role });
    dispatch({ type: SET_USERNAME, payload: username });
    dispatch({ type: SET_ID, payload: id });
    dispatch({ type: SET_EMAIL, payload: email });
    dispatch({
      type: SET_USER,
      payload: {
        name: username,
        email: email,
        location: "Jakarta",
        company: "Kemenparekraf/Baparekraf",
        description: role,
        img: "m1.png",
        color: "green",
        country: "Indonesia",
      },
    });
  };
};

// removing the token
export const deauthenticate = () => {
  return (dispatch) => {
    removeCookies("kpkfadmtoken");
    dispatch({ type: DEAUTHENTICATE });
    removeCookies("kpkfadmrole");
    dispatch({ type: SET_ROLE, payload: null });
    removeCookies("kpkfadmusername");
    dispatch({ type: SET_USERNAME, payload: null });
    removeCookies("kpkfadmid");
    dispatch({ type: SET_ID, payload: null });

    Router.replace("/pages/login");
  };
};

export const removeCookies = (key, value) => {
  if (process.browser) {
    Cookies.remove(key, value, {
      expires: 1, // 1 day
    });
  }
};

const getCookiesFromBrowser = (key) => {
  return Cookies.get(key);
};

const getCookiesFromServer = (key, req) => {
  if (req) {
    if (!req.headers.cookie) {
      return undefined;
    }
    const rawCookie = req.headers.cookie
      .split(";")
      .find((c) => c.trim().startsWith(`${key}=`));
    if (!rawCookie) {
      return undefined;
    }
    return rawCookie.split("=")[1];
  }
};

export const getCookies = (key, req) => {
  return process.browser
    ? getCookiesFromBrowser(key)
    : getCookiesFromServer(key, req);
};

export const checkServerSideCookie = (ctx) => {
  let isAuthenticated = false;

  if (!!ctx.req) {
    if (ctx.req.headers.cookie) {
      const token = getCookies("kpkfadmtoken", ctx.req);
      const role = getCookies("kpkfadmrole", ctx.req);
      const username = getCookies("kpkfadmusername", ctx.req);
      const user = getCookies("kpkfadmuser", ctx.req);
      const email = getCookies("kpkfadmemail", ctx.req);
      ctx.reduxStore.dispatch(
        reauthenticate({ token, role, username, user, email }),
      );
    }
  } else {
    const token = ctx.reduxStore.getState().token;

    if (token && ctx.pathname === "/pages/login") {
      setTimeout(function () {
        Router.push("/");
      }, 0);
    }
  }
  const { token } = ctx.reduxStore.getState();
  if (token) {
    isAuthenticated = true;
  }
  return {
    isAuthenticated,
  };
};

export const checkAuthor = (ctx) => {
  let isAuthor = false;

  const { role } = ctx.reduxStore.getState();
  if (role === "Author") {
    isAuthor = true;
  }
  return {
    isAuthor,
  };
};
export const checkContributor = (ctx) => {
  let isContributor = false;

  const { role } = ctx.reduxStore.getState();
  if (role === "Contributor") {
    isContributor = true;
  }
  return {
    isContributor,
  };
};
