import axios from "axios";

export const updateData = async (data, url, token, id) => {
  const res = await axios.put(
    `${process.env.NEXT_PUBLIC_FETCH_URL}/${url}/${id}`,
    data,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
  return res;
};

export const updateSurvey = async (data, token, id) => {
  const res = await updateData(data, "surveys", token, id);
  return res.data ? res.data.id : null;
};

export const updateSurveyQuestionCategory = async (data, token, id) => {
  const res = await updateData(data, "survey-question-categories", token, id);
  return res.data ? res.data.id : null;
};

export const updateSurveyQuestion = async (data, token, id) => {
  const res = await updateData(data, "survey-questions", token, id);
  return res.data ? res.data.id : null;
};

export const updatePolling = async (data, token, id) => {
  const res = await updateData(data, "pollings", token, id);
  return res.data ? res.data.id : null;
};
