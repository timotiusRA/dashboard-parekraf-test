import { useState, useEffect } from "react";
import Link from "next/link";
import { useForm } from "react-hook-form";
import * as Icon from "react-feather";

let socialMediaColors = {
  facebook: "#365397",
  linkedin: "#006db3",
  google: "#e0452c",
  github: "#2f2f2f",
};

const Login1 = ({ doLogin }) => {
  const [checked, setChecked] = useState();
  const { register, handleSubmit, watch, errors } = useForm();
  const onSubmit = (data) => {
    doLogin(data);
  };
  const aneh = (e) => {
    e.preventDefault();
    setChecked(!checked);
  };

  return (
    <>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="flex flex-col text-sm mb-4 w-full"
      >
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">Email/Username</span>
            <input
              name="email"
              type="text"
              ref={register({ required: true })}
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter your email/username"
            />
          </label>
          {errors.email && (
            <p className="mt-1 text-xs text-red-500">
              Email/Username is required
            </p>
          )}
        </div>
        <div className="w-full mb-4">
          <label className="block">
            <span className="text-default">Password</span>
            <button type="button" onClick={(e) => aneh(e)} className="ml-5">
              {!checked ? <Icon.Eye size={20} /> : <Icon.EyeOff size={20} />}
            </button>
            <input
              name="password"
              type={!checked ? "password" : "text"}
              ref={register({ required: true })}
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter your password"
            />
          </label>
          {errors.password && (
            <p className="mt-1 text-xs text-red-500">Password is required</p>
          )}
        </div>

        <div className="w-full">
          <input
            type="submit"
            className="px-4 py-3 uppercase font-bold text-white bg-primaryNavy rounded-lg hover:bg-gold1 focus:outline-none active:outline-none"
            value="Login"
          />
        </div>
      </form>

      {/* <div className="w-full">
        <span>
          <Link href="/pages/forgot-password">
            <a className="link">Forgot password?</a>
          </Link>
        </span>
      </div> */}
    </>
  );
};

export default Login1;
