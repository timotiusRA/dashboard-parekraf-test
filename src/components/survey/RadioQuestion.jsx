import { useState } from "react";
import * as Icon from "react-feather";
import { postOption } from "../../lib/postData";
import { updateSurveyQuestion } from "../../lib/updateData";
import { deleteSurveyAnswer, deleteSurveyQuestion } from "../../lib/deleteData";

export default function Question({ getData, data, num, token }) {
  const { id, is_required, question, option } = data;
  const [newAnswer, setNewAnswer] = useState("");

  const deleteOption = async (optionId) => {
    const newOptionId = option
      .map((item) => parseInt(item.id))
      .filter((item) => item !== parseInt(optionId));

    await updateSurveyQuestion({ option_id: { data: newOptionId } }, token, id);
    await deleteSurveyAnswer(optionId, token);
    window.location.reload();
  };

  const addOption = async () => {
    const newOptionId = option.map((item) => parseInt(item.id));
    const newAnswerId = await postOption({ option: newAnswer }, token);
    newOptionId.push(newAnswerId);
    await updateSurveyQuestion({ option_id: { data: newOptionId } }, token, id);
    window.location.reload();
  };

  const deleteQuestion = async () => {
    for (const item of option) {
      await deleteSurveyAnswer(item.id, token);
    }
    await deleteSurveyQuestion(id, token);
    window.location.reload();
  };

  return (
    <div className={`m-auto sm:mx-20 rounded-xl shadow-lg pb-3 pt-3 relative`}>
      <button
        onClick={() => {
          deleteQuestion();
        }}
        className={`bg-red-600 text-white p-1 text-sm rounded-md absolute top-0 right-0 mt-12 mr-10`}
      >
        Delete Question
      </button>
      <div className={`text-sm sm:text-sm md:text-lg font-bold p-10`}>
        {`${num}. `}
        <input
          placeholder={question}
          name={`survey-questions:${id}:question`}
          className={`text-sm sm:text-sm md:text-lg font-bold p-1 w-3/5`}
          onChange={getData}
        ></input>
        {is_required ? <span className="text-red-600">*</span> : ""}
        <div className="mt-5 mb-5">
          {option.map((item, index) => {
            return (
              <div
                className={`mb-2 font-medium rounded-md border-2 p-2 cursor-pointer flex justify-between`}
                key={index}
              >
                <div>
                  <input type="radio" id={`radio${item.id}`} name="radio" />
                  <label
                    htmlFor={`radio${item.id}`}
                    className={`ml-5 cursor-pointer`}
                  >
                    <input
                      placeholder={item.option}
                      name={`survey-question-options:${item.id}:option`}
                      className={`text-sm sm:text-sm md:text-lg font-bold p-1 w-3/5`}
                      onChange={getData}
                    ></input>
                  </label>
                </div>
                <Icon.Trash2
                  className="text-red-600 cursor-pointer my-auto"
                  size={20}
                  onClick={() => {
                    deleteOption(item.id);
                  }}
                />
              </div>
            );
          })}
          <div className="grid grid-cols-3 gap-4">
            <div className="col-span-2">
              <input
                onChange={(event) => {
                  setNewAnswer(event.target.value);
                }}
                placeholder="Add new option here..."
                className={`text-sm font-semibold p-1 w-full border-2 border-light-blue-500 border-opacity-50 rounded-md`}
              ></input>
            </div>
            <button
              onClick={() => {
                addOption();
              }}
              className={`bg-red-600 text-white p-1 text-sm rounded-md`}
            >
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
