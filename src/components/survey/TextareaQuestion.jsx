import { deleteSurveyAnswer, deleteSurveyQuestion } from "../../lib/deleteData";

export default function Question({ getData, data, num, token }) {
  const { id, is_required, question } = data;

  const deleteQuestion = async () => {
    await deleteSurveyQuestion(id, token);
    window.location.reload();
  };

  return (
    <div className={`m-auto sm:mx-20 rounded-xl shadow-lg pb-3 pt-3 relative`}>
      <button
        onClick={() => {
          deleteQuestion();
        }}
        className={`bg-red-600 text-white p-1 text-sm rounded-md absolute top-0 right-0 mt-12 mr-10`}
      >
        Delete Question
      </button>
      <div className={`text-sm sm:text-sm md:text-lg font-bold p-10`}>
        {`${num}. `}
        <input
          placeholder={question}
          name={`survey-questions:${id}:question`}
          className={`text-sm sm:text-sm md:text-lg font-bold p-1 w-3/5`}
          onChange={getData}
        ></input>
        {is_required ? <span className="text-red-600">*</span> : ""}
        <div className="mt-5 mb-5">
          <input
            type="text"
            placeholder="Jawaban anda di sini"
            className="border-2 w-full p-3 rounded-md"
          />
        </div>
      </div>
    </div>
  );
}
