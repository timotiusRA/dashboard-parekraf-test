import * as Icon from "react-feather";

export default function TableBanner({
  dataLink,
  page,
  handleSubmit,
  register,
  onSubmit,
  handleOnClick,
  deleteItem,
  limit,
}) {
  return (
    <table className="table-fixed mt-10">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold">No</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Image</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Title ID</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Title EN</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Link</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border">
                <div className="flex items-center">
                  {i + 1 + (page - 1) * limit}
                </div>
              </td>
              {!el.isEdit ? (
                <>
                  <td className="border w-300">
                    <img
                      className=""
                      src={el.image && el.image.url}
                      alt="gambar-i"
                    />
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">
                      {el.title_id && el.title_id}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">
                      {el.title_en && el.title_en}
                    </div>
                  </td>
                  <td className="border break-all">
                    <div className="flex items-center">
                      <a href={el.link && el.link} className="text-clickable">
                        {el.link && el.link}
                      </a>
                    </div>
                  </td>
                </>
              ) : (
                <>
                  <td className="border">
                    <input ref={register} type="file" name="files" />
                  </td>
                  <td className="border">
                    <textarea
                      className="px-3 py-1 resize appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="text"
                      name="title_id"
                      rows={5}
                      defaultValue={el.title_id && el.title_id}
                    />
                  </td>
                  <td className="border">
                    <textarea
                      className="px-3 py-1 resize appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="text"
                      name="title_en"
                      rows={5}
                      defaultValue={el.title_en && el.title_en}
                    />
                  </td>

                  <td className="border">
                    <form onSubmit={handleSubmit(onSubmit)} className="flex">
                      <textarea
                        className="px-3 py-1 resize appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                        ref={register}
                        type="text"
                        name="link"
                        rows={5}
                        defaultValue={el.link && el.link}
                      />
                      <button
                        type="submit"
                        className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                      >
                        Submit
                      </button>
                    </form>
                  </td>
                </>
              )}
              <td className="border">
                <div className="flex">
                  <button onClick={() => handleOnClick(el)} className="mx-2">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
