import dynamic from "next/dynamic";
import Select from "react-select";

const Editor = dynamic(() => import("../../components/tables/editor"), {
  ssr: false,
});

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

export default function InputFaq({
  handleSubmit,
  onSubmit,
  htmlEN,
  htmlID,
  setHtmlEN,
  setHtmlID,
  register,
  article,
  kategori,
  setKategoriFaq,
  kategoriFaq,
}) {
  let options = [];
  kategori.forEach((element) => {
    let option = {
      label: element.category_id,
      value: element.id,
    };
    options.push(option);
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Question ID</label>
          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="question_id"
            defaultValue={article && article.question_id}
          />
        </div>
        <div className="col-span-6">
          <label className="m-2 ">Question EN</label>
          <input
            className={`${style} `}
            ref={register}
            type="text"
            name="question_en"
            defaultValue={article && article.question_en}
          />
        </div>
      </div>

      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-3 m-2 relative z-10">
          <label className="">Kategori</label>
          <Select
            options={options}
            onChange={(opt) => setKategoriFaq(opt.value)}
            defaultValue={{
              label: article && article.faq_category['category_id'],
              value: article && article.faq_category['id'],
            }}
          />
        </div>
      </div>
      <label className="m-2">Answer ID</label>
      <Editor onChange={setHtmlID} value={htmlID} />
      <label className="m-2">Answer EN</label>
      <Editor onChange={setHtmlEN} value={htmlEN} />
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
