import * as Icon from "react-feather";

export default function TableCards({
  thKu,
  mainFeature,
  handleOnClick,
  handleSubmit,
  onSubmit,
  register,
}) {
  return (
    <table className="table-fixed mt-5 mb-10">
      <thead>
        <tr>
          {thKu.map((el, i) => (
            <th key={i} className="border">
              <h2 className="text-sm font-bold">{el}</h2>
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {mainFeature &&
          mainFeature.map((el, i) => (
            <tr key={i} className="border-b-2 w-85 h-140">
              {!el.isEdit ? (
                <>
                  <td className="border">
                    <img src={el.image && el.image.url} alt="gambar-i" />
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">{el.title_id && el.title_id}</div>
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">{el.title_en && el.title_en}</div>
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">{el.link && el.link}</div>
                  </td>
                </>
              ) : (
                <>
                  <td className="border">
                    <input
                      ref={register}
                      type="file"
                      name="files"
                      className="w-1/2"
                    />
                  </td>
                  <td className="border ">
                    <textarea
                      className="w-full h-140 my-2 px-3 resize py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="text"
                      name="title_id"
                      defaultValue={el.title_id && el.title_id}
                    />
                  </td>
                  <td className="border">
                    <textarea
                      className="px-3 w-full h-140 my-2 resize py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="text"
                      name="title_en"
                      defaultValue={el.title_en && el.title_en}
                    />
                  </td>

                  <td className="border">
                    <form onSubmit={handleSubmit(onSubmit)} className="flex">
                      <input
                        className="px-3 py-1 w-full h-140 my-2 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                        ref={register}
                        type="text"
                        name="link"
                        defaultValue={el.link && el.link}
                      />
                      <button
                        type="submit"
                        className="border my-10 border-primaryNavy bg-primaryNavy text-white px-1 ml-1 rounded-lg hover:bg-white hover:text-primaryNavy"
                      >
                        Submit
                      </button>
                    </form>
                  </td>
                </>
              )}
              <td className="border">
                <div className="flex justify-center">
                  <button onClick={() => handleOnClick(el)} className="">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
