const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

export default function InputOrganisasiPublik({
  el,
  handleSubmit,
  onSubmit,
  register,
  kategori,
  statusKategori,
}) {
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full grid grid-cols-1">
      <div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-6">
            <label className="m-2 ">Title ID</label>
            <input
              ref={register}
              type="text"
              name="title_id"
              defaultValue={el && el.title_id}
              className={`${style}`}
            />
          </div>
          <div className="col-span-6">
            <label className="m-2 ">Title EN</label>
            <input
              ref={register}
              type="text"
              name="title_en"
              defaultValue={el && el.title_en}
              className={`${style}`}
            />
          </div>
          {statusKategori === null || statusKategori === "vito" ? (
            <div className="col-span-3">
              <label className="m-2">Chief Name (untuk kategori VITO)</label>
              <input
                ref={register}
                type="text"
                name="chief"
                defaultValue={el && el.chief}
                className={`${style}`}
              />
            </div>
          ) : null}
        </div>
        {statusKategori === null ||
        statusKategori === "kementerian dan lembaga" ? (
          <div className="grid grid-cols-6 w-full mr-8">
            <div className="col-span-3">
              <label className="m-2 ">
                Position ID (untuk kategori Kementerian dan Lembaga)
              </label>
              <input
                ref={register}
                type="text"
                name="position_id"
                defaultValue={el && el.position_id}
                className={`${style}`}
              />
            </div>
            <div className="col-span-3 ml-5">
              <label className="m-2">
                Position EN (untuk kategori Kementerian dan Lembaga)
              </label>
              <input
                ref={register}
                type="text"
                name="position_en"
                defaultValue={el && el.position_en}
                className={`${style}`}
              />
            </div>
          </div>
        ) : null}
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Address ID</label>

            <textarea
              className={`${style} resize`}
              ref={register}
              type="text"
              name="address_id"
              defaultValue={el && el.address_id}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Address EN</label>
            <textarea
              className={`${style} resize`}
              ref={register}
              type="text"
              name="address_en"
              defaultValue={el && el.address_en}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2 ">Label ID</label>
            <input
              ref={register}
              type="text"
              name="label_id"
              defaultValue={el && el.label_id}
              className={`${style}`}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Label EN</label>
            <input
              ref={register}
              type="text"
              name="label_en"
              defaultValue={el && el.label_en}
              className={`${style}`}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Telp</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="telp"
              defaultValue={el && el.telp}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Website</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="website"
              defaultValue={el && el.website}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2 ">Fax</label>
            <input
              ref={register}
              type="text"
              name="fax"
              defaultValue={el && el.fax}
              className={`${style}`}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Email</label>
            <input
              ref={register}
              type="text"
              name="email"
              defaultValue={el && el.email[0]["email"]}
              className={`${style}`}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Kategori</label>
            <select
              className="w-full ml-2 p-2"
              id="grid-state"
              ref={register({ required: true })}
              name="organisasi_publik"
              defaultValue={
                el && el.organisasi_publik && el.organisasi_publik["tab_id"]
              }
            >
              {kategori &&
                kategori.map((el, i) => (
                  <option value={el.id} key={i}>
                    {el.tab_id}
                  </option>
                ))}
            </select>
          </div>
        </div>
      </div>
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
