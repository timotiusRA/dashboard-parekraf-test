import * as Icon from "react-feather";

export default function TableFaqCategories({
  dataLink,
  page,
  handleSubmit,
  register,
  onSubmit,
  handleOnClick,
  deleteItem,
  limit,
}) {
  return (
    <table className="table table-lg mt-10">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold">No</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Category ID</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Category EN</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border">
                <div className="flex items-center">
                  {i + 1 + (page - 1) * limit}
                </div>
              </td>
              {!el.isEdit ? (
                <>
                  <td className="border">
                    <div className="flex items-center ml-3">
                      {el.category_id && el.category_id}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center ml-3">
                      {el.category_en && el.category_en}
                    </div>
                  </td>
                </>
              ) : (
                <>
                  <td className="border">
                    <input
                      className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="text"
                      name="category_id"
                      defaultValue={el.category_id && el.category_id}
                    />
                  </td>
                  <td className="border">
                    <form onSubmit={handleSubmit(onSubmit)} className="flex">
                      <input
                        className="py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                        ref={register}
                        type="text"
                        name="category_en"
                        defaultValue={el.category_en && el.category_en}
                      />
                      <button
                        type="submit"
                        className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                      >
                        Submit
                      </button>
                    </form>
                  </td>
                </>
              )}
              <td className="border">
                <div className="-ml-5">
                  <button onClick={() => handleOnClick(el)} className="ml-5">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="mx-5 ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
