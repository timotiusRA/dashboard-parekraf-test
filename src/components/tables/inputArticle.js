import DatePicker from "react-datepicker";
import { WithContext as ReactTags } from "react-tag-input";
import dynamic from "next/dynamic";
// import Select from "react-select";

const Editor = dynamic(() => import("../../components/tables/editor"), {
  ssr: false,
});

const Select = dynamic(() => import("react-select"), {
  ssr: false,
});

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

export default function InputArticle({
  defaultStatus,
  kategoriArtikel,
  setKategoriArtikel,
  handleSubmit,
  onSubmit,
  register,
  article,
  kategori,
  //ckeditor
  htmlEN,
  htmlID,
  setHtmlEN,
  setHtmlID,
  //datepicker
  startDate,
  setStartDate,
  publishDate,
  setPublishDate,
  expireDate,
  setExpireDate,
  // react tags
  tagsID,
  tagsEN,
  suggestionsID,
  suggestionsEN,
  handleDeleteID,
  handleAdditionID,
  handleDragID,
  handleDeleteEN,
  handleAdditionEN,
  handleDragEN,
  delimiters,
  setKategoriCardArtikel,
}) {
  let options = [];
  kategori.forEach((element) => {
    let option = {
      label: element.category_id,
      value: element.id,
    };
    options.push(option);
  });

  const optionsCard = [
    { label: "Kebijakan", value: "kebijakan" },
    { label: "Panduan Perjalanan Wisata", value: "panduan perjalanan wisata" },
    { label: "Pelatihan Parekraf", value: "pelatihan parekraf" },
    { label: "Industri Parekraf", value: "industri parekraf" },
  ];

  const status =
    defaultStatus && defaultStatus === "published"
      ? ["published", "draft"]
      : ["draft", "published"];

  const handleStatusChange = (e) => {
    if (e?.target) {
      e.preventDefault();
      if (e.target.value === "draft") setPublishDate();
      if (e.target.value === "published") setPublishDate(new Date());
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
        <div className="grid grid-cols-6 mr-8">
          <div className="col-span-6">
            <label className="m-2 ">Title (ID)</label>
            <p className="ml-2">
              karakter yang diperbolehkan untuk judul: - # / : ! ' " ? @ ,
            </p>
            <input
              ref={register}
              type="text"
              name="title_id"
              className={`${style} w-full `}
              defaultValue={(article && article.title_id) || ""}
            />
          </div>
          <div className="col-span-6">
            <label className="m-2 ">Title (EN)</label>
            <input
              ref={register}
              type="text"
              name="title_en"
              className={`${style} w-full  `}
              defaultValue={(article && article.title_en) || ""}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 ">
          <div className="col-span-3 m-2 mr-3">
            <label className="">Tag (ID)</label>
            <ReactTags
              inputFieldPosition="bottom"
              tags={tagsID}
              suggestions={suggestionsID}
              handleDelete={handleDeleteID}
              handleAddition={handleAdditionID}
              handleDrag={handleDragID}
              delimiters={delimiters}
            />
          </div>
          <div className="col-span-3 my-2 mr-6 ml-2">
            <label className=" ">Tag (EN)</label>
            <ReactTags
              inputFieldPosition="bottom"
              tags={tagsEN}
              suggestions={suggestionsEN}
              handleDelete={handleDeleteEN}
              handleAddition={handleAdditionEN}
              handleDrag={handleDragEN}
              delimiters={delimiters}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 ">
          <div className="col-span-3 mr-5">
            <label className="m-2">Day (ID)</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="day_id"
              defaultValue={(article && article.day_id) || ""}
            />
          </div>
          <div className="col-span-3 mr-8">
            <label className="m-2">Day (EN)</label>
            <input
              className={`${style}  `}
              ref={register}
              type="text"
              name="day_en"
              defaultValue={(article && article.day_en) || ""}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 ">
          <div className="col-span-3 mr-5">
            <label className="m-2 ">Posted By (ID)</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="posted_by_id"
              defaultValue={(article && article.posted_by_id) || ""}
            />
          </div>
          <div className="col-span-3 mr-8">
            <label className="m-2 ">Posted By (EN)</label>
            <input
              className={`${style}  `}
              ref={register}
              type="text"
              name="posted_by_en"
              defaultValue={(article && article.posted_by_en) || ""}
            />
          </div>
        </div>

        <div className="grid grid-cols-6 my-5">
          <div className="col-span-3 ml-2 mr-4">
            <label className="">Kategori</label>
            <Select
              options={options}
              onChange={(opt) => setKategoriArtikel(opt.value)}
              defaultValue={{
                label:
                  article &&
                  article.article_categories &&
                  article.article_categories[0] &&
                  article.article_categories[0].category_id,
                value:
                  article &&
                  article.article_categories &&
                  article.article_categories[0] &&
                  article.article_categories[0].id,
              }}
            />
          </div>

          <div className="col-span-3 ml-2 mr-6">
            <label className="">Status</label>
            <select
              onChange={(e) => handleStatusChange(e)}
              className="w-full p-2 rounded-lg bg-white border border-gray-500 focus:outline-none"
              id="grid-state"
              ref={register}
              name="status"
            >
              {status.map((el, i) => (
                <option value={el} key={i}>
                  {el}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="grid grid-cols-6  my-5">
          <div className="col-span-3 ml-2 mr-4">
            <label className="">
              Kategori Card (Optional: pilih salah satu untuk memasukkan artikel
              ke Card)
            </label>
            <Select
              options={optionsCard}
              onChange={(opt) => setKategoriCardArtikel(opt.value)}
              defaultValue={{
                label: article && article.tag_category,
                value: article && article.tag_category,
              }}
            />
          </div>
        </div>
        <div className="grid grid-cols-6  my-5">
          <div className="col-span-3 ml-5">
            <label className="">Banner</label>
            <br />
            <input ref={register} type="file" name="files" className="" />
            <div className="mt-5">
              * Image resolution 860 x 400px (43 : 20)
              <br /> * Maximum size 2MB <br />* Image type (JPG/PNG)
            </div>
          </div>
          <div className="col-span-3">
            <div className="col-span-2 ml-4">
              <label className="my-2">Article Date</label>
              <div className="">
                <DatePicker
                  selected={startDate}
                  onChange={(date) => setStartDate(date)}
                />
              </div>
            </div>

            <div className="col-span-2 ml-4 mt-3">
              <label className="my-2">Publish Date</label>
              <div className="">
                <DatePicker
                  selected={publishDate}
                  onChange={(date) => setPublishDate(date)}
                />
              </div>
            </div>
            <div className="col-span-2 ml-4 mt-3">
              <label className="my-2">Expire Date</label>
              {/* border border-black mt-3 w-1/2 */}
              <div className="">
                <DatePicker
                  selected={expireDate}
                  onChange={(date) => setExpireDate(date)}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="grid grid-cols-6 mr-8">
          <div className="col-span-6">
            <label className="m-2">Headline (ID)</label>
            <textarea
              className={`${style} resize`}
              ref={register}
              type="text"
              name="headline_id"
              rows={4}
              defaultValue={(article && article.headline_id) || ""}
            />
          </div>
          <div className="col-span-6">
            <label className="m-2 ">Headline (EN)</label>
            <textarea
              className={`${style} resize`}
              ref={register}
              type="text"
              name="headline_en"
              rows={4}
              defaultValue={(article && article.headline_en) || ""}
            />
          </div>
        </div>

        <div className="ml-2 mt-5">
          <label className="">Content (ID)</label>
          <Editor onChange={setHtmlID} value={htmlID} />
        </div>
        <div className="ml-2 mt-5">
          <label className="">Content (EN)</label>
          <Editor onChange={setHtmlEN} value={htmlEN} />
        </div>
        <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 ml-2 bg-blue-300 rounded-lg p-2 text-lg">
          Submit
        </button>
      </form>
    </>
  );
}
