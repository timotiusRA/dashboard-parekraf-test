import * as Icon from "react-feather";

export default function TableFaq({
  dataLink,
  page,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-10">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold">No</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Category</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Question ID</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Answer ID</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border">
                <div className="flex items-center">
                  {i + 1 + (page - 1) * 10}
                </div>
              </td>
              <td className="border">
                <div className="flex items-center ml-3">
                  {el.faq_category && el.faq_category["category_id"]}
                </div>
              </td>
              <td className="border">
                <div className="flex items-center ml-3">{el.question_id && el.question_id}</div>
              </td>

              <td className="border">
                <div className="flex items-center ml-3">{el.answer_id && el.answer_id}</div>
              </td>

              <td className="border">
                <div className="flex justify-center">
                  <button onClick={() => handleOnClick(el)} className="ml-5">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="mx-3 ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
