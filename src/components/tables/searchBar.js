export default function SearchBar({ updateInput, input }) {
  return (
    <div className="pt-2 text-gray-600">
      <input
        className="border-2 border-gray-300 bg-white h-10 pl-5 pr-16 rounded-lg text-sm focus:outline-none"
        type="search"
        name="search"
        placeholder="Search"
        input={input}
        onChange={(e) => updateInput(e.target.value)}
      />
    </div>
  );
}
