import dynamic from "next/dynamic";
import Select from "react-select";

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";
const Editor = dynamic(() => import("../../components/tables/editor"), {
  ssr: false,
});

export default function InputProfilPimpinan({
  handleSubmit,
  onSubmit,
  register,
  article,
  setHtmlEN,
  setHtmlID,
  htmlEN,
  htmlID,
  setEksDivisi,
  divisi,
}) {
  let options = [];

  divisi.forEach((element) => {
    let option = {
      label: element.divisi_id,
      value: element.id,
    };
    options.push(option);
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Nama</label>
          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="name"
            defaultValue={article && article.name}
          />
        </div>
        <div className="col-span-6">
          <label className="m-2 ">Position (ID)</label>
          <input
            className={`${style} `}
            ref={register}
            type="text"
            name="position_id"
            defaultValue={article && article.position_id}
          />
        </div>
      </div>

      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Position (EN) </label>
          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="position_en"
            defaultValue={article && article.position_en}
          />
        </div>

        <div className="col-span-6">
          <label className="m-2 ">File PDF (Laporan Harta Kekayaan) </label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files"
          />
        </div>
        <div className="col-span-6">
          <label className="m-2 ">Photo </label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files-img"
          />
        </div>
        <div className="col-span-3 ml-2 mr-4 my-5 bg-white relative z-50">
          <label className="">Divisi (Jabatan secara umum)</label>
          <Select
            options={options}
            onChange={(opt) => setEksDivisi(opt.value)}
            defaultValue={{
              label:
                article &&
                article.executive_division &&
                article.executive_division.divisi_id,
              value:
                article &&
                article.executive_division &&
                article.executive_division.id,
            }}
          />
        </div>
      </div>
      <div className="ml-2 mt-5">
        <label className="">Description (ID)</label>
        <Editor onChange={setHtmlID} value={htmlID} />
      </div>
      <div className="ml-2 mt-5">
        <label className="">Description (EN)</label>
        <Editor onChange={setHtmlEN} value={htmlEN} />
      </div>

      {/* <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Description (ID) </label>
          <textarea
            rows={8}
            className={`${style}`}
            ref={register}
            type="text"
            name="description_id"
            defaultValue={article && article.description_id}
          />
        </div>
        <div className="col-span-6">
          <label className="m-2 ">Description (EN) </label>
          <textarea
            rows={8}
            className={`${style}`}
            ref={register}
            type="text"
            name="description_en"
            defaultValue={article && article.description_en}
          />
        </div>
      </div> */}

      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
