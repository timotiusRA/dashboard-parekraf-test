import * as Icon from "react-feather";

export default function TableEvent({ data, deleteItem, editItem }) {
  return (
    <table className="table-fixed">
      <thead>
        <tr>
          <th className="border w-1/12 px-4 py-2">ID</th>
          <th className="border w-1/4 px-4 py-2">Name</th>
          <th className="border w-1/12 px-4 py-2">Durasi </th>
          <th className="border w-1/12 px-4 py-2">Kota</th>
          <th className="border w-1/12 px-4 py-2">Action</th>
        </tr>
      </thead>
      <tbody>
        {data &&
          data.map((ortu, i) => (
            <tr key={i} className="text-center border">
              <td className="border px-4 py-2">{ortu.id}</td>

              <td className="break-words border px-4 py-2">
                {ortu.name_id && ortu.name_id} | {ortu.name_en && ortu.name_en}
              </td>
              <td className="border px-4 py-2">{ortu.duration_id}</td>
              <td className="border px-4 py-2">
                {ortu.city && ortu.city["name"]}
              </td>
              <td className="px-4 py-3 flex justify-center">
                <button
                  onClick={() => editItem(ortu.id)}
                  className="mr-5"
                  value={ortu.id}
                >
                  <Icon.Edit className="text-green-500" size={16} />
                </button>
                <button onClick={() => deleteItem(ortu.id)} value={ortu.id}>
                  <Icon.Trash2 className="text-red-600" size={16} />
                </button>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
