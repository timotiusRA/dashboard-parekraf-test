import PropTypes from "prop-types";
import { confirmable } from "react-confirm";

const YourDialog = ({ show, proceed, confirmation, options }) => {
  return (
    <div
      className="fixed inset-0 z-50 overflow-auto flex"
      onHide={() => proceed(false)}
      show={show}
    >
      <div className="relative p-8 bg-gray-500 w-1/2 max-w-md m-auto flex-col flex rounded-lg">
        <p className="text-lg text-primaryNavy font-semibold">{confirmation}</p>
        <div className="flex justify-center mt-5">
          <button
            onClick={() => proceed(true)}
            className="mr-20 bg-primaryNavy py-3 px-5 rounded-lg text-white hover:text-black hover:bg-gold1"
          >
            YES
          </button>
          <button
            className=" bg-primaryNavy py-3 px-5 rounded-lg text-white hover:text-black hover:bg-gold1"
            onClick={() => proceed(false)}
          >
            NO
          </button>
        </div>
      </div>
    </div>
  );
};

YourDialog.propTypes = {
  show: PropTypes.bool, // from confirmable. indicates if the dialog is shown or not.
  proceed: PropTypes.func, // from confirmable. call to close the dialog with promise resolved.
  confirmation: PropTypes.string, // arguments of your confirm function
  options: PropTypes.object, // arguments of your confirm function
};

// confirmable HOC pass props `show`, `dismiss`, `cancel` and `proceed` to your component.
export default confirmable(YourDialog);
