import DatePicker from "react-datepicker";
import Select from "react-select";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

export default function InputEvent({
  handleSubmit,
  onSubmit,
  register,
  event,
  startDate,
  endDate,
  setStartDate,
  setEndDate,
  cities,
  setCity,
  city,
}) {
  let options = [];
  cities.forEach((element) => {
    let option = {
      label: element.name,
      value: element.id,
    };
    options.push(option);
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full grid grid-cols-1">
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2 ">Name ID</label>
          <input
            ref={register}
            type="text"
            name="name_id"
            defaultValue={event && event.name_id}
            className={`${style}`}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Name EN</label>
          <input
            ref={register}
            type="text"
            name="name_en"
            defaultValue={event && event.name_en}
            className={`${style}`}
          />
        </div>
      </div>
      {/* <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Place ID</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="place_id"
            defaultValue={event && event.place_id}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Place EN</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="place_en"
            defaultValue={event && event.place_en}
          />
        </div>
      </div>

      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">City ID</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="city_id"
            defaultValue={event && event.city_id}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">City EN</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="city_en"
            defaultValue={event && event.city_en}
          />
        </div>
      </div> */}
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Telephone</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="telp"
            defaultValue={event && event.telp}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Website</label>

          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="website"
            defaultValue={event && event.website}
          />
        </div>
      </div>
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Image</label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files"
          />
        </div>
        <div className="col-span-3 ml-5 mt-5">
          <p>
            * Maximum size 2MB <br />* Image must high resolution
            <br /> * Image type (JPG/PNG)
          </p>
        </div>
      </div>
      <div className="grid grid-cols-6 w-full mr-8 mt-5">
        <div className="col-span-3">
          <label className="m-2">Start Date</label>
          <div className="m-2 mt-3">
            <DatePicker
              selected={startDate}
              onChange={(date) => setStartDate(date)}
            />
          </div>
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">End Date</label>
          <div className="mt-3 m-2">
            <DatePicker
              selected={endDate}
              onChange={(date) => setEndDate(date)}
            />
          </div>
        </div>
      </div>

      <div className="grid grid-cols-6 w-full mr-8 mt-10">
        <div className="col-span-3 ml-3">
          <input
            type="checkbox"
            ref={register}
            defaultChecked={event && event.national_event}
            name="national_event"
          />
          <label className="m-2">National Event</label>
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Regencies</label>
          <Select
            options={options}
            onChange={(opt) => setCity(opt.value)}
            defaultValue={{
              label: event && event.city && event.city.name,
              value: event && event.city && event.city.id,
            }}
          />
        </div>
      </div>

      <label className="m-2">Content ID</label>
      <textarea
        rows={5}
        className={`${style}`}
        ref={register({ required: true })}
        type="text"
        name="content_id"
        defaultValue={event && event.content_id}
      />

      <label className="m-2">Content EN</label>
      <textarea
        rows={5}
        className={`${style}`}
        ref={register({ required: true })}
        type="text"
        name="content_en"
        defaultValue={event && event.content_en}
      />
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
