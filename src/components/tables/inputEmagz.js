const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";
export default function InputEmagz({
  handleSubmit,
  onSubmit,
  register,
  magz,
  tahun,
  edisi,
}) {
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full grid grid-cols-1">
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2 ">Title ID</label>
          <input
            ref={register}
            type="text"
            name="title_id"
            defaultValue={(magz && magz.title_id) || ""}
            className={`${style}`}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Title EN</label>
          <input
            ref={register}
            type="text"
            name="title_en"
            defaultValue={(magz && magz.title_en) || ""}
            className={`${style}`}
          />
        </div>
      </div>
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Description ID</label>

          <textarea
            className={`${style} resize`}
            ref={register}
            type="text"
            name="description_id"
            defaultValue={(magz && magz.description_id) || ""}
          />
        </div>
        <div className="col-span-3 ml-5">
          <label className="m-2">Description EN</label>

          <textarea
            className={`${style} resize`}
            ref={register}
            type="text"
            name="description_en"
            defaultValue={(magz && magz.description_en) || ""}
          />
        </div>
      </div>
      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Image</label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files-img"
          />
        </div>
        <div className="col-span-3 ml-6 mt-2">
          <p>
            * Maximum size 2MB <br />* Image must high resolution
            <br /> * Image type (JPG/PNG)
          </p>
        </div>
      </div>

      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">PDF</label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files"
          />
        </div>
        <div className="col-span-3 ml-6 mt-2">
          <p>
            * Maximum size 300MB <br />* PDF type
          </p>
        </div>
      </div>

      <div className="grid grid-cols-6 w-full mr-8">
        <div className="col-span-3">
          <label className="m-2">Edisi</label>
          <select
            className="w-full ml-2 p-2"
            id="grid-state"
            ref={register({ required: true })}
            name="edisi"
            defaultValue={
              magz && magz.magazine_category && magz.magazine_category["edisi"]
            }
          >
            {edisi.map((el, i) => (
              <option value={el} key={i}>
                {el}
              </option>
            ))}
          </select>
        </div>
        <div className="col-span-3 ml-6">
          <label className="m-2">Tahun</label>
          <select
            className="w-full ml-2 p-2"
            id="grid-state"
            ref={register({ required: true })}
            name="tahun"
            defaultValue={
              magz && magz.magazine_category && magz.magazine_category["tahun"]
            }
          >
            {tahun.map((el, i) => (
              <option value={el} key={i}>
                {el}
              </option>
            ))}
          </select>
        </div>
      </div>
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
