import React, { useRef } from "react";
import WidgetTitle from "../widget-title";
import { NotificationManager } from "react-notifications";
import { useRouter } from "next/router";
import axios from "axios";
import * as Icon from "react-feather";

const TableMedia = ({ data, type, token }) => {
  const router = useRouter();
  const urlRef = useRef([]);
  function copyToClipboard(ref, index) {
    let currentNode = ref.current[index];
    if (document.body.createTextRange) {
      const range = document.body.createTextRange();
      range.moveToElementText(currentNode);
      range.select();
      document.execCommand("copy");
      range.remove();
      NotificationManager.success("Berhasil Dicopy", 3000);
    } else if (window.getSelection) {
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents(currentNode);
      selection.removeAllRanges();
      selection.addRange(range);
      document.execCommand("copy");
      selection.removeAllRanges();
      NotificationManager.success("Berhasil Dicopy", 3000);
    } else {
      NotificationManager.error("Browser tidak tersupport", "Gagal copy", 5000);
    }
  }

  const deleteItem = async (id) => {
    await axios
      .delete(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload/files/` + id, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        NotificationManager.success("Sukses", 3000);
        router.push("/media/all-media");
      })
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <>
      <table className="table">
        <thead>
          <tr>
            {type === "image" ? <th className="border">Media</th> : null}
            <th className="border">File</th>
            <th className="border">Type</th>
            <th className="border">Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((el, i) => (
            <tr key={i}>
              {type === "image" ? (
                <td className="border">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 h-20 w-20">
                      <img
                        className="h-20 w-20 rounded-lg shadow object-cover"
                        src={`${el.url}`}
                        alt={`image-${el.url}`}
                      />
                    </div>
                  </div>
                </td>
              ) : null}
              <td className="border">
                <div className="flex items-center">
                  <div className="flex flex-col">
                    <a href={el.url} ref={(el) => (urlRef.current[i] = el)}>
                      <div className="text-sm font-medium text-blue-600">
                        {el.name.slice(0, 90) + "..."}
                      </div>
                    </a>
                  </div>
                </div>
              </td>
              <td className="border">{type}</td>
              <td className="border">
                {/* <div className="flex justify-center "> */}
                {/* <button onClick={() => copyToClipboard(urlRef, i)}>
                    <Icon.Copy className="text-green-500" size={16} />
                  </button> */}
                <button onClick={() => deleteItem(el.id)} type="button">
                  <Icon.Trash2 className="text-red-600" size={16} />
                </button>
                {/* </div> */}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};
export default TableMedia;
