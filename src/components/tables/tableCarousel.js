import * as Icon from "react-feather";

export default function TableBanner({
  dataLink,
  page,
  handleOnClick,
  deleteItem,
  limit,
}) {
  return (
    <table className="table-fixed mt-10">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold w-1/12">No</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold w-2/12">Image</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold w-2/12">Title</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold w-4/12">Description</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold w-2/12">Link</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold w-1/12">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border w-1/12">
                <div className="flex items-center">
                  {i + 1 + (page - 1) * limit}
                </div>
              </td>
              <>
                <td className="border w-2/12 ">
                  <img
                    className=""
                    src={el.image && el.image.url}
                    alt="gambar-i"
                  />
                </td>
                <td className="border w-2/12">
                  <div className="flex items-center ml-3 ">
                    {el.title_id && el.title_id}
                  </div>
                </td>
                <td className="border w-4/12">
                  <div className="flex items-center ml-3">
                    {el.description_id && el.description_id}
                  </div>
                </td>
                <td className="border break-all w-2/12">
                  <div className="flex items-center">
                    <a href={el.url && el.url} className="text-clickable">
                      {el.url && el.url}
                    </a>
                  </div>
                </td>
              </>

              <td className="border w-1/12">
                <div className="flex">
                  <button onClick={() => handleOnClick(el)} className="mx-2">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
