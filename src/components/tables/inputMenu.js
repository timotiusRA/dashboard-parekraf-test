import Select from "react-select";

const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

export default function InputMenu({
  el,
  nav,
  onSubmit,
  register,
  handleSubmit,
  parent,
  setParent,
}) {
  let options = [];
  nav.forEach((element) => {
    let option = {
      label: element.menu_id,
      value: element.id,
    };
    options.push(option);
  });

  let parent_name;
  if (el && el.parent_id) {
    for (let i = 0; i < nav.length; i++) {
      if (nav[i].id === el.parent_id) {
        parent_name = nav[i].menu_id;
      }
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full grid grid-cols-1">
      <div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2 ">Menu ID</label>
            <input
              ref={register}
              type="text"
              name="menu_id"
              defaultValue={el && el.menu_id}
              className={`${style}`}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Menu EN</label>
            <input
              ref={register}
              type="text"
              name="menu_en"
              defaultValue={el && el.menu_en}
              className={`${style}`}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3 m-2">
            <label className="">Parent</label>
            <Select
              options={options}
              onChange={(opt) => setParent(opt.value)}
              defaultValue={{
                label: el && parent_name,
                value: el && el.parent_id,
              }}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Order</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="order"
              defaultValue={el && el.order}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Link</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="href"
              placeholder="contoh: /berita"
              defaultValue={el && el.href}
            />
          </div>
        </div>
      </div>
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
