import WidgetTitle from "../widget-title";
import Link from "next/link";
import * as Icon from "react-feather";

const Table3 = ({ title, data, toEditPage, deleteItem }) => {
  const sluggify = (str) =>
    str
      .replace(/-/g, "_")
      .replace(/\s/g, "-")
      .replace(/\#/g, "%23")
      .replace(/\//g, "~")
      .replace(/\:/g, "%3A")
      .replace(/!/g, "%21")
      .replace(/'/g, "%27")
      .replace(/"/g, "%22")
      .replace(/\?/g, "%3F")
      .replace(/@/g, "%40")
      .replace(/,/g, "%2C");

  return (
    <>
      <div className="flex items-center justify-between">
        <WidgetTitle title={title} />
      </div>
      <div className="overflow-x-scroll w-full">
        <table className="table table-lg">
          <thead>
            <tr>
              <th className="border">
                <h2 className="text-sm font-bold">Title</h2>
              </th>
              <th className="border">
                <h2 className="text-sm font-bold">Category</h2>
              </th>
              <th className="border">
                <h2 className="text-sm font-bold">Author</h2>
              </th>
              <th className="border">
                <h2 className="text-sm font-bold">Action</h2>
              </th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((el, i) => (
                <tr key={i}>
                  <td className="border">
                    <div className="flex items-center">
                      {el.title_id && el.title_id.slice(0, 50) + "..."}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.article_categories[0]
                        ? el.article_categories[0].category_id
                        : null}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex items-center">
                      {el.user && el.user.username}
                    </div>
                  </td>
                  <td className="border">
                    <div className="flex">
                      <a
                        href={`https://kemenparekraf.go.id/berita/${sluggify(
                          el.title_id,
                        )}`}
                        target="_blank"
                      >
                        <button className="mr-5">
                          <Icon.Eye size={20} className="text-yellow-500" />
                        </button>
                      </a>
                      <button
                        onClick={() => toEditPage(el.id)}
                        className="mr-5"
                      >
                        <img
                          src="/edit.svg"
                          alt="edit-svg"
                          className="w-5 h-5"
                        />
                      </button>
                      <button onClick={() => deleteItem(el.id)}>
                        <img
                          src="/trash.svg"
                          alt="trash-svg"
                          className="w-5 h-5"
                        />
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default Table3;
