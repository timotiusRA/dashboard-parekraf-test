import * as Icon from "react-feather";

export default function TableProfilPimpinan({
  dataLink,
  page,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-10 w-full">
      <thead>
        <tr>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">No</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold ml-3">Image</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold w-2/12 ml-3">Name</h2>
          </th>
          <th className="border w-4/12">
            <h2 className="text-sm font-bold w-4/12 ml-3">Position</h2>
          </th>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border w-1/12 ">
                <div className="flex items-center ml-3">
                  {i + 1 + (page - 1) * 10}
                </div>
              </td>
              <td className="border">
                <img
                  className="w-40 ml-10"
                  src={el.photo && el.photo["url"]}
                  alt={`images-${el.name}`}
                />
              </td>
              <td className="border w-2/12">
                <div className="flex items-center ml-3 break-words">
                  {el.name && el.name}
                </div>
              </td>
              <td className="border w-4/12 ">
                <div className="flex items-center ml-3 break-words">
                  {el.position_id && el.position_id}
                </div>
              </td>
              <td className="border w-1/12">
                <div className="flex">
                  <button onClick={() => handleOnClick(el.id)} className="ml-5">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="ml-5">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
