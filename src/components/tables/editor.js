import { Component } from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";
import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough";
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote";
import Link from "@ckeditor/ckeditor5-link/src/link";
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice";
import Heading from "@ckeditor/ckeditor5-heading/src/heading";
import Font from "@ckeditor/ckeditor5-font/src/font";
import Image from "@ckeditor/ckeditor5-image/src/image";
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle";
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar";
import ImageUpload from "@ckeditor/ckeditor5-image/src/imageupload";
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
import ImageTextAlternative from "@ckeditor/ckeditor5-image/src/imagetextalternative";
import List from "@ckeditor/ckeditor5-list/src/list";
import ListStyle from "@ckeditor/ckeditor5-list/src/liststyle";
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation";
import Indent from "@ckeditor/ckeditor5-indent/src/indent";
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock";
import Markdown from "@ckeditor/ckeditor5-markdown-gfm/src/markdown";
import axios from "axios";
import ImageTextAlternativeEditing from "@ckeditor/ckeditor5-image/src/imagetextalternative/imagetextalternativeediting";
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat";
export default class Editor extends Component {
  render() {
    const { value, onChange } = this.props;

    const custom_config = {
      extraPlugins: [MyCustomUploadAdapterPlugin],
      plugins: [
        Markdown,
        Essentials,
        Autoformat,
        Bold,
        Italic,
        ImageTextAlternativeEditing,
        ImageTextAlternative,
        Paragraph,
        Heading,
        Indent,
        IndentBlock,
        Underline,
        Strikethrough,
        BlockQuote,
        Font,
        Alignment,
        List,
        Link,
        PasteFromOffice,
        Image,
        ImageStyle,
        ImageToolbar,
        ImageUpload,
        ImageResize,
        Table,
        TableToolbar,
        TextTransformation,
      ],
      toolbar: [
        "heading",
        "|",
        "bold",
        "italic",
        "underline",
        "strikethrough",
        "|",
        "fontSize",
        "fontColor",
        "fontBackgroundColor",
        "|",
        "alignment",
        "outdent",
        "indent",
        "bulletedList",
        "numberedList",
        "blockQuote",
        "|",
        "link",
        "insertTable",
        "imageUpload",
        "imageStyle:full",
        "imageTextAlternative",
        "|",
        "undo",
        "redo",
      ],
      heading: {
        options: [
          {
            model: "paragraph",
            view: "p",
            title: "Paragraph",
            class: "ck-heading_paragraph",
          },
          {
            model: "heading1",
            view: "h1",
            title: "Heading 1",
            class: "ck-heading_heading1",
          },
          {
            model: "heading2",
            view: "h2",
            title: "Heading 2",
            class: "ck-heading_heading2",
          },
          {
            model: "heading3",
            view: "h3",
            title: "Heading 3",
            class: "ck-heading_heading3",
          },
          {
            model: "heading4",
            view: "h4",
            title: "Heading 4",
            class: "ck-heading_heading4",
          },
          {
            model: "heading5",
            view: "h5",
            title: "Heading 5",
            class: "ck-heading_heading5",
          },
          {
            model: "heading6",
            view: "h6",
            title: "Heading 6",
            class: "ck-heading_heading6",
          },
        ],
      },
      fontSize: {
        options: [
          9,
          10,
          11,
          12,
          13,
          14,
          15,
          16,
          17,
          18,
          19,
          20,
          21,
          23,
          25,
          27,
          29,
          31,
          33,
          35,
        ],
      },
      alignment: {
        options: ["justify", "left", "center", "right"],
      },
      table: {
        contentToolbar: ["tableColumn", "tableRow", "mergeTableCells"],
      },
      image: {
        resizeUnit: "px",
        toolbar: [
          "imageStyle:alignLeft",
          "imageStyle:full",
          "imageStyle:alignRight",
          "|",
          "imageTextAlternative",
        ],
        styles: ["full", "alignLeft", "alignRight"],
      },
      typing: {
        transformations: {
          remove: [
            "enDash",
            "emDash",
            "oneHalf",
            "oneThird",
            "twoThirds",
            "oneForth",
            "threeQuarters",
          ],
        },
      },
      placeholder: "Click here to start typing",
    };

    return (
      <>
        <div className="ck">
          <CKEditor
            className="ck-content"
            required
            editor={ClassicEditor}
            config={custom_config}
            data={value}
            onChange={(event, editor) => {
              onChange(editor.getData());
            }}
          />
        </div>
      </>
    );
  }
}

function MyCustomUploadAdapterPlugin(editor) {
  editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
    return new MyUploadAdapter(loader);
  };
}

class MyUploadAdapter {
  constructor(loader) {
    this.loader = loader;
  }

  // Starts the upload process.
  upload() {
    return this.loader.file.then((uploadedFile) => {
      return new Promise(async (resolve, reject) => {
        const data = new FormData();
        data.append("files", uploadedFile);

        axios
          .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/upload`, data, {
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((response) => {
            if (response.status === 200) {
              resolve({
                default: response.data[0].url,
              });
            } else {
              reject(response.data.message);
            }
          })
          .catch((response) => {
            reject("Upload failed");
          });
      });
    });
  }

  abort() {
    if (this.xhr) {
      this.xhr.abort();
    }
  }
}
