import { useRouter } from "next/router";

export default function Pagination({ url, page, lastPage, limit, category }) {
  const router = useRouter();

  const handleClickMinus = () => {
    if (category) {
      router.push(
        `${url}?category=${category}&page=${page - 1}&limit=${limit}`,
      );
    } else {
      router.push(`${url}?page=${page - 1}&limit=${limit}`);
    }
  };
  const handleClickPlus = () => {
    if (category) {
      router.push(
        `${url}?category=${category}&page=${page + 1}&limit=${limit}`,
      );
    } else {
      router.push(`${url}?page=${page + 1}&limit=${limit}`);
    }
  };
  return (
    <>
      <button
        onClick={() => handleClickMinus()}
        className={`${
          page <= 1 ? "invisible" : null
        } bg-primaryNavy text-white font-bold py-2 px-4 mr-5 mt-5 rounded`}
      >
        {/* opacity-50 cursor-not-allowed */}
        Previous
      </button>
      <button
        onClick={() => handleClickPlus()}
        className={`${
          page >= lastPage ? "invisible" : null
        } bg-primaryNavy text-white font-bold py-2 px-4 mt-5 rounded `}
      >
        Next
      </button>
      <div className="mt-5">
        <span>
          Halaman {page} dari total {lastPage} halaman
        </span>
      </div>
    </>
  );
}
