import * as Icon from "react-feather";

export default function TableSubsektor({
  spbe,
  dataLink,
  page,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-10 w-full">
      <thead>
        <tr>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">No</h2>
          </th>
          <th className="border w-3/12">
            <h2 className="text-sm font-bold w-3/12 ml-3">Label</h2>
          </th>
          {!spbe ? (
            <>
              <th className="border w-1/12">
                <h2 className="text-sm font-bold w-1/12 ml-3">Icon</h2>
              </th>
            </>
          ) : null}
          <th className="border w-3/12">
            <h2 className="text-sm font-bold w-4/12 ml-3">Deskripsi</h2>
          </th>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border w-1/12 ">
                <div className="flex items-center ml-3">
                  {i + 1 + (page - 1) * 10}
                </div>
              </td>
              <td className="border w-2/12">
                <div className="flex items-center ml-3 break-words">
                  {!spbe ? el.label_id && el.label_id : el.title && el.title}
                </div>
              </td>
              {!spbe ? (
                <td className="border w-1/12">
                  <div className="flex items-center ml-3 break-words">
                    <img src={el.icon && el.icon.url} alt={"icon-subs"} />
                  </div>
                </td>
              ) : null}
              <td className="border w-3/12 ">
                <div className="flex items-center ml-3 break-words">
                  {el.content_id && el.content_id}
                </div>
              </td>
              <td className="border w-1/12">
                <div className="flex">
                  <button onClick={() => handleOnClick(el.id)} className="mx-5">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)}>
                    <img src="/trash.svg" alt="trash-svg" className="w-5 h-5" />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
