import * as Icon from "react-feather";

export default function TableAlamatKementerian({
  dataLink,
  page,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-10">
      <thead>
        <tr>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold ">No</h2>
          </th>
          <th className="border w-4/12">
            <h2 className="text-sm font-bold ">Gedung</h2>
          </th>
          <th className="border w-4/12">
            <h2 className="text-sm font-bold">Jalan</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold ">Kota</h2>
          </th>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold ">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border">
                <div className="ml-8">{i + 1 + (page - 1) * 10}</div>
              </td>
              <td className="border">
                <div className="">{el.building_id && el.building_id}</div>
              </td>
              <td className="border">
                <div className="">{el.street_id && el.street_id}</div>
              </td>

              <td className="border">
                <div className="">{el.city_id && el.city_id}</div>
              </td>

              <td className="border">
                <div className="flex justify-center ">
                  <button onClick={() => handleOnClick(el.id)} className="mr-2">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className=" ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
