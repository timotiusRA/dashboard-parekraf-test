import * as Icon from "react-feather";

export default function TableUser({
  data,
  page,
  register,
  kategori,
  handleSubmit,
  onSubmit,
  handleOnClick,
  deleted,
  thEdit,
}) {
  return (
    <table className="table table-lg">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold">No</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Username</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Email</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Role</h2>
          </th>
          {thEdit ? (
            <th className="border">
              <h2 className="text-sm font-bold">Password</h2>
            </th>
          ) : null}
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {data.map((el, i) => (
          <tr key={i}>
            <td className="border">
              <div className="flex items-center">{i + 1 + (page - 1) * 10}</div>
            </td>
            {!el.isEdit ? (
              <>
                <td className="border">
                  <div className="flex items-center">
                    {el.username && el.username}
                  </div>
                </td>
                <td className="border">
                  <div className="flex items-center">
                    {el.email && el.email}
                  </div>
                </td>
                <td className="border">
                  <div className="flex items-center">
                    {el.role && el.role["name"]}
                  </div>
                </td>
              </>
            ) : (
              <>
                <td className="border">
                  <input
                    className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                    ref={register}
                    type="text"
                    name="username"
                    defaultValue={el.username && el.username}
                  />
                </td>
                <td className="border">
                  <input
                    className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                    ref={register}
                    type="text"
                    name="email"
                    defaultValue={el.email && el.email}
                  />
                </td>
                <td className="border">
                  <select
                    className="w-full ml-2 p-2"
                    id="grid-state"
                    ref={register}
                    name="akses"
                  >
                    {kategori &&
                      kategori.map((el, i) => (
                        <option value={el.id} key={i}>
                          {el.role && el.role}
                        </option>
                      ))}
                  </select>
                </td>

                <td className="border">
                  <form onSubmit={handleSubmit(onSubmit)} className="flex">
                    <input
                      className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                      ref={register}
                      type="password"
                      name="password"
                    />
                    <button
                      type="submit"
                      className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                    >
                      Submit
                    </button>
                  </form>
                </td>
              </>
            )}

            <td className="border">
              <button onClick={() => handleOnClick(el)}>
                <Icon.Edit className="text-green-500" size={16} />
              </button>
              <button onClick={() => deleted(el.id)} className="ml-5">
                <Icon.Trash2 className="text-red-600" size={16} />
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
