const style =
  "focus:outline-none w-full border border-black rounded-lg m-2 p-2 text-lg";

export default function InputAlamatUpt({
  handleSubmit,
  onSubmit,
  register,
  el,
}) {
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full grid grid-cols-1">
      <div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Street ID</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="street_id"
              defaultValue={el && el.street_id}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Street EN</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="street_en"
              defaultValue={el && el.street_en}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">City ID</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="city_id"
              defaultValue={el && el.city_id}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">City EN</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="city_en"
              defaultValue={el && el.city_en}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Instance ID</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="instance_id"
              defaultValue={el && el.instance_id}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Instance EN</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="instance_en"
              defaultValue={el && el.instance_en}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Website</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="website"
              defaultValue={el && el.website}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Email</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="email"
              defaultValue={el && el.email}
            />
          </div>
        </div>
        <div className="grid grid-cols-6 w-full mr-8">
          <div className="col-span-3">
            <label className="m-2">Fax</label>

            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="fax"
              defaultValue={el && el.fax}
            />
          </div>
          <div className="col-span-3 ml-5">
            <label className="m-2">Telepon</label>
            <input
              className={`${style}`}
              ref={register}
              type="text"
              name="telp"
              defaultValue={el && el.telp}
            />
          </div>
        </div>
      </div>
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
