import dynamic from "next/dynamic";

const Editor = dynamic(() => import("../../components/tables/editor"), {
  ssr: false,
});

const style =
  "focus:outline-none w-full text-black border border-black rounded-lg m-2 p-2 text-lg";

export default function InputSpbe({
  handleSubmit,
  onSubmit,
  htmlEN,
  htmlID,
  setHtmlEN,
  setHtmlID,
  register,
  article,
}) {
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Title</label>
          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="title"
            defaultValue={article && article.title}
          />
        </div>
      </div>

      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2 ">Banner </label>
          <input
            className={`${style}`}
            ref={register}
            type="file"
            name="files"
          />
        </div>
        <div className="col-span-6">
          <label className="m-2">Link URL</label>
          <input
            className={`${style}`}
            ref={register}
            type="text"
            name="link"
            defaultValue={article && article.link}
          />
        </div>
      </div>
      <div className="grid grid-cols-6 mr-8">
        <div className="col-span-6">
          <label className="m-2">Headline (ID) </label>
          <textarea
            className={`${style}`}
            ref={register}
            type="text"
            name="content_id"
            defaultValue={article && article.content_id}
          />
        </div>
        <div className="col-span-6">
          <label className="m-2 ">Headline (EN) </label>
          <textarea
            className={`${style}`}
            ref={register}
            type="text"
            name="content_en"
            defaultValue={article && article.content_en}
          />
        </div>
      </div>

      <label className="m-2">Article (ID)</label>
      <Editor onChange={setHtmlID} value={htmlID} />
      <label className="m-2">Article (EN)</label>
      <Editor onChange={setHtmlEN} value={htmlEN} />
      <button className="hover:bg-white border border-primaryNavy text-primaryNavy mt-5 bg-blue-300 rounded-lg p-2 ml-2 w-full text-lg">
        Submit
      </button>
    </form>
  );
}
