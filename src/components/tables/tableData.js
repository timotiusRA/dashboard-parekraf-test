import React from "react";
import WidgetTitle from "../widget-title";
import * as Icon from "react-feather";
import moment from "moment";
import id from "moment/locale/id";
import { NotificationManager } from "react-notifications";
import { useForm } from "react-hook-form";

const TableData = ({ title, data }) => {
  const { register, handleSubmit } = useForm();
  const onSubmit = async (data) => {
    await axios
      .post(`${process.env.NEXT_PUBLIC_FETCH_URL}/XXX`, data)
      .then((res) => NotificationManager.success("Berhasil Diupload", 3000))
      .catch((err) => NotificationManager.error("Error", 5000));
  };

  return (
    <>
      <div className="flex items-center justify-between">
        <WidgetTitle title={title} />
      </div>
      <table className="table table-lg">
        <thead>
          <tr>
            <th>
              <h2 className="text-sm font-bold">Link</h2>
            </th>
            <th>
              <h2 className="text-sm font-bold">Date</h2>
            </th>
            <th>
              <h2 className="text-sm font-bold">Action</h2>
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map((el, i) => (
            <tr key={i}>
              <td>
                <div className="items-center">
                  <a
                    href={el.Youtube_link}
                    className="hover:text-blue-600 hover:underline"
                  >
                    {el.Youtube_link}
                  </a>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="">
                      <input
                        className="appearance-none border border-black rounded w-1/4 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="nama"
                        type="text"
                        aria-label="nama"
                        name="youtube_link"
                        placeholder="https://youtube.com/watch?v=X23feWgjc"
                        ref={register}
                      />
                      <button
                        type="submit"
                        className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                      >
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              </td>
              <td>
                <div className="flex items-center">
                  {moment(el.updated_at).locale("id", id).format("DD MMM YYYY")}
                </div>
              </td>
              <td>
                <div className="flex justify-center">
                  <button>
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button>
                    <Icon.Trash2 className="text-red-500 mx-3" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};
export default TableData;
