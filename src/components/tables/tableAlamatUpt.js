import * as Icon from "react-feather";

export default function TableAlamatUpt({
  dataLink,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-5">
      <thead>
        <tr className="border-b-2">
          <th className="border w-3/12">
            <h2 className="text-sm font-bold ">Instansi</h2>
          </th>
          <th className="border w-3/12">
            <h2 className="text-sm font-bold ">Jalan</h2>
          </th>

          <th className="border w-1/12">
            <h2 className="text-sm font-bold ">Website</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold ">Email</h2>
          </th>
          <th className="border  w-2/12">
            <h2 className="text-sm font-bold">Telp</h2>
          </th>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold ">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2 mt-5">
              {/* <td className="border">
                    <div className="flex items-center">{i + 1}</div>
                  </td> */}
              <td className="border">
                <div className="mr-3 flex items-center break-words">
                  {el.instance_id}
                </div>
              </td>
              <td className="border">
                <div className=" mr-3 flex items-center break-all">
                  {el.street_id && el.street_id} {el.city_id && el.city_id}
                </div>
              </td>

              <td className="border">
                <div className=" mr-3 flex items-center">{el.website && el.website}</div>
              </td>
              <td className="border">
                <div className="mr-3 flex items-center">{el.email && el.email}</div>
              </td>
              <td className="border">
                <div className="mr-3 flex items-center">{el.telp && el.telp}</div>
              </td>

              <td className="border">
                <div className="flex ">
                  <button onClick={() => handleOnClick(el.id)} className="ml-2">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="mx-3 ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
