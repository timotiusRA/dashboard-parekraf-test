import * as Icon from "react-feather";
import { Fragment } from "react";

export default function TableOrganisasiPublik({
  dataLink,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed ">
      <thead>
        <tr className="border-b-2">
          <th className="border">
            <h2 className="text-sm font-bold">Instansi</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Kategori</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Alamat</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, ind) => (
            <Fragment key={ind}>
              <tr className="border-b-2 mt-5">
                <td className="border">
                  <div className="flex items-center">{el.title_id && el.title_id}</div>
                </td>
                <td className="border">
                  <div className="flex items-center">
                    {el.organisasi_publik && el.organisasi_publik["tab_id"]}
                  </div>
                </td>
                <td className="border">
                  <div className="flex items-center">{el.address_id && el.address_id}</div>
                </td>

                <td className="border">
                  <div className="flex">
                    <button
                      onClick={() => handleOnClick(el.id)}
                      className="ml-2"
                    >
                      <Icon.Edit className="text-green-500" size={16} />
                    </button>
                    <button onClick={() => deleteItem(el.id)} className="mx-3 ">
                      <Icon.Trash2 className="text-red-600" size={16} />
                    </button>
                  </div>
                </td>
              </tr>
            </Fragment>
          ))}
      </tbody>
    </table>
  );
}
