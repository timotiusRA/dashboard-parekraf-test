import * as Icon from "react-feather";

export default function TableEmagz({
  dataLink,
  page,
  handleOnClick,
  deleteItem,
}) {
  return (
    <table className="table-fixed mt-10 w-full">
      <thead>
        <tr>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">No</h2>
          </th>
          <th className="border w-3/12">
            <h2 className="text-sm font-bold w-3/12 ml-3">PDF</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold w-2/12 ml-3">Nama</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">Kategori</h2>
          </th>
          <th className="border w-2/12">
            <h2 className="text-sm font-bold w-4/12 ml-3">Deskripsi</h2>
          </th>
          <th className="border w-1/12">
            <h2 className="text-sm font-bold w-1/12 ml-3">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {dataLink &&
          dataLink.map((el, i) => (
            <tr key={i} className="border-b-2">
              <td className="border w-1/12 ">
                <div className="flex items-center ml-3">
                  {i + 1 + (page - 1) * 10}
                </div>
              </td>
              {el.file && el.file.url ? (
                <td className="border w-3/12">
                  <div className="flex items-center">
                    <a
                      target="_blank"
                      className="break-all text-clickable cursor-pointer"
                      href={el.file && el.file.url}
                    >
                      {el.file && el.file.url}
                    </a>
                  </div>
                </td>
              ) : (
                <td className="border w-3/12">
                  <div className="flex items-center">{""}</div>
                </td>
              )}
              <td className="border w-2/12">
                <div className="flex items-center ml-3 break-words">
                  {el.title_id && el.title_id}
                </div>
              </td>
              <td className="border w-2/12">
                <div className="flex items-center ml-3 break-words">
                  {el.magazine_category
                    ? el.magazine_category["category_id"]
                    : null}
                </div>
              </td>
              <td className="border w-2/12 ">
                <div className="flex items-center ml-3 break-words">
                  {el.description_id && el.description_id}
                </div>
              </td>
              <td className="border w-1/12">
                <div className="flex">
                  <button onClick={() => handleOnClick(el.id)} className="ml-5">
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="mx-5 ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
