import * as Icon from "react-feather";

export default function TableKategoriArtikel({
  categoryData,
  handleOnClick,
  handleSubmit,
  deleteItem,
  register,
  onSubmit,
}) {
  return (
    <table className="table-lg table">
      <thead>
        <tr>
          <th className="border">
            <h2 className="text-sm font-bold ">ID</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Kategori Jabatan (ID)</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Kategori Jabatan (EN)</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Urutan</h2>
          </th>
          <th className="border">
            <h2 className="text-sm font-bold">Action</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        {categoryData &&
          categoryData.map((el, i) => (
            <tr key={i}>
              <td className="border">{el.id}</td>
              {!el.isEdit ? (
                <>
                  <td className="px-5">
                    <div className="items-center">
                      <span>{el.divisi_id && el.divisi_id}</span>
                    </div>
                  </td>
                  <td className="border">
                    <div className="items-center">
                      <span>{el.divisi_en && el.divisi_en}</span>
                    </div>
                  </td>
                  <td className="border">
                    <div className="items-center">
                      <span>{el.level && el.level}</span>
                    </div>
                  </td>
                </>
              ) : (
                <>
                  <td className="px-5">
                    <div className="items-center">
                      <input
                        className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                        id="nama"
                        type="text"
                        aria-label="nama"
                        name="divisi_id"
                        ref={register}
                        defaultValue={el.divisi_id && el.divisi_id}
                      />
                    </div>
                  </td>
                  <td className="border">
                    <div className="items-center">
                      <input
                        className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                        id="nama_en"
                        type="text"
                        aria-label="nama_en"
                        name="divisi_en"
                        ref={register}
                        defaultValue={el.divisi_en && el.divisi_en}
                      />
                    </div>
                  </td>
                  <td className="border">
                    <form onSubmit={handleSubmit(onSubmit)} className="flex">
                      <div className="items-center">
                        <input
                          className="px-3 py-1 appearance-none border border-black rounded leading-tight focus:outline-none focus:shadow-outline"
                          id="nama_en"
                          type="text"
                          aria-label="nama_en"
                          name="level"
                          ref={register}
                          defaultValue={el.level && el.level}
                        />
                      </div>
                      <button
                        type="submit"
                        className="ml-3 border border-primaryNavy bg-primaryNavy text-white px-3 rounded-lg hover:bg-white hover:text-primaryNavy"
                      >
                        Submit
                      </button>
                    </form>
                  </td>
                </>
              )}
              <td className="border">
                <div className="flex justify-center">
                  <button onClick={() => handleOnClick(el)}>
                    <Icon.Edit className="text-green-500" size={16} />
                  </button>
                  <button onClick={() => deleteItem(el.id)} className="mx-5 ">
                    <Icon.Trash2 className="text-red-600" size={16} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
