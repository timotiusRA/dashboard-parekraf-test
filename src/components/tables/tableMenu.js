import * as Icon from "react-feather";

export default function TableMenu({ data, deleteItem, editItem }) {
  return (
    <table className="table-fixed">
      <thead>
        <tr>
          <th className="border w-1/12 px-4 py-2">ID</th>
          <th className="border w-1/4 px-4 py-2">Name</th>
          <th className="border w-1/12 px-4 py-2">Parent</th>
          <th className="border w-1/12 px-4 py-2">Order</th>
          <th className="border w-1/12 px-4 py-2">Action</th>
        </tr>
      </thead>
      <tbody>
        {data &&
          data.map((ortu, i) => (
            <tr key={i} className="text-center ">
              <td className="border px-4 py-2">{ortu.id}</td>

              <td className="break-words border px-4 py-2">
                {ortu.menu_id && ortu.menu_id}
              </td>
              <td className="border px-4 py-2">
                {(ortu.parent_name_id && ortu.parent_name_id) || "-"}
              </td>
              <td className="border px-4 py-2">{ortu.order && ortu.order}</td>
              <td className=" border p-4 flex justify-center">
                <button onClick={() => editItem(ortu.id)} className="mr-5">
                  <Icon.Edit className="text-green-500" size={16} />
                </button>
                <button onClick={() => deleteItem(ortu.id)}>
                  <Icon.Trash2 className="text-red-600" size={16} />
                </button>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
}
