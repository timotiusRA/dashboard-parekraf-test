import { useRouter } from "next/router";

export default function TotalEntries({ url, category, limit }) {
  const router = useRouter();

  const handleChange = (e) => {
    if (category) {
      router.push(`${url}?category=${category}&page=1&limit=${e.target.value}`);
    } else {
      router.push(`${url}?page=1&limit=${e.target.value}`);
    }
  };

  return (
    <>
      <div className="w-full md:w-1/3 px-3 -mt-4 md:mb-0">
        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
          Total Entries
        </label>
        <div className="relative">
          <select
            className="block appearance-none w-full bg-white border border-black text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="grid-state"
            onChange={(e) => handleChange(e)}
            defaultValue={limit}
          >
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
            <option value={100}>100</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg
              className="fill-current h-4 w-4"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
            </svg>
          </div>
        </div>
      </div>
    </>
  );
}
