export default function SocialMedia({ sosmedData }) {
  // regex youtube url
  let youtubeUrl = ["Vq_07WzFSgE"];
  if (sosmedData) {
    youtubeUrl = sosmedData[0].Youtube_link;
    const regex = /([a-zA-Z0-9_-]{11})/;
    youtubeUrl = regex.exec(youtubeUrl);
  }
  return (
    <div className="bg-baseCol pb-68p ">
      <div className="mx-4 md:mx-5vw mb-23p flex justify-between items-center">
        <h2
          className={`text-darkGray font-signika text-2xl md:text-2xl lg:text-4xl`}
        >
          Preview of Media Sosial
        </h2>
      </div>
      <div className="font-poppins text-sm text-darkGray  mx-4 md:mx-5vw">
        <div className="grid lg:grid-rows-3 lg:grid-flow-col gap-4 lg:mx-auto lg:my-auto">
          <div className="lg:h-654p lg:w-331p lg:row-span-2 bg-white rounded-20p shadow-widget">
            <a href="https://www.facebook.com/ParekrafRI/">
              <p className="px-10 mt-5 lg:mt-56 leading-22p">
                {sosmedData[0] && sosmedData[0].Facebook}
              </p>
              <div className="my-5 lg:my-0 px-10 lg:py-3 lg:mt-40 flex font-signika text-17p">
                <img src="/MediaSosial_Facebook.svg" alt="fb-logo" />
                <span className="ml-3">@Kemenparekraf.ri</span>
              </div>
            </a>
          </div>
          <div className="lg:row-span-1 lg:w-331p bg-white rounded-20p shadow-widget">
            <a href="https://twitter.com/Kemenparekraf">
              <p className="pt-10 px-10">
                {sosmedData[0] && sosmedData[0].Twitter_2}
              </p>
              <div className="px-10 mt-16 flex font-signika text-17p">
                <img src="/MediaSosial_Twitter.svg" alt="fb-logo" />
                <span className="my-5 lg:my-0 ml-3">@Kemenparekraf.ri</span>
              </div>
            </a>
          </div>
          <div className="lg:row-span-1 lg:col-span-2 bg-white rounded-20p shadow-widget">
            <a href="https://twitter.com/Kemenparekraf" className="lg:flex">
              <div>
                <p className="p-10">
                  {sosmedData[0] && sosmedData[0].Twitter_1}
                </p>
                <div className="px-10 lg:mt-5 flex font-signika text-17p">
                  <img
                    src="/instagram.png"
                    className="flex w-6 h-6 my-5 "
                    alt="fb-logo"
                  />
                  <span className="ml-3 my-5 ">@Kemenparekraf.ri</span>
                </div>
              </div>
              <div
                href="https://www.instagram.com/kemenparekraf.ri/"
                className="m-auto md:mr-5"
              >
                <img
                  src={sosmedData[0] && sosmedData[0].Instagram.url}
                  alt={sosmedData[0] && sosmedData[0].Instagram.alternativeText}
                  className="rounded-20p"
                />
              </div>
            </a>
          </div>
          <div className="lg:row-span-2 lg:col-span-2 h-330p lg:w-full lg:h-full bg-white rounded-20p shadow-widget overflow-hidden pt-5 relative">
            <iframe
              src={`https://www.youtube.com/embed/${youtubeUrl[0]}`}
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
              className="border-none h-full absolute w-full top-0 left-0"
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
}
