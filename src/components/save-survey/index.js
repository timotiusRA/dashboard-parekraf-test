import { useState, useEffect } from "react";
import { withRedux } from "../../lib/redux";
import { useSelector, shallowEqual } from "react-redux";
import {
  postSurvey,
  postOption,
  postQuestion,
  postSection,
} from "../../lib/postData";
import { useRouter } from "next/router";

const SaveSurvey = ({ surveyData, sectionData, userId }) => {
  const router = useRouter();
  const { token } = useSelector(
    (state) => ({
      token: state.token,
    }),
    shallowEqual,
  );

  const postDataSurvey = async (status) => {
    const question_category_id = [];
    for (const section of sectionData) {
      const question_id = [];
      for (const question of section.question) {
        const option_id = [];
        if (question.option) {
          for (const option of question.option) {
            const setDataOption = {
              option: option,
              created_by: userId,
            };
            const resOption = await postOption(setDataOption, token);
            option_id.push(resOption);
          }
        }

        delete question.option;
        const setDataQuestion = {
          ...question,
          option_id: option_id.length > 0 ? { data: option_id } : null,
        };
        const resQuestion = await postQuestion(setDataQuestion, token);
        question_id.push(resQuestion);
      }

      delete section.question;
      const setDataSection = {
        ...section,
        question_id: question_id.length > 0 ? { data: question_id } : null,
      };
      const resSection = await postSection(setDataSection, token);
      question_category_id.push(resSection);
    }

    const setDataSurvey = {
      ...surveyData,
      question_category_id:
        question_category_id.length > 0 ? { data: question_category_id } : null,
      status,
    };
    await postSurvey(setDataSurvey, token);
    alert("Succes add Survey");
    router.push("/survey/all-survey");
  };

  console.log(surveyData);
  // console.log(sectionData);
  // console.log(token);

  return (
    <div>
      <div className="m-2 rounded-md shadow-lg border-blue-200 border-2 p-4 my-20">
        <div className="grid grid-cols-2 gap-4">
          <button
            className="bg-blue-700 text-white py-20 rounded-md"
            onClick={() => {
              postDataSurvey(false);
            }}
          >
            Save as Draft
          </button>
          <button
            className="bg-green-700 text-white py-20 rounded-md"
            onClick={() => {
              postDataSurvey(true);
            }}
          >
            Publish
          </button>
        </div>
      </div>
    </div>
  );
};

export default withRedux(SaveSurvey);
