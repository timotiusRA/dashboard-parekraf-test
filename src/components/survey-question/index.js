import { useState, useEffect } from "react";
import * as Icon from "react-feather";

const AddQuestion = ({ userId, section, getSectionQuestion }) => {
  const [desc, setDesc] = useState("");
  const [questions, setQuestion] = useState([]);
  const [option, setOption] = useState([]);
  const [type, setType] = useState("");
  const [tmpOption, setTmpOption] = useState("");

  const [rawData, setRawData] = useState({
    question: "",
    is_required: false,
    survey_question_type: "",
    created_by: userId,
  });
  const [toggle, setToggle] = useState(false);

  const removeOption = (item) => {
    setOption(option.filter((e) => e !== item));
  };

  const removeQuestion = (index) => {
    const newQuestion = questions;
    newQuestion.splice(index, 1);
    setToggle(!toggle);
    setQuestion(newQuestion);
  };

  const saveQuestion = () => {
    if (option.length > 0) {
      setQuestion([...questions, { ...rawData, option }]);
    } else {
      setQuestion([...questions, rawData]);
    }
    setOption([]);
    setType("");
    setTmpOption("");
    setRawData({
      question: "",
      is_required: false,
      survey_question_type: "",
      created_by: userId,
    });
  };

  const collectQuestion = () => {
    const setData = {
      name: section,
      desc,
      question: questions,
      created_by: userId,
    };
    getSectionQuestion(setData);
  };

  // console.log("quesfefe", questions);
  return (
    <div>
      <div className="font-bold text-xl">ADD Survey - Section - {section}</div>
      <div className="w-full mb-4 mt-4">
        <label className="block">
          <span className="text-default">Description</span>
          <input
            name="desc"
            type="text"
            value={desc}
            onChange={(event) => {
              setDesc(event.target.value);
            }}
            className="form-input mt-1 text-xs block w-full bg-white"
            placeholder="Enter Section Description"
          />
        </label>
      </div>
      <div className="m-2 rounded-md shadow-lg border-blue-200 border-2 p-4">
        <div className="font-semibold text-lg">Add Question</div>
        <div className="w-full mb-1 mt-4">
          <label className="block">
            <span className="text-xs">Question</span>
            <input
              onChange={(event) => {
                setRawData({ ...rawData, question: event.target.value });
              }}
              value={rawData.question}
              type="text"
              className="form-input mt-1 text-xs block w-full bg-white"
              placeholder="Enter survey title"
            />
          </label>
        </div>
        <div className="mb-4">
          <label>
            <input
              name="question"
              type="checkbox"
              checked={rawData.is_required}
              onChange={(event) => {
                setRawData({ ...rawData, is_required: event.target.checked });
              }}
            />
            <span className="text-xs ml-2 text-red-500">Required</span>
          </label>
        </div>
        <div className="mb-4">
          <div className="text-xs">Type</div>
          <select
            className="border-2 border-gray-600 rounded-md w-full sm:w-1/4 mt-1"
            value={rawData.survey_question_type}
            onChange={(event) => {
              setType(event.target.value);
              setRawData({
                ...rawData,
                survey_question_type: event.target.value,
              });
            }}
          >
            <option value="">-</option>
            <option value="1">Radio</option>
            <option value="2">Checkbox</option>
            <option value="3">Text Area</option>
          </select>
        </div>
        {rawData.survey_question_type &&
        rawData.survey_question_type !== "3" ? (
          <div className="w-full mb-4">
            <label className="block">
              <div className="text-default">Option</div>
              <input
                type="text"
                className="form-input mt-1 w-10/12 text-xs bg-white"
                placeholder="Enter survey section"
                onChange={(event) => {
                  setTmpOption(event.target.value);
                }}
              />
              <button
                className="bg-blue-800 text-white p-2 rounded-md ml-2"
                onClick={() => {
                  setOption([...option, tmpOption]);
                }}
              >
                Add
              </button>
            </label>
          </div>
        ) : (
          ""
        )}
        {option.map((item, index) => {
          return (
            <button
              className="bg-blue-600 text-white p-2 rounded-md ml-3 flex mb-2"
              key={index}
            >
              {item}
              <Icon.Trash
                className="text-white-500 cursor-pointer ml-2"
                onClick={() => {
                  removeOption(item);
                }}
                size={16}
              />
            </button>
          );
        })}
        {option.length > 0 || rawData.survey_question_type === "3" ? (
          <button
            className="bg-green-800 text-white p-2 rounded-md mt-3 w-full"
            onClick={saveQuestion}
          >
            Save
          </button>
        ) : (
          ""
        )}
      </div>
      <table className="table-fixed border-collapse border border-gray-400 w-full mt-5">
        <thead>
          <tr>
            <th className="w-1/12 border border-gray-400 p-2">NO.</th>
            <th className="w-5/12 border border-gray-400">QUESTIONS</th>
            <th className="w-4/12 border border-gray-400">OPTIONS</th>
          </tr>
        </thead>
        <tbody>
          {questions.map((item, index) => {
            return (
              <tr className="mt-4" key={`${index}${toggle}`}>
                <td className="text-center border border-gray-400 font-semibold">
                  {index + 1}
                </td>
                <td className="border border-gray-400 relative">
                  <Icon.Trash
                    className="text-red-500 cursor-pointer ml-2 absolute top-0 right-0 m-1"
                    onClick={() => {
                      removeQuestion(index);
                    }}
                    size={16}
                  />
                  <p className="break-words p-1">
                    {item.question}
                    {item.is_required ? (
                      <span className="mt-8 text-red-600 mb-8">*</span>
                    ) : (
                      ""
                    )}
                  </p>
                </td>
                <td className="border border-gray-400 p-1">
                  {item.option
                    ? item.option.map((e, i) => {
                        return (
                          <div key={i}>
                            <button className="bg-blue-800 text-white pl-2 pr-2 m-1 rounded-md">
                              {e}
                            </button>
                          </div>
                        );
                      })
                    : "Text Area"}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {questions.length > 0 ? (
        <div className="flex justify-end mt-4">
          <button
            className="bg-blue-800 text-white pl-8 pr-8 pt-2 pb-2 rounded-md"
            onClick={collectQuestion}
          >
            Next
          </button>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default AddQuestion;
