# Dockerfile

# base image
FROM node:alpine

# create & set working directory
RUN mkdir -p /usr/src
WORKDIR /usr/src

# copy source files
COPY . /usr/src

ENV DISABLE_OPENCOLLECTIVE=true

# install dependencies
RUN yarn install

# Environment variables
ARG NEXT_PUBLIC_FETCH_URL
ENV NEXT_PUBLIC_FETCH_URL=$NEXT_PUBLIC_FETCH_URL

# start app
RUN yarn run build
EXPOSE 4444
CMD yarn run start
